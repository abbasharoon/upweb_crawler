var express = require('express');
var router = express.Router();
const startChecks = require('../controllers/initialChecks');
const Crawler = require('simplecrawler');
/* GET home page. */
router.post('/', function (req, res) {
  res.end('scanStarted');
  const serverSecret = req.body.serverSecret;
  startChecks(Crawler, serverSecret);
});

module.exports = router;
