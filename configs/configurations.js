const fs = require('fs');
const path = require('path');

let configurations = null;

const get = function get() {
  if (!configurations) {
    const configs = fs.readFileSync(path.join(__dirname, '/default.json'));
    configurations = JSON.parse(configs);
    return configurations;
  } else {
    return configurations;
  }
};

module.exports = get;
