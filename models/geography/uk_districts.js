/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ukDistricts', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    pseudo_id: {
      type: DataTypes.INTEGER(11),
      allowNull: true
    }
  }, {
    tableName: 'uk_districts',
    timestamps: false           // this will deactivate the timestamp columns

  });
};
