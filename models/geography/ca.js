/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('ca', {
    zip: {
      type: DataTypes.STRING,
      allowNull: false,
      primaryKey: true
    },
    FSA: {
      type: DataTypes.STRING,
      allowNull: false
    },
    latitude: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    longitude: {
      type: DataTypes.DECIMAL,
      allowNull: false
    },
    Place_Name: {
      type: DataTypes.STRING,
      allowNull: true
    },
    FSA1: {
      type: DataTypes.STRING,
      allowNull: false
    },
    FSAProvince: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    AreaType: {
      type: DataTypes.STRING,
      allowNull: false
    }
  }, {
    tableName: 'ca',
    timestamps: false           // this will deactivate the timestamp columns

  });
};
