var fs = require("fs");
var path = require("path");
var Sequelize = require("sequelize");
const Configs = require('./../../configs/configurations.js');

const configs = Configs();

var sequelize = new Sequelize(configs.DbConfig.geographics.name,
  configs.DbConfig.geographics.user,
  configs.DbConfig.geographics.password, {
    dialect: "mysql", // or 'sqlite', 'postgres', 'mariadb'
    port: configs.DbConfig.geographics.port, // or 5432 (for postgres)
    logging: false,
    host: configs.DbConfig.geographics.host,
  });
var db = {};

fs
  .readdirSync(__dirname)
  .filter(function (file) {
    return (file.indexOf(".") !== -1) && (file !== "index.js");
  })
  .forEach(function (file) {
    var model = sequelize.import(path.join(__dirname, file));
    db[model.name] = model;
  });

Object.keys(db).forEach(function (modelName) {
  if ("associate" in db[modelName]) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;
