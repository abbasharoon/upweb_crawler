/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('gb', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },
    zip: {
      type: DataTypes.STRING,
      allowNull: true
    },  latitude: {
      type: DataTypes.DECIMAL,
      allowNull: false
    }, longitude: {
      type: DataTypes.DECIMAL,
      allowNull: false
    }
  }, {
    tableName: 'gb',
    timestamps: false           // this will deactivate the timestamp columns

  });
};
