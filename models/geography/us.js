/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('us', {
    zip: {
      type: DataTypes.INTEGER(11),
      allowNull: false
    },
    primary_city: {
      type: DataTypes.STRING,
      allowNull: false
    },
    state: {
      type: DataTypes.STRING,
      allowNull: false
    },
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    },  latitude: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }, longitude: {
      type: DataTypes.DECIMAL,
      allowNull: true
    }
  }, {
    tableName: 'us',
    timestamps: false           // this will deactivate the timestamp columns

  });
};
