/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('socialChecks', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    },
    link: {
      type: DataTypes.STRING(300),
      allowNull: false,
      field: 'link',
    },
    typeId: {
      type: DataTypes.INTEGER(30),
      allowNull: false,
      field: 'type_id',
    },
    uriId: {
      type: DataTypes.STRING(250),
      allowNull: false,
      field: 'uri_id',
    },
    scanId: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      field: 'scan_id',
    },
  }, {
    tableName: 'social_checks',
    timestamps: false,
  });
};
