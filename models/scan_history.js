/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('scanHistory', {
    id: {
      type: DataTypes.INTEGER(8),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    },
    criticalCount: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      primaryKey: true,
      field: 'cat_a_count',
    },
    criticalTypes: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      field: 'cat_a_types',
    },
    seoCount: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      primaryKey: true,
      field: 'cat_b_count',
    },
    seoTypes: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      field: 'cat_b_types',
    },
    recommendationsCount: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      primaryKey: true,
      field: 'cat_c_count',
    },
    recommendationsTypes: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      field: 'cat_c_types',
    },
    croCount: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      primaryKey: true,
      field: 'cat_d_count',
    },
    croTypes: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      field: 'cat_d_types',
    },
    scanId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'scan_id',
    },
    totalIssuesCount: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'total_issues_count',
    },
    pagesCrawled: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'pages_crawled',
    },
    issuesPages: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'issues_pages',
    },
    uniqueIssues: {
      type: DataTypes.INTEGER(200),
      allowNull: false,
      field: 'unique_issues',
    },
    hostId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'host_id',
    },
    // created_at: {
    //   type: 'TIMESTAMP',
    //   defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    //   allowNull: false,
    //   field: 'created_at',
    // },
    // updated_at: {
    //   type: 'TIMESTAMP',
    //   defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
    //   allowNull: false,
    //   field: 'updated_at',
    // },
  }, {
    tableName: 'scan_history',
    underscored: true,
    timestamps: true,
  });
};
