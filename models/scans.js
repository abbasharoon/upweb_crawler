/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('scans', {
    id: {
      type: DataTypes.INTEGER(8),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    hostId: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      field: 'host_id'
    },
    serverSecret: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'server_secret'
    },
    crawlLimit: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      field: 'crawl_limit',
    },
    browserChecks: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      field: 'browser_checks',
    },
    status: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      field: 'status'
    },
    deletedAt: {
      type: DataTypes.TIME,
      allowNull: true,
      field: 'deleted_at'
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: true,
      field: 'created_at'
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: true,
      field: 'updated_at'
    }
  }, {
    tableName: 'scans'
  });
};
