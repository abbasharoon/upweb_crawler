/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('uris', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    },
    uri: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'uri',
    },
    type: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'type',
    },
    statusCode: {
      type: DataTypes.INTEGER(6),
      field: 'status_code',
    },
    responseTime: {
      type: DataTypes.INTEGER(6),
      field: 'response_time',
    },
    contentLength: {
      type: DataTypes.INTEGER(16),
      field: 'content_Length',
    },
    cartLinks: {
      type: DataTypes.INTEGER(2),
      field: 'cart_links',
    },
    referrerLink: {
      type: DataTypes.STRING(256),
      allowNull: true,
      field: 'referrer_link',
    },
    hostId: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      field: 'host_id',
    },
  }, {
    tableName: 'uris',
    timestamps: false,
  });
};
