/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('scanQueue', {
    id: {
      type: DataTypes.INTEGER(8),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id',
    },
    userId: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      field: 'user_id',
    },
    hostId: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      field: 'host_id',
    },
    gapHours: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      field: 'gap_hours',
    },
    status: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      field: 'status',
    },
    mailReport: {
      type: DataTypes.INTEGER(1),
      allowNull: false,
      field: 'mail_report',
    },
    scanTime: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'scan_time',
    },
  }, {
    tableName: 'scan_queues',
    underscored: true,
    timestamps: false,
  });
};
