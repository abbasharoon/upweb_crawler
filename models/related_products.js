/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('relatedProducts', {
    scanId: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      primaryKey: true,
      field: 'scan_id'
    },
    title: {
      type: "BLOB",
      allowNull: false,
      field: 'title'
    },
    image: {
      type: "BLOB",
      allowNull: false,
      field: 'image'
    },
    price: {
      type: "BLOB",
      allowNull: false,
      field: 'price'
    }
  }, {
    tableName: 'related_products'
  });
};
