/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hosts', {
    id: {
      type: DataTypes.INTEGER(8),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    projectName: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'project_name'
    },
    host: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'host'
    },
    userId: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      field: 'user_id'
    },
    crawlLimit: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      field: 'crawl_limit'
    },
    filter: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'filter'
    },
    createdAt: {
      type: DataTypes.TIME,
      allowNull: true,
      field: 'created_at'
    },
    updatedAt: {
      type: DataTypes.TIME,
      allowNull: true,
      field: 'updated_at'
    }
  }, {
    tableName: 'hosts'
  });
};
