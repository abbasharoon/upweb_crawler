/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('referrers', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    fromUriId: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'from_uri_id'
    },
    toUriId: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'to_uri_id'
    },
    findingsId: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      field: 'findings_id'
    },
    scanId: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      field: 'scan_id'
    }
  }, {
    tableName: 'referrers',
    timestamps: false,
  });
};
