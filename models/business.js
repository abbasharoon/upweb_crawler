/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('business', {
    scanId: {
      type: DataTypes.INTEGER(9),

      primaryKey: true,
      field: 'scan_id'
    },
    address: {
      type: DataTypes.STRING,
      field: 'address'
    },
    phoneNumber: {
      type: DataTypes.STRING,
      field: 'phone_number'
    },
    tradingHours: {
      type: DataTypes.STRING,
      field: 'trading_hours'
    },
    shippingDetails: {
      type: DataTypes.INTEGER(11),
      field: 'shipping_details'
    },
    voucher: {
      type: "BLOB",
      field: 'voucher'
    },
    badge: {
      type: "BLOB",
      field: 'badge'
    },
  }, {
    tableName: 'business',
    timestamps: false,
  });
};
