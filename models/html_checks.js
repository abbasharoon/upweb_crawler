/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('htmlChecks', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    findingsId: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      field: 'findings_id'
    },
    uriId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'uri_id'
    },
    value: {
      type: DataTypes.STRING(500),
      allowNull: false,
      field: 'value',
    },
    scanId: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      field: 'scan_id'
    }
  }, {
    tableName: 'html_checks',
    timestamps: false,
  });
};
