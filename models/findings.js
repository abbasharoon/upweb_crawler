/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('findings', {
    id: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      primaryKey: true,
      field: 'id'
    },
    textId: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'text_id',
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'name'
    },
    description: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'description'
    }
  }, {
    tableName: 'findings'
  });
};

/**
 * 0 -
 */
