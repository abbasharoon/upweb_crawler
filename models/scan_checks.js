/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('scanChecks', {
    scanId: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      primaryKey: true,
      field: 'scan_id'
    },
    ssl: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      field: 'ssl'
    },
    cms: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'cms'
    },
    cartButtonSize: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'cart_button_size'
    },
    cartButtonColor: {
      type: DataTypes.STRING,
      allowNull: false,
      field: 'cart_button_color'
    },
    liveChat: {
      type: "BLOB",
      allowNull: false,
      field: 'live_chat'
    },
    productReviews: {
      type: "BLOB",
      allowNull: false,
      field: 'product_reviews'
    },
    wwwResolve: {
      type: "BLOB",
      allowNull: false,
      field: 'www_resolve'
    }
  }, {
    tableName: 'scan_checks',
    timestamps: false,
  });
};
