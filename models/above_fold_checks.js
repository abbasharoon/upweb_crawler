/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('aboveFoldChecks', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    searchBox: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      field: 'search_box',
    },
    title: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      field: 'title',
    },
    image: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      field: 'image',
    },
    description: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      field: 'description',
    },
    price: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      field: 'price',
    },
    addToCart: {
      type: DataTypes.INTEGER(3),
      allowNull: false,
      field: 'add_to_cart',
    },
    uriId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'uri_id',
    },
    scanId: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      field: 'scan_id',
    },
  }, {
    tableName: 'above_fold_checks',
    timestamps: false,
  });
};
