/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('sitemapsRobots', {
    scanId: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      primaryKey: true,
      field: 'scan_id'
    },
    robots: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      field: 'robots'
    },
    sitemapInRobots: {
      type: "BLOB",
      allowNull: true,
      field: 'sitemap_in_robots'
    },
    sitemap: {
      type: DataTypes.INTEGER(6),
      allowNull: true,
      field: 'sitemap'
    },
    sitemapWrong: {
      type: "BLOB",
      allowNull: true,
      field: 'sitemap_wrong'
    }
  }, {
    tableName: 'sitemaps_robots',
    timestamps: false,
  });
};
