/* jshint indent: 2 */

module.exports = function(sequelize, DataTypes) {
  return sequelize.define('paymentOptions', {
    scanId: {
      type: DataTypes.INTEGER(9),
      allowNull: false,
      primaryKey: true,
      field: 'scan_id'
    },
    paypal: {
      type: "BLOB",
      allowNull: false,
      field: 'paypal'
    },
    bank: {
      type: "BLOB",
      allowNull: false,
      field: 'bank'
    },
    creditCard: {
      type: "BLOB",
      allowNull: false,
      field: 'credit_card'
    },
    cash: {
      type: "BLOB",
      allowNull: false,
      field: 'cash'
    }
  }, {
    tableName: 'payment_options'
  });
};
