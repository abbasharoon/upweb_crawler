/* jshint indent: 2 */

module.exports = function (sequelize, DataTypes) {
  return sequelize.define('duplicateContents', {
    id: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    uriId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'uri_id'
    },
    matchedUriId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'matched_uri_id'
    },
    findingsId: {
      type: DataTypes.INTEGER(6),
      allowNull: false,
      field: 'findings_id',
    },
    value: {
      type: DataTypes.INTEGER(4),
      allowNull: false,
      field: 'value'
    },
    scanId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'scan_id'
    }
  }, {
    tableName: 'duplicate_contents',
    timestamps: false,
  });
};
