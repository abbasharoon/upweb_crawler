/* eslint-disable */
exports.data = {
  requiredTags: `
  <!DOCTYPE html>
    <html lang="en-US">
      <head>
	      <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Flying Ninja &#8211; Test Site</title>
        <meta name="description" content="WordPress 4.6.1" />
        <meta name="keywords" content="WooCommerce 2.6.13" />
      </head>
      <body>
        <h1></h1>
      </body>
  </html>`,
  missingRequiredTags: `
  <!DOCTYPE html>
    <html lang="en-US">
      <head>
	      <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Flying Ninja &#8211; Test Site</title>
        <!-- Missing Description Tag -->
        <!-- Missing keywords Tag -->
      </head>
      <body>
        <h1></h1>
      </body>
  </html>`,
  disallowedTags: `
  <!DOCTYPE html>
    <html lang="en-US">
      <head>
      </head>
      <body>
        <h1></h1>
        <object data="flash.swf">
        <object data="Should not be detected as flash">
        <iframe></iframe>
        <a href=""></a>
        <a href="" rel="notNoFollow"></a>
        <a href="" rel=""></a>
        <a href="" rel="nofollow"></a>
        <img>
        <img alt="">
        <img alt="Alt is present, it should not be disallowed">
      </body>
  </html>`,
  missingDisallowedTags: `
  <!DOCTYPE html>
    <html lang="en-US">
      <head>
      </head>
      <body>
        <h1></h1>
        <!-- all disallowed tags removed -->
        <!-- Flash tag is Missing -->
        <a href="" rel="nofollow"></a>
        <a href="" rel="nofollow"></a>
        <object data="Should not be detected as flash">
        <!-- iframe tag is missing -->
        <!-- img tag without alt tag or empty tag missing -->
        <img alt="Alt is present, it should not be disallowed">
      </body>
  </html>`,
  multipleTags: `
  <!DOCTYPE html>
    <html lang="en-US">
      <head>
      </head>
      <body>
      <h1></h1>
      <h1></h1>
      <h1></h1>
      </body>
  </html>`,
  noMultipleTags: `
  <!DOCTYPE html>
    <html lang="en-US">
      <head>
      </head>
      <body>
      <h1></h1>
      <a></a>
      </body>
  </html>`,

};

exports.dataForExtraction = {
  structuredData: `<!doctype html>
<html lang="en-US" prefix="og: http://ogp.me/ns#" class="no-js">
<head>
    <meta charset="UTF-8">
    <title>LIVfit Superfood® Organic Superfood Blend Powder- 360g bag</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<div itemscope itemtype="http://schema.org/Product">
    <div class="summary-container" style="height:auto;width: 100vw;">
        <div class="summary entry-summary">
            <div class="mobile-product-image">
                <div class="images">
                    <a itemprop="image">
                        <img src="https://www.betterbodyfoods.com/wp-content/uploads/2016/10/bbf-superfood-blend-464x650.png"/>
                    </a>
                </div>
            </div>
            <h1 itemprop="name">
                LIVfit Superfood® Organic Superfood Blend
            </h1>
            <div class="woocommerce-product-rating" itemprop="aggregateRating" itemscope
                 itemtype="http://schema.org/AggregateRating">
			<span style="width:100%">
				<strong itemprop="ratingValue" class="rating">5</strong> out of <span itemprop="bestRating">5</span>				based on <span
                    itemprop="ratingCount" class="rating">6</span> customer ratings			</span>
            </div>
            <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
                <meta itemprop="price" content="10.98"/>
                <meta itemprop="priceCurrency" content="USD"/>
                <link itemprop="availability" href="http://schema.org/InStock"/>
            </div>
            <div itemprop="Description">
                <p class="p1">Need a nutrition boost to go? Got picky eaters? This flavor-neutral power pack of
                    nutrients is your new best friend. Made from whole fruit and vegetable concentrates, LIVfit
                    Superfood scoops into smoothies, spaghetti sauces, yogurts and pretty much anything you or your
                    family eat. Its neutral flavor means they’ll never suspect you’ve boosted the nutrition. Don’t
                    worry, we’ll keep your secret.</p>
                <h3 class="p1">Regular Prices:</h3>
                <p><strong>• 12.7 oz</strong><br/>
                    <span style="text-decoration: line-through;">Reg: $16.48</span>  <span style="color: #00a68d;">Sale: $14.48</span>
                </p>
                <p><strong>• Single Serving Packets (8-ct)</strong><br/>
                    Reg: $10.98</p>
                <h3 class="p1"></h3>
            </div>


            <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "url": "http://test-5791.myshopify.com/products/example-t-shirt",
  "name": "Example T-Shirt",
  "image": "//cdn.shopify.com/s/files/1/1034/2545/products/shopify_shirt.png?v=1445540737",
  "description": "This is the description for variant Lithograph - Height: 9&quot; x Width, id: 7397573253",

  "category": [
    "http://test-5791.myshopify.com/collections/test-collection-2","http://test-5791.myshopify.com/collections/test-collection-3"
  ],

  "brand": {
    "name": "Acme"
  },
  "offers": {
    "@type": "Offer",
    "priceCurrency": "RON",
    "price": "25,00 lei",
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "test"
    }
  }
}





            
            
            </script>
            <div prefix="
 rdf: http://www.w3.org/1999/02/22-rdf-syntax-ns#
  foaf: http://xmlns.com/foaf/0.1/
  gr: http://purl.org/goodrelations/v1#
  xsd: http://www.w3.org/2001/XMLSchema#"
                 typeof="gr:Offering">
                <div>
                    <h1 property="gr:name">Canon Rebel T2i (EOS 550D)</h1>
                    <div rel="foaf:depiction">
                        <img style="float:left; width:20%"
                             src="http://shop.usa.canon.com/wcsstore/eStore/images/t2ikit_1_l.jpg"/>
                    </div>
                    <p property="gr:description">
                        The Canon Rebel T2i (EOS 550D) is Cannon's top-of-the-line consumer digital SLR camera.
                        It can shoot up to 18 megapixel resolution photos and features an ISO range of 100-6400.
                    </p>
                    <link rel="gr:hasBusinessFunction" href="http://purl.org/goodrelations/v1#Sell"/>
                    <meta property="gr:hasEAN_UCC-13" content="013803123784"/>
                    Sale price:
                    <span property="gr:hasPriceSpecification" typeof="gr:UnitPriceSpecification">
      <span property="gr:hasCurrency" content="USD">$</span>
      <span property="gr:hasCurrencyValue" datatype="xsd:float">899</span>
    </span>
                    <link rel="gr:acceptedPaymentMethods" href="http://purl.org/goodrelations/v1#PayPal"/>
                    <link rel="gr:acceptedPaymentMethods" href="http://purl.org/goodrelations/v1#MasterCard"/>
                    [<a rel="foaf:page" href="http://shop.usa.canon.com/">more...</a>]
                </div>
            </div>

            <span class="onsale">Sale!</span>
            <div class="images">
                <a href="https://www.betterbodyfoods.com/wp-content/uploads/2016/10/bbf-superfood-blend.png"
                   itemprop="image"><img
                        src="https://www.betterbodyfoods.com/wp-content/uploads/2016/10/bbf-superfood-blend-464x650.png"/></a>
                <li itemprop="review" itemscope itemtype="http://schema.org/Review"
                    class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-1088">

                    <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
                                                        <span style="width:100%"><strong
                                                                itemprop="ratingValue">5</strong> out of 5</span>
                    </div>


                    <p class="meta">
                        <strong itemprop="author">Kay Marchand</strong> &ndash;
                        <time itemprop="datePublished"
                              datetime="2016-12-25T20:53:39+00:00">25 December 2016
                        </time>
                        :
                    </p>

                    <div itemprop="description" class="description"><p>I received a bag
                        in my Christmas stocking and wow am I ever impressed! I am
                        thrilled that I have finally found a vegan product without
                        sweeteners of any kind! This mixed well in savory and sweet
                        foods (sauces &amp; smoothies) and has an amazing price.</p>
                    </div>
            </div>
        </div>
        </li><!-- #comment-## -->
        <li itemprop="review" itemscope itemtype="http://schema.org/Review"
            class="comment even thread-even depth-1" id="li-comment-1355">

            <div id="comment-1355" class="comment_container">

                <img alt=''
                     src='https://secure.gravatar.com/avatar/126d6845d3df075add053ae9f2417fe9?s=60&#038;d=mm&#038;r=g'
                     srcset='https://secure.gravatar.com/avatar/126d6845d3df075add053ae9f2417fe9?s=120&amp;d=mm&amp;r=g 2x'
                     class='avatar avatar-60 photo' height='60' width='60'/>
                <div class="comment-text">


                    <div itemprop="reviewRating" itemscope
                         itemtype="http://schema.org/Rating" class="star-rating"
                         title="Rated 5 out of 5">
                                                        <span style="width:100%"><strong
                                                                itemprop="ratingValue">5</strong> out of 5</span>
                    </div>


                    <p class="meta">
                        <strong itemprop="author">Terrie Villegas</strong> &ndash;
                        <time itemprop="datePublished"
                              datetime="2017-01-07T16:47:35+00:00">7 January 2017
                        </time>
                        :
                    </p>

                    <div itemprop="description" class="description"><p>I&#8217;ve been
                        adding it to my berries and yogurt and it hasn&#8217;t change
                        the flavor today I tried my own version the power upbar and I
                        can&#8217;t believe how wonderful it is. The Taste is neutral
                        and it mixes well.</p>
                    </div>
                </div>
            </div>
        </li><!-- #comment-## -->
        <li itemprop="review" itemscope itemtype="http://schema.org/Review"
            class="comment odd alt thread-odd thread-alt depth-1" id="li-comment-1390">

            <div id="comment-1390" class="comment_container">

                <img alt=''
                     src='https://secure.gravatar.com/avatar/050ef0f17e2b1dd1bef1556ef49c7849?s=60&#038;d=mm&#038;r=g'
                     srcset='https://secure.gravatar.com/avatar/050ef0f17e2b1dd1bef1556ef49c7849?s=120&amp;d=mm&amp;r=g 2x'
                     class='avatar avatar-60 photo' height='60' width='60'/>
                <div class="comment-text">


                    <div itemprop="reviewRating" itemscope
                         itemtype="http://schema.org/Rating" class="star-rating"
                         title="Rated 5 out of 5">
                                                        <span style="width:100%"><strong
                                                                itemprop="ratingValue">5</strong> out of 5</span>
                    </div>


                    <p class="meta">
                        <strong itemprop="author">Amber</strong> &ndash;
                        <time itemprop="datePublished"
                              datetime="2017-01-08T14:06:13+00:00">8 January 2017
                        </time>
                        :
                    </p>

                    <div itemprop="description" class="description"><p>I also purchased
                        at Costco. This stuff rocks. I even put it in the kids pancake
                        batter and they love it. Great way to pack in superfoods!
                        Amazing ingredients. So happy. A new staple in my kitchen.</p>
                    </div>
                </div>
            </div>
            <ul class="children">
                <li itemprop="review" itemscope itemtype="http://schema.org/Review"
                    class="comment byuser comment-author-joshuaallan even depth-2"
                    id="li-comment-1533">

                    <div id="comment-1533" class="comment_container">

                        <img alt=''
                             src='https://secure.gravatar.com/avatar/c28e8d6404827db382fb65b874785780?s=60&#038;d=mm&#038;r=g'
                             srcset='https://secure.gravatar.com/avatar/c28e8d6404827db382fb65b874785780?s=120&amp;d=mm&amp;r=g 2x'
                             class='avatar avatar-60 photo' height='60' width='60'/>
                        <div class="comment-text">


                            <p class="meta">
                                <strong itemprop="author">Dani Mann</strong> &ndash;
                                <time itemprop="datePublished"
                                      datetime="2017-01-12T10:46:34+00:00">12 January
                                    2017
                                </time>
                                :
                            </p>

                            <div itemprop="description" class="description"><p>So glad
                                that you like it, Amber! Have you tried any of our other
                                LIVfit Superfood® products? Give them a try
                                sometime!</p>
                            </div>
                        </div>
                    </div>
                </li><!-- #comment-## -->
            </ul><!-- .children -->
        </li><!-- #comment-## -->
        <li itemprop="review" itemscope itemtype="http://schema.org/Review"
            class="comment odd alt thread-even depth-1" id="li-comment-1740">

            <div id="comment-1740" class="comment_container">

                <img alt=''
                     src='https://secure.gravatar.com/avatar/eca135c368d16b74f32d4e6b0c1fd4ea?s=60&#038;d=mm&#038;r=g'
                     srcset='https://secure.gravatar.com/avatar/eca135c368d16b74f32d4e6b0c1fd4ea?s=120&amp;d=mm&amp;r=g 2x'
                     class='avatar avatar-60 photo' height='60' width='60'/>
                <div class="comment-text">


                    <div itemprop="reviewRating" itemscope
                         itemtype="http://schema.org/Rating" class="star-rating"
                         title="Rated 5 out of 5">
                                                        <span style="width:100%"><strong
                                                                itemprop="ratingValue">5</strong> out of 5</span>
                    </div>


                    <p class="meta">
                        <strong itemprop="author">patrick looby</strong> &ndash;
                        <time itemprop="datePublished"
                              datetime="2017-01-18T02:13:37+00:00">18 January 2017
                        </time>
                        :
                    </p>

                    <div itemprop="description" class="description"><p>l got it at
                        walmart surprised no flavors,must mean healthy, been taking it
                        two days now and feel great! lam hooked,going to try the peanut
                        butter next. thank you for areal great product!</p>
                    </div>
                </div>
            </div>
        </li><!-- #comment-## -->
        <li itemprop="review" itemscope itemtype="http://schema.org/Review"
            class="comment byuser comment-author-just-terri-home even thread-odd thread-alt depth-1"
            id="li-comment-1751">

            <div id="comment-1751" class="comment_container">

                <img alt=''
                     src='https://secure.gravatar.com/avatar/9430848aad30b5374329147e27253113?s=60&#038;d=mm&#038;r=g'
                     srcset='https://secure.gravatar.com/avatar/9430848aad30b5374329147e27253113?s=120&amp;d=mm&amp;r=g 2x'
                     class='avatar avatar-60 photo' height='60' width='60'/>
                <div class="comment-text">


                    <div itemprop="reviewRating" itemscope
                         itemtype="http://schema.org/Rating" class="star-rating"
                         title="Rated 5 out of 5">
                                                        <span style="width:100%"><strong
                                                                itemprop="ratingValue">5</strong> out of 5</span>
                    </div>


                    <p class="meta">
                        <strong itemprop="author">Teresa</strong> <em class="verified">(verified
                        owner)</em> &ndash;
                        <time itemprop="datePublished"
                              datetime="2017-01-18T10:47:29+00:00">18 January 2017
                        </time>
                        :
                    </p>

                    <div itemprop="description" class="description"><p>Saw this at
                        Walmart and decided to pick it up. Easy to use; great flavor.
                        Then I decided to add it to ground chicken to make jerky treats
                        for my pups (adds nutrients). What a success! Love this
                        stuff!!!</p>
                    </div>
                </div>
            </div>
        </li><!-- #comment-## -->
        </ol>


    </div>


    <div class="clear"></div>
</div>
</div>
</div>


<meta itemprop="url" content="https://www.betterbodyfoods.com/product/livfit-superfood-organic-superfood-blend/"/>

</div><!-- #product-609 -->


</div></div>



</body>
</html>`,
  jsonldData: `
<head>
<title>Dummy Title</title>
<meta name="description">meta data</meta>
</head>
<body>
            <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "url": "http://test-5791.myshopify.com/products/example-t-shirt",
  "name": "Example T-Shirt",
  "image": "//cdn.shopify.com/s/files/1/1034/2545/products/shopify_shirt.png?v=1445540737",
  "description": "This is the description for variant Lithograph - Height: 9&quot; x Width, id: 7397573253",

  "category": [
    "http://test-5791.myshopify.com/collections/test-collection-2","http://test-5791.myshopify.com/collections/test-collection-3"
  ],

  "brand": {
    "name": "Acme"
  },
  "offers": {
    "@type": "Offer",
    "priceCurrency": "RON",
    "price": "25,00 lei",
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "test"
    }
  }
}
</script>
</body>`,
  withoutTitleDescription: `
<head>
</head>
<body>
            <script type="application/ld+json">
{
  "@context": "http://schema.org/",
  "@type": "Product",
  "url": "http://test-5791.myshopify.com/products/example-t-shirt",
  "name": "Example T-Shirt",
  "image": "//cdn.shopify.com/s/files/1/1034/2545/products/shopify_shirt.png?v=1445540737",
  "description": "This is the description for variant Lithograph - Height: 9&quot; x Width, id: 7397573253",

  "category": [
    "http://test-5791.myshopify.com/collections/test-collection-2","http://test-5791.myshopify.com/collections/test-collection-3"
  ],

  "brand": {
    "name": "Acme"
  },
  "offers": {
    "@type": "Offer",
    "priceCurrency": "RON",
    "price": "25,00 lei",
    "availability": "http://schema.org/InStock",
    "seller": {
      "@type": "Organization",
      "name": "test"
    }
  }
}
</script>
</body>`,
  microdata: `<div itemscope itemtype="http://schema.org/Product">
  <span itemprop="name">Kenmore White 17" Microwave</span>
  <img itemprop="image" src="kenmore-microwave-17in.jpg" alt='Kenmore 17" Microwave' />
  <div itemprop="aggregateRating"
    itemscope itemtype="http://schema.org/AggregateRating">
   Rated <span itemprop="ratingValue">3.5</span>/5
   based on <span itemprop="reviewCount">11</span> customer reviews
  </div>
  <div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
    <!--price is 1000, a number, with locale-specific thousands separator
    and decimal mark, and the $ character is marked up with the
    machine-readable code "USD" -->
    <span itemprop="priceCurrency" content="USD">$</span><span
          itemprop="price" content="1000.00">1,000.00</span>
    <link itemprop="availability" href="http://schema.org/InStock" />In stock
  </div>
  Product description:
    
    <!--
    Having multiple descriptions but the data extractor should return only the first one-->
  <span itemprop="description">0.7 cubic feet countertop microwave.
  Has six preset cooking categories and convenience features like
  Add-A-Minute and Child Lock.</span>
   <span itemprop="description">0.7 cubic feet countertop microwave.
  Has six preset cooking categories and convenience features like
  Add-A-Minute and Child Lock.</span> 
  <span itemprop="description">0.7 cubic feet countertop microwave.
  Has six preset cooking categories and convenience features like
  Add-A-Minute and Child Lock.</span> 
  <span itemprop="description">0.7 cubic feet countertop microwave.
  Has six preset cooking categories and convenience features like
  Add-A-Minute and Child Lock.</span>
  Customer reviews:
  <div itemprop="review" itemscope itemtype="http://schema.org/Review">
    <span itemprop="name">Not a happy camper</span> -
    by <span itemprop="author">Ellie</span>,
    <meta itemprop="datePublished" content="2011-04-01">April 1, 2011
    <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
      <meta itemprop="worstRating" content = "1">
      <span itemprop="ratingValue">1</span>/
      <span itemprop="bestRating">5</span>stars
    </div>
    <span itemprop="description">The lamp burned out and now I have to replace
    it. </span>
  </div>
  <div itemprop="review" itemscope itemtype="http://schema.org/Review">
    <span itemprop="name">Value purchase</span> -
    by <span itemprop="author">Lucas</span>,
    <meta itemprop="datePublished" content="2011-03-25">March 25, 2011
    <div itemprop="reviewRating" itemscope itemtype="http://schema.org/Rating">
      <meta itemprop="worstRating" content = "1"/>
      <span itemprop="ratingValue">4</span>/
      <span itemprop="bestRating">5</span>stars
    </div>
    <span itemprop="description">Great microwave for the price. It is small and
    fits in my apartment.</span>
  </div>
</div>`,
  htmlForGuess: `<!doctype html>
<head>

<title>Stone Island Sweat Shirt (Military Green) 661565640.V0054</title>
<meta name="description" content="Bestellt das Stone Island Sweat Shirt (Military Green) und viele andere Dinge auf allikestore.com"/>
</head>
<body class="  catalog-product-view grid-fluid catalog-product-view product-stone-island-sweat-shirt-military-green-661565640-v0054">

<div class="wrapper">
        <noscript>
        <div class="global-site-notice noscript">
            <div class="notice-inner">
                <p>
                    <strong>JavaScript scheint in Ihrem Browser deaktiviert zu sein.</strong><br />
                    Sie müssen JavaScript in Ihrem Browser aktivieren um alle Funktionen dieser Webseite nutzen zu können.                </p>
            </div>
        </div>
    </noscript>
    <div class="page">
        <!-- ESI START DUMMY COMMENT [] -->
<!-- ESI URL DUMMY COMMENT -->

 	<div class="header-container full-header ">
		<div class="header header-2 row">
			<div class="grid_18">
				<div class="table-container">
					<div class="table-cell v-align-cell logo-container">
						<div class="logo-container-indent">
															<h1 class="logo">
									<strong>Allike Store</strong>
									<a href="https://www.allikestore.com/german/" title="Allike Store">
										<p><img src="https://www.allikestore.com/media/wysiwyg/olegnax/logo.gif" alt="Allike" /></p>									</a>
								</h1>
													</div>
					</div>
										<div class="table-cell header-info-container">
						<div class="relative">
							<div class="top-links-container ">
										<div class="header-switch header-language">
    	<span class="header-switch-trigger">
		    <span class="icon" style="background-image:url('https://www.allikestore.com/skin/frontend/athlete/default/images/flags/german.png')">
				Deutsch			</span>
		</span>
		<div class="header-dropdown">
			<ul><li class="selected"><a style="background-image:url('https://www.allikestore.com/skin/frontend/athlete/default/images/flags/german.png');" href="https://www.allikestore.com/german/stone-island-sweat-shirt-military-green-661565640-v0054.html?___from_store=german">Deutsch</a></li><li><a style="background-image:url('https://www.allikestore.com/skin/frontend/athlete/default/images/flags/default.png');" href="https://www.allikestore.com/default/stone-island-sweat-shirt-military-green-661565640-v0054.html?___from_store=german">English</a></li></ul>
		</div>
	</div>



<div class="header-switch header-currency">
    <span class="header-switch-trigger">
        <span class="current">EUR</span>
    </span>
    <div class="header-dropdown">
        <ul><li><a href="https://www.allikestore.com/german/directory/currency/switch/currency/AUD/uenc/aHR0cHM6Ly93d3cuYWxsaWtlc3RvcmUuY29tL2dlcm1hbi9zdG9uZS1pc2xhbmQtc3dlYXQtc2hpcnQtbWlsaXRhcnktZ3JlZW4tNjYxNTY1NjQwLXYwMDU0Lmh0bWw,/" >Australischer Dollar - AUD</a></li><li><a href="https://www.allikestore.com/german/directory/currency/switch/currency/GBP/uenc/aHR0cHM6Ly93d3cuYWxsaWtlc3RvcmUuY29tL2dlcm1hbi9zdG9uZS1pc2xhbmQtc3dlYXQtc2hpcnQtbWlsaXRhcnktZ3JlZW4tNjYxNTY1NjQwLXYwMDU0Lmh0bWw,/" >Britisches Pfund Sterling - GBP</a></li><li><a href="https://www.allikestore.com/german/directory/currency/switch/currency/EUR/uenc/aHR0cHM6Ly93d3cuYWxsaWtlc3RvcmUuY29tL2dlcm1hbi9zdG9uZS1pc2xhbmQtc3dlYXQtc2hpcnQtbWlsaXRhcnktZ3JlZW4tNjYxNTY1NjQwLXYwMDU0Lmh0bWw,/"  class="selected">Euro - EUR</a></li><li><a href="https://www.allikestore.com/german/directory/currency/switch/currency/JPY/uenc/aHR0cHM6Ly93d3cuYWxsaWtlc3RvcmUuY29tL2dlcm1hbi9zdG9uZS1pc2xhbmQtc3dlYXQtc2hpcnQtbWlsaXRhcnktZ3JlZW4tNjYxNTY1NjQwLXYwMDU0Lmh0bWw,/" >Japanischer Yen - JPY</a></li><li><a href="https://www.allikestore.com/german/directory/currency/switch/currency/CAD/uenc/aHR0cHM6Ly93d3cuYWxsaWtlc3RvcmUuY29tL2dlcm1hbi9zdG9uZS1pc2xhbmQtc3dlYXQtc2hpcnQtbWlsaXRhcnktZ3JlZW4tNjYxNTY1NjQwLXYwMDU0Lmh0bWw,/" >Kanadischer Dollar - CAD</a></li><li><a href="https://www.allikestore.com/german/directory/currency/switch/currency/RUB/uenc/aHR0cHM6Ly93d3cuYWxsaWtlc3RvcmUuY29tL2dlcm1hbi9zdG9uZS1pc2xhbmQtc3dlYXQtc2hpcnQtbWlsaXRhcnktZ3JlZW4tNjYxNTY1NjQwLXYwMDU0Lmh0bWw,/" >Russischer Rubel - RUB</a></li><li><a href="https://www.allikestore.com/german/directory/currency/switch/currency/CHF/uenc/aHR0cHM6Ly93d3cuYWxsaWtlc3RvcmUuY29tL2dlcm1hbi9zdG9uZS1pc2xhbmQtc3dlYXQtc2hpcnQtbWlsaXRhcnktZ3JlZW4tNjYxNTY1NjQwLXYwMDU0Lmh0bWw,/" >Schweizer Franken - CHF</a></li><li><a href="https://www.allikestore.com/german/directory/currency/switch/currency/USD/uenc/aHR0cHM6Ly93d3cuYWxsaWtlc3RvcmUuY29tL2dlcm1hbi9zdG9uZS1pc2xhbmQtc3dlYXQtc2hpcnQtbWlsaXRhcnktZ3JlZW4tNjYxNTY1NjQwLXYwMDU0Lmh0bWw,/" >US-Dollar - USD</a></li></ul>
    </div>
</div>
<!-- cart BOF -->
<div class="header-switch header-cart">
	<a class="header-switch-trigger summary icon-black" href="https://www.allikestore.com/german/checkout/cart/">
		<span>Warenkorb</span><span class="qty">0</span>	</a>
	<div class="header-dropdown">
									<p class="a-center">Du hast keine Artikel im Warenkorb.</p>
			</div>
</div>
<!-- cart EOF -->								<div class="top-links"><ul class="links">
                     <li class="first" ><a href="https://www.allikestore.com/german/customer/account/" title="Konto" >Konto</a></li>
                                <li ><a href="https://www.allikestore.com/german/wishlist/" title="Wunschzettel" >Wunschzettel</a></li>
                                <li ><a href="https://www.allikestore.com/german/checkout/" title="Kasse" class="top-link-checkout">Kasse</a></li>
                                <li class=" last" ><a href="https://www.allikestore.com/german/customer/account/login/" title="Anmelden" >Anmelden</a></li>
            <!--     <li><div class="fb-like" style="margin-bottom:-5px;" data-href="https://www.facebook.com/AllikeStore" data-width="50" data-height="35" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div></li> -->
</ul>
</div>
								<div class="clear"></div>
							</div>
							<div class="nav-search-container search-visible">
								<form id="search_mini_form" action="https://www.allikestore.com/german/catalogsearch/result/" method="get">
    <div class="form-search">
	    <button type="submit" title="Suche" class="button icon-black"><span><span>Suche</span></span></button>
	    <label for="search">Suche:</label>
        <div class="input-overflow"><input id="search" type="text" name="q" value="" class="input-text" maxlength="128" /></div>
        <div id="search_autocomplete" class="search-autocomplete"></div>
        <script type="text/javascript">
        //<![CDATA[
            var searchForm = new Varien.searchForm('search_mini_form', 'search', 'Den gesamten Shop durchsuchen...');
            searchForm.initAutocomplete('https://www.allikestore.com/autocomplete.php?store=german&fallback_url=https://www.allikestore.com/german/catalogsearch/ajax/suggest/', 'search_autocomplete');
        //]]>
        </script>
    </div>
</form>
																									<div class="nav-container header-nav-txt std">
																			</div>
															</div>
						</div>
					</div>
				</div>
									<div class="header-nav-wide">		<div class="nav-container olegnaxmegamenu icons-black">
		<div class="nav-top-title"><div class="icon"><span></span><span></span><span></span></div><a href="#">Navigation</a></div>
		<ul id="nav">
						<li  class="level0 nav-1 first  level-top "><a href="https://www.allikestore.com/german/new-arrivals-sneakers.html" ><span>Neuheiten</span></a></li><li id="brands-menu" class="level0 nav-7 level-top  parent "><a href="https://www.allikestore.com/german/brands/" class="level0"><span>Marken</span></a><ul class="level0"><li class="level1 nav-7-1 first ">
                                <a class="" href="https://www.allikestore.com/german/adidas.html">
                                <span>adidas</span></a></li><li class="level1 nav-7-2  ">
                                <a class="" href="https://www.allikestore.com/german/adidas-skateboarding.html">
                                <span>adidas Skateboarding</span></a></li><li class="level1 nav-7-3  ">
                                <a class="" href="https://www.allikestore.com/german/adidas-x-palace.html">
                                <span>adidas x Palace</span></a></li><li class="level1 nav-7-4  ">
                                <a class="" href="https://www.allikestore.com/german/adidas-x-pharrell-williams.html">
                                <span>adidas x Pharrell Williams</span></a></li><li class="level1 nav-7-5  ">
                                <a class="" href="https://www.allikestore.com/german/all-good-everything.html">
                                <span>All Good Everything</span></a></li><li class="level1 nav-7-6  ">
                                <a class="" href="https://www.allikestore.com/german/ambivalent-berlin.html">
                                <span>Ambivalent Berlin</span></a></li><li class="level1 nav-7-7  ">
                                <a class="" href="https://www.allikestore.com/german/aries.html">
                                <span>Aries</span></a></li><li class="level1 nav-7-8  ">
                                <a class="" href="https://www.allikestore.com/german/asics.html">
                                <span>Asics</span></a></li><li class="level1 nav-7-9  ">
                                <a class="" href="https://www.allikestore.com/german/bellroy.html">
                                <span>Bellroy</span></a></li><li class="level1 nav-7-10  ">
                                <a class="" href="https://www.allikestore.com/german/burlington.html">
                                <span>Burlington</span></a></li><li class="level1 nav-7-11  ">
                                <a class="" href="https://www.allikestore.com/german/clae-footwear.html">
                                <span>Clae Footwear</span></a></li><li class="level1 nav-7-12  ">
                                <a class="" href="https://www.allikestore.com/german/converse.html">
                                <span>Converse</span></a></li><li class="level1 nav-7-13  ">
                                <a class="" href="https://www.allikestore.com/german/edwin-denim.html">
                                <span>Edwin Denim</span></a></li><li class="level1 nav-7-14  ">
                                <a class="" href="https://www.allikestore.com/german/elka.html">
                                <span>Elka</span></a></li><li class="level1 nav-7-15  ">
                                <a class="" href="https://www.allikestore.com/german/epokhe.html">
                                <span>Epokhe</span></a></li><li class="level1 nav-7-16  ">
                                <a class="" href="https://www.allikestore.com/german/falke.html">
                                <span>Falke</span></a></li><li class="level1 nav-7-17  ">
                                <a class="" href="https://www.allikestore.com/german/filling-pieces.html">
                                <span>Filling Pieces</span></a></li><li class="level1 nav-7-18  ">
                                <a class="" href="https://www.allikestore.com/german/casio-g-shock.html">
                                <span>G-Shock</span></a></li><li class="level1 nav-7-19  ">
                                <a class="" href="https://www.allikestore.com/german/gestalten.html">
                                <span>Gestalten</span></a></li><li class="level1 nav-7-20  ">
                                <a class="" href="https://www.allikestore.com/german/grado.html">
                                <span>Grado</span></a></li><li class="level1 nav-7-21  ">
                                <a class="" href="https://www.allikestore.com/german/halo.html">
                                <span>Halo</span></a></li><li class="level1 nav-7-22  ">
                                <a class="" href="https://www.allikestore.com/german/han-kjobenhavn.html">
                                <span>Han Kjobenhavn</span></a></li><li class="level1 nav-7-23  ">
                                <a class="" href="https://www.allikestore.com/german/henrik-vibskov.html">
                                <span>Henrik Vibskov</span></a></li><li class="level1 nav-7-24  ">
                                <a class="" href="https://www.allikestore.com/german/hypebeast.html">
                                <span>Hypebeast</span></a></li><li class="level1 nav-7-25  ">
                                <a class="" href="https://www.allikestore.com/german/item-m6.html">
                                <span>ITEM m6</span></a></li><li class="level1 nav-7-26  ">
                                <a class="" href="https://www.allikestore.com/german/jordan-brand.html">
                                <span>Jordan Brand</span></a></li><li class="level1 nav-7-27  ">
                                <a class="" href="https://www.allikestore.com/german/journal.html">
                                <span>Journal</span></a></li><li class="level1 nav-7-28  ">
                                <a class="" href="https://www.allikestore.com/german/juniper-ridge.html">
                                <span>Juniper Ridge</span></a></li><li class="level1 nav-7-29  ">
                                <a class="" href="https://www.allikestore.com/german/karhu.html">
                                <span>Karhu</span></a></li><li class="level1 nav-7-30  ">
                                <a class="" href="https://www.allikestore.com/german/levis-made-and-crafted.html">
                                <span>Levis Made and Crafted</span></a></li><li class="level1 nav-7-31  ">
                                <a class="" href="https://www.allikestore.com/german/lexdray.html">
                                <span>Lexdray</span></a></li><li class="level1 nav-7-32  ">
                                <a class="" href="https://www.allikestore.com/german/libertine-libertine.html">
                                <span>Libertine-Libertine</span></a></li><li class="level1 nav-7-33  ">
                                <a class="" href="https://www.allikestore.com/german/lodown-mag.html">
                                <span>Lodown Mag</span></a></li><li class="level1 nav-7-34  ">
                                <a class="" href="https://www.allikestore.com/german/marine-machine.html">
                                <span>Marine Machine</span></a></li><li class="level1 nav-7-35  ">
                                <a class="" href="https://www.allikestore.com/german/medicom.html">
                                <span>Medicom</span></a></li><li class="level1 nav-7-36  ">
                                <a class="" href="https://www.allikestore.com/german/new-balance.html">
                                <span>New Balance</span></a></li><li class="level1 nav-7-37  ">
                                <a class="" href="https://www.allikestore.com/german/nike-quickstrike.html">
                                <span>Nike Quickstrike</span></a></li><li class="level1 nav-7-38  ">
                                <a class="" href="https://www.allikestore.com/german/nike-sb.html">
                                <span>Nike SB</span></a></li><li class="level1 nav-7-39  ">
                                <a class="" href="https://www.allikestore.com/german/nike-sportswear.html">
                                <span>Nike Sportswear</span></a></li><li class="level1 nav-7-40  ">
                                <a class="" href="https://www.allikestore.com/german/norse-projects.html">
                                <span>Norse Projects</span></a></li><li class="level1 nav-7-41  ">
                                <a class="" href="https://www.allikestore.com/german/our-legacy.html">
                                <span>Our Legacy</span></a></li><li class="level1 nav-7-42  ">
                                <a class="" href="https://www.allikestore.com/german/palace.html">
                                <span>Palace</span></a></li><li class="level1 nav-7-43  ">
                                <a class="" href="https://www.allikestore.com/german/pendleton.html">
                                <span>Pendleton</span></a></li><li class="level1 nav-7-44  ">
                                <a class="" href="https://www.allikestore.com/german/penfield.html">
                                <span>Penfield</span></a></li><li class="level1 nav-7-45  ">
                                <a class="" href="https://www.allikestore.com/german/porter-by-yoshida.html">
                                <span>Porter by Yoshida</span></a></li><li class="level1 nav-7-46  ">
                                <a class="" href="https://www.allikestore.com/german/publish.html">
                                <span>Publish</span></a></li><li class="level1 nav-7-47  ">
                                <a class="" href="https://www.allikestore.com/german/puma.html">
                                <span>Puma</span></a></li><li class="level1 nav-7-48  ">
                                <a class="" href="https://www.allikestore.com/german/raised-by-wolves.html">
                                <span>Raised by Wolves</span></a></li><li class="level1 nav-7-49  ">
                                <a class="" href="https://www.allikestore.com/german/reebok.html">
                                <span>Reebok</span></a></li><li class="level1 nav-7-50  ">
                                <a class="" href="https://www.allikestore.com/german/retrosuperfuture.html">
                                <span>Retrosuperfuture</span></a></li><li class="level1 nav-7-51  ">
                                <a class="" href="https://www.allikestore.com/german/saucony.html">
                                <span>Saucony</span></a></li><li class="level1 nav-7-52  ">
                                <a class="" href="https://www.allikestore.com/german/sex-skateboards.html">
                                <span>Sex Skateboards</span></a></li><li class="level1 nav-7-53  ">
                                <a class="" href="https://www.allikestore.com/german/soulland.html">
                                <span>Soulland</span></a></li><li class="level1 nav-7-54  ">
                                <a class="" href="https://www.allikestore.com/german/stance.html">
                                <span>Stance</span></a></li><li class="level1 nav-7-55  ">
                                <a class="" href="https://www.allikestore.com/german/stone-island.html">
                                <span>Stone Island</span></a></li><li class="level1 nav-7-56  ">
                                <a class="" href="https://www.allikestore.com/german/stone-island-shadow-project.html">
                                <span>Stone Island Shadow Project</span></a></li><li class="level1 nav-7-57  ">
                                <a class="" href="https://www.allikestore.com/german/surface-to-air.html">
                                <span>Surface To Air</span></a></li><li class="level1 nav-7-58  ">
                                <a class="" href="https://www.allikestore.com/german/the-fourness.html">
                                <span>The Fourness</span></a></li><li class="level1 nav-7-59  ">
                                <a class="" href="https://www.allikestore.com/german/vans.html">
                                <span>Vans</span></a></li><li class="level1 nav-7-60  ">
                                <a class="" href="https://www.allikestore.com/german/won-hundred.html">
                                <span>Won Hundred</span></a></li><li class="level1 nav-7-61  ">
                                <a class="" href="https://www.allikestore.com/german/wood-wood.html">
                                <span>Wood Wood</span></a></li><li class="level1 nav-7-62 last ">
                                <a class="" href="https://www.allikestore.com/german/y-3.html">
                                <span>Y-3</span></a></li></ul><li  class="level0 nav-2  level-top  parent"><a href="https://www.allikestore.com/german/sneakers.html" ><span>Sneaker</span></a><div class="megamenu-dropdown"><ul class="level0"><li  class="level1 nav-2-1 first"><a href="https://www.allikestore.com/german/sneakers/mens-sneakers.html" ><span>Men</span></a></li><li  class="level1 nav-2-2"><a href="https://www.allikestore.com/german/sneakers/womens-sneakers.html" ><span>Women</span></a></li><li  class="level1 nav-2-3"><a href="https://www.allikestore.com/german/sneakers/kids-sneakers.html" ><span>Kids</span></a></li><li  class="level1 nav-2-4 last"><a href="https://www.allikestore.com/german/sneakers/limited-editions-sneakers.html" ><span>Limited Editions</span></a></li></ul><div class="clear"></div></div></li><li  class="level0 nav-3  level-top  parent"><a href="https://www.allikestore.com/german/bekleidung.html" ><span>Bekleidung</span></a><div class="megamenu-dropdown"><ul class="level0"><li  class="level1 nav-3-1 first"><a href="https://www.allikestore.com/german/bekleidung/mens-tops.html" ><span>Mens Tops</span></a></li><li  class="level1 nav-3-2"><a href="https://www.allikestore.com/german/bekleidung/pants.html" ><span>Mens Bottoms</span></a></li><li  class="level1 nav-3-3"><a href="https://www.allikestore.com/german/bekleidung/womens-tops.html" ><span>Womens Tops</span></a></li><li  class="level1 nav-3-4"><a href="https://www.allikestore.com/german/bekleidung/womens-bottoms.html" ><span>Womens Bottoms</span></a></li><li  class="level1 nav-3-5"><a href="https://www.allikestore.com/german/bekleidung/socks.html" ><span>Socken</span></a></li><li  class="level1 nav-3-6 last"><a href="https://www.allikestore.com/german/bekleidung/caps-beanies.html" ><span>Headwear</span></a></li></ul><div class="clear"></div></div></li><li  class="level0 nav-4  level-top  parent"><a href="https://www.allikestore.com/german/accessories.html" ><span>Accessories</span></a><div class="megamenu-dropdown"><ul class="level0"><li  class="level1 nav-4-1 first"><a href="https://www.allikestore.com/german/accessories/audio-90.html" ><span>Audio</span></a></li><li  class="level1 nav-4-2"><a href="https://www.allikestore.com/german/accessories/bags.html" ><span>Taschen &amp; Cases</span></a></li><li  class="level1 nav-4-3"><a href="https://www.allikestore.com/german/accessories/sunglasses.html" ><span>Eyewear</span></a></li><li  class="level1 nav-4-4"><a href="https://www.allikestore.com/german/accessories/homewear.html" ><span>Homewear</span></a></li><li  class="level1 nav-4-5"><a href="https://www.allikestore.com/german/accessories/toys-79.html" ><span>Toys</span></a></li><li  class="level1 nav-4-6"><a href="https://www.allikestore.com/german/accessories/magazines.html" ><span>Magazine &amp; Bücher</span></a></li><li  class="level1 nav-4-7"><a href="https://www.allikestore.com/german/accessories/shoelaces.html" ><span>Shoelaces</span></a></li><li  class="level1 nav-4-8"><a href="https://www.allikestore.com/german/accessories/skateboarding.html" ><span>Skateboarding</span></a></li><li  class="level1 nav-4-9"><a href="https://www.allikestore.com/german/accessories/vouchers.html" ><span>Vouchers</span></a></li><li  class="level1 nav-4-10"><a href="https://www.allikestore.com/german/accessories/watches.html" ><span>Uhren</span></a></li><li  class="level1 nav-4-11 last"><a href="https://www.allikestore.com/german/accessories/other.html" ><span>Anderes</span></a></li></ul><div class="clear"></div></div></li><li  class="level0 nav-5  level-top  parent"><a href="https://www.allikestore.com/german/sale.html" ><span>Sale</span></a><div class="megamenu-dropdown"><ul class="level0"><li  class="level1 nav-5-1 first"><a href="https://www.allikestore.com/german/sale/footwear-men.html" ><span>Footwear Men</span></a></li><li  class="level1 nav-5-2"><a href="https://www.allikestore.com/german/sale/footwear-women.html" ><span>Footwear Women</span></a></li><li  class="level1 nav-5-3"><a href="https://www.allikestore.com/german/sale/footwear-kids-sale.html" ><span>Footwear Kids</span></a></li><li  class="level1 nav-5-4"><a href="https://www.allikestore.com/german/sale/sale-clothing-men.html" ><span>Clothing Men</span></a></li><li  class="level1 nav-5-5"><a href="https://www.allikestore.com/german/sale/sale-clothing-women.html" ><span>Clothing Women</span></a></li><li  class="level1 nav-5-6 last"><a href="https://www.allikestore.com/german/sale/sale-accessories.html" ><span>Accessories</span></a></li></ul><div class="clear"></div></div></li><li  class="level0 nav-6 last  level-top "><a href="https://www.allikestore.com/german/coming-soon.html" ><span>Coming Soon</span></a></li>					</ul>
	</div>
</div>
							</div>
		</div>
	</div>
 
<!-- ESI END DUMMY COMMENT [] -->
        <div class="main-container col-layouts col1-layout">
	        <div class="content-container">
		        <div class="main row clearfix">
	                <div class="col-main grid_18">
	                    <!-- ESI START DUMMY COMMENT [] -->
<!-- ESI URL DUMMY COMMENT -->

  
<!-- ESI END DUMMY COMMENT [] -->
	                    <div class="breadcrumbs">
    <ul itemtype="http://schema.org/BreadcrumbList" itemscope="">
                    <li class="home" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a href="https://www.allikestore.com/german/" title="Zur Startseite gehen" itemprop="item">Startseite<meta itemprop="name" content = "Startseite"/></a>
                                        <span>/ </span>
                        </li>
                    <li class="product" itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <strong>Stone Island Sweat Shirt (Military Green)<meta itemprop="name" content = "Stone Island Sweat Shirt (Military Green)"/></strong>
                                    </li>
            </ul>
</div>
						
<script type="text/javascript">
	var optionsPrice = new Product.OptionsPrice({"productId":"56949","priceFormat":{"pattern":"%s\u00a0\u20ac","precision":2,"requiredPrecision":2,"decimalSymbol":",","groupSymbol":".","groupLength":3,"integerRequired":1},"includeTax":"true","showIncludeTax":true,"showBothPrices":false,"productPrice":149.9,"productOldPrice":149.9,"priceInclTax":149.9,"priceExclTax":149.9,"skipCalculate":1,"defaultTax":19,"currentTax":19,"idSuffix":"_clone","oldPlusDisposition":0,"plusDisposition":0,"plusDispositionTax":0,"oldMinusDisposition":0,"minusDisposition":0,"tierPrices":[],"tierPricesInclTax":[]});
</script>

<div id="messages_product_view"><!-- ESI START DUMMY COMMENT [] -->
<!-- ESI URL DUMMY COMMENT -->

  
<!-- ESI END DUMMY COMMENT [] -->
</div>
<div class="product-view" itemtype="http://schema.org/Product" itemscope="">
	<div class="product-essential">
		<form action="https://www.allikestore.com/german/checkout/cart/add/uenc/aHR0cHM6Ly93d3cuYWxsaWtlc3RvcmUuY29tL2dlcm1hbi9zdG9uZS1pc2xhbmQtc3dlYXQtc2hpcnQtbWlsaXRhcnktZ3JlZW4tNjYxNTY1NjQwLXYwMDU0Lmh0bWw,/product/56949/form_key/cGytDzMnhGZGGZq7/" method="post"
		      id="product_addtocart_form">
			<input name="form_key" type="hidden" value="cGytDzMnhGZGGZq7" />
			<div class="no-display">
				<input type="hidden" name="product" value="56949"/>
				<input type="hidden" name="related_product" id="related-products-field" value=""/>
			</div>
			<div class="row">
				<div class="grid_11">
					<div class="product-img">
						<div class="product-image">
		<a href="https://www.allikestore.com/media/catalog/product/cache/2/image/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-sweat-shirt-military-green-2.jpg" rel="lighbox-zoom-gallery" >
		<img src="https://www.allikestore.com/media/catalog/product/cache/2/image/541x541/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-sweat-shirt-military-green-2.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/image/1082x1082/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-sweat-shirt-military-green-2.jpg" alt="Stone Island Sweat Shirt (Military Green)" title="Stone Island Sweat Shirt (Military Green)" width="541" height="541" itemprop="image" />
	</a>
</div>


<div class="more-views">
	<div class="more-views-nav" id="thumbs_slider_nav"><ul><li><a class="prev" href="#"></a></li><li><a class="next" href="#"></a></li></ul></div>
    <ul class="carousel-slider slides" id="thumbs_slider" data-items="[ [0, 3], [426, 4] ]">
                        <li>
	        <a href='https://www.allikestore.com/media/catalog/product/cache/2/image/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-sweat-shirt-military-green-1.jpg' class='lighbox-zoom-gallery' rel='lighbox-zoom-gallery' title='Stone Island Sweat Shirt (Military Green)' >
		        <span></span>
		        <img src="https://www.allikestore.com/media/catalog/product/cache/2/thumbnail/127x127/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-sweat-shirt-military-green-1.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/thumbnail/254x254/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-sweat-shirt-military-green-1.jpg" width="127" height="127" alt="Stone Island Sweat Shirt (Military Green)"/>
	        </a>
        </li>
        </ul>
</div>

<script type="text/javascript">
	jQuery(function($) {
		$('a[rel="lighbox-zoom-gallery"]').fancybox({
			titleShow:false,
			hideOnContentClick:true
		});
	});
</script>
					</div>
				</div>
				<div class="grid_7">

				<div class="product-name text-center">
															<img src="https://www.allikestore.com/skin/frontend/base/default/images/brands/stone-island.png" alt="stone-island">
										<h1 itemprop="name">Stone Island Sweat Shirt (Military Green)</h1>
					
				</div><h4 class="aitmanufacturers-link text-center"><a href="https://www.allikestore.com/german/stone-island.html">Alle Produkte von Stone Island zeigen</a><br /></h4>
								<br>
				<div class="row">
					<div class="grid_8">
						

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-56949" itemtype="http://schema.org/Offer" itemscope="" itemprop="offers">
                                            <span class="price" itemprop="price">149,90 €</span>                                    
<meta itemprop='priceCurrency' content='EUR' />
<link itemprop="availability" href="http://schema.org/InStock">
<meta itemprop="category" content="Mens Tops">
</span>
                        
        </div>

					</div>
					<div class="grid_10">
						<p style="margin-bottom: 0;">inkl. 19% Mwst. zzgl. Versand <br/> Versand innerhalb 1-2 Werktagen </p>
                        												    <p class="availability in-stock">Verfügbarkeit: <span>Auf Lager</span></p>
											</div>
				</div>
				
				<div class="clear"></div>
				
									<div class="short-description">
						<div
							class="std">Stone Island ist eine italienische hochwertige Herrenmarke aus Ravarino, deren Markenzeichen das Kompass-Patch ist, welches immer auf dem linken Oberarm sitzt.<br />
<br />
Dieser Crewneck Sweater ist aus hochwertiger Baumwolle und hat das bekannte Stone Island Patch auf dem linken Oberarm. <br />
<br />
Farbe:  Military Green<br />
Material: 100% Baumwolle<br />
<br />
Unser Modell ist 189 cm groß und trägt eine XL.</div>
					</div>
				
				<!-- Product Attributes -->
				<table class="table-article-options">
					<tbody>
						<tr>
							<td><b>Artikelnummer</b></td>
							<td>661565640.V0054</td>
						</tr>
						
												
												
											</tbody>
				</table>
				<!-- // Product Attributes -->

								
				<div class="product-info">
					<div class="row">
															<div class="product-options" id="product-options-wrapper">
    
    <dl>
            <dt><label class="required"><em>*</em>Size</label></dt>
        <dd class="last">
            <div class="input-box">
                <select name="super_attribute[525]" id="attribute525" class="required-entry super-attribute-select">
                    <option>Eine Option auswählen...</option>
                  </select>
              </div>
        </dd>
        </dl>
    
<script type="text/javascript">
        var spConfig = new Product.Config({"attributes":{"525":{"id":"525","code":"shirt_size","label":"Size","options":[{"id":"99","label":"Medium","price":"0","oldPrice":"0","products":["56950"]},{"id":"98","label":"Large","price":"0","oldPrice":"0","products":["56951"]},{"id":"183","label":"X-Large","price":"0","oldPrice":"0","products":["56952"]},{"id":"373","label":"XX-Large","price":"0","oldPrice":"0","products":["56953"]}]}},"template":"#{price}\u00a0\u20ac","basePrice":"149.9","oldPrice":"149.9","productId":"56949","chooseText":"Gr\u00f6\u00dfe w\u00e4hlen...","taxConfig":{"includeTax":true,"showIncludeTax":true,"showBothPrices":false,"defaultTax":19,"currentTax":19,"inclTaxTitle":"Mit Mwst."}});
    </script>


<script type="text/javascript">
//<![CDATA[
var DateOption = Class.create({

    getDaysInMonth: function(month, year)
    {
        var curDate = new Date();
        if (!month) {
            month = curDate.getMonth();
        }
        if (2 == month && !year) { // leap year assumption for unknown year
            return 29;
        }
        if (!year) {
            year = curDate.getFullYear();
        }
        return 32 - new Date(year, month - 1, 32).getDate();
    },

    reloadMonth: function(event)
    {
        var selectEl = event.findElement();
        var idParts = selectEl.id.split("_");
        if (idParts.length != 3) {
            return false;
        }
        var optionIdPrefix = idParts[0] + "_" + idParts[1];
        var month = parseInt($(optionIdPrefix + "_month").value);
        var year = parseInt($(optionIdPrefix + "_year").value);
        var dayEl = $(optionIdPrefix + "_day");

        var days = this.getDaysInMonth(month, year);

        //remove days
        for (var i = dayEl.options.length - 1; i >= 0; i--) {
            if (dayEl.options[i].value > days) {
                dayEl.remove(dayEl.options[i].index);
            }
        }

        // add days
        var lastDay = parseInt(dayEl.options[dayEl.options.length-1].value);
        for (i = lastDay + 1; i <= days; i++) {
            this.addOption(dayEl, i, i);
        }
    },

    addOption: function(select, text, value)
    {
        var option = document.createElement('OPTION');
        option.value = value;
        option.text = text;

        if (select.options.add) {
            select.options.add(option);
        } else {
            select.appendChild(option);
        }
    }
});
dateOption = new DateOption();
//]]>
</script>




<script type="text/javascript">
//<![CDATA[
enUS = {"m":{"wide":["January","February","March","April","May","June","July","August","September","October","November","December"],"abbr":["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]}}; // en_US locale reference
Calendar._DN = ["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"]; // full day names
Calendar._SDN = ["So.","Mo.","Di.","Mi.","Do.","Fr.","Sa."]; // short day names
Calendar._FD = 1; // First day of the week. "0" means display Sunday first, "1" means display Monday first, etc.
Calendar._MN = ["Januar","Februar","M\u00e4rz","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember"]; // full month names
Calendar._SMN = ["Jan.","Feb.","M\u00e4rz","Apr.","Mai","Juni","Juli","Aug.","Sep.","Okt.","Nov.","Dez."]; // short month names
Calendar._am = "vorm."; // am/pm
Calendar._pm = "nachm.";

// tooltips
Calendar._TT = {};
Calendar._TT["INFO"] = "Über den Kalender";

Calendar._TT["ABOUT"] =
"DHTML Date/Time Selektor\n" +
"(c) dynarch.com 2002-2005 / Author: Mihai Bazon\n" +
"Für die letzte Version besuchen Sie bitte: http://www.dynarch.com/projects/calendar/\n" +
"Veröffentlicht unter der GNU LGPL. Mehr Informationen unter http://gnu.org/licenses/lgpl.html." +
"\n\n" +
"Datumsauswahl:\n" +
"- Benutzen Sie die \xab, \xbb Buttons für die Jahresauswahl\n" +
"- Benutzen Sie die " + String.fromCharCode(0x2039) + ", " + String.fromCharCode(0x203a) + " Buttons für die Monatsauswahl\n" +
"- Halten Sie einen der Buttons gedrückt für eine Schnellauswahl.";
Calendar._TT["ABOUT_TIME"] = "\n\n" +
"Zeitauswahl:\n" +
"- Klicken Sie auf einen der Zeitwerte um den Wert zu erhöhen\n" +
"- oder Umschalt-Taste gedrückt halten und klicken, um den Wert zu verkleinern\n" +
"- oder klicken und ziehen Sie für eine Schnellauswahl.";

Calendar._TT["PREV_YEAR"] = "Vor. Jahr (gedrückt halten für Menü)";
Calendar._TT["PREV_MONTH"] = "Vor. Monat (gedrückt halten für Menü)";
Calendar._TT["GO_TODAY"] = "Heute aufrufen";
Calendar._TT["NEXT_MONTH"] = "Nächster Monat (gedrückt halten für Menü)";
Calendar._TT["NEXT_YEAR"] = "Nächstes Jahr (gedrückt halten für Menü)";
Calendar._TT["SEL_DATE"] = "Datum auswählen";
Calendar._TT["DRAG_TO_MOVE"] = "Ziehen zum Bewegen";
Calendar._TT["PART_TODAY"] = ' (' + "Heute" + ')';

// the following is to inform that "%s" is to be the first day of week
Calendar._TT["DAY_FIRST"] = "Zeige %s zuerst";

// This may be locale-dependent. It specifies the week-end days, as an array
// of comma-separated numbers. The numbers are from 0 to 6: 0 means Sunday, 1
// means Monday, etc.
Calendar._TT["WEEKEND"] = "0,6";

Calendar._TT["CLOSE"] = "Schließen";
Calendar._TT["TODAY"] = "Heute";
Calendar._TT["TIME_PART"] = "(Umschalt-)Klick oder ziehen, um Wert zu ändern";

// date formats
Calendar._TT["DEF_DATE_FORMAT"] = "%d.%m.%Y";
Calendar._TT["TT_DATE_FORMAT"] = "%e. %B %Y";

Calendar._TT["WK"] = "Woche";
Calendar._TT["TIME"] = "Zeit:";
//]]>
</script>

            <p class="required">* Notwendige Felder</p>
    </div>

<script type="text/javascript">decorateGeneric($$('#product-options-wrapper dl'), ['last']);</script>

<div class="product-options-bottom">
        <div class="add-to-cart">
                		<button type="button" title="In den Warenkorb" class="button btn-cart icon-black" onclick="productAddToCartForm.submit(this)"><span><span>In den Warenkorb</span></span></button>
            </div>
</div>
													</div>
					
				</div>

																	<div class="clear"></div>

				<div><!-- ESI START DUMMY COMMENT [] -->
<!-- ESI URL DUMMY COMMENT -->

  
<!-- ESI END DUMMY COMMENT [] --></div>
				<div class="row product-shop">
					<div class="grid_6">
						

<ul class="add-to-links">
		    <li><a class="link-wishlist  icon-black" href="https://www.allikestore.com/german/wishlist/index/add/product/56949/form_key/cGytDzMnhGZGGZq7/" onclick="productAddToCartForm.submitLight(this, this.href); return false;"><span class="link_i"></span>Auf Wunschzettel</a></li>
				</ul>
					</div>
					<div class="grid_6">
					</div>

				</div>
				    

<script src="/skin/frontend/base/default/js/jquery.magnific-popup.min.js"></script>

	    		    	    <div id="product_modal_tab_tabbed" class="white-popup mfp-hide">
			<h2 id="product_acc_tab_tabbed" class="tab-heading"><a href="#">Größentabelle</a></h2>
			<div style="overflow-x:auto;">		</div>
	    </div>
	    		    	    <div id="product_modal_athlete_custom_tab1" class="white-popup mfp-hide">
			<h2 id="product_acc_athlete_custom_tab1" class="tab-heading"><a href="#">Versandkosten</a></h2>
			<div style="overflow-x:auto;"><table style="width: 100%;" border="0">
<tbody>
<tr>
<td><strong>DHL GoGreen/Premium<br /></strong></td>
<td><strong>Versandkosten<br /></strong></td>
<td><strong>VK-Frei ab<br /></strong></td>
<td><strong>Lieferzeit<br /></strong></td>
</tr>
<tr>
<td>Deutschland</td>
<td align="center">&euro;&nbsp; 4,95</td>
<td align="center">&euro;&nbsp; 80</td>
<td align="center">1-2 Werktage</td>
</tr>
<tr>
<td>Schweiz &amp; Norwegen</td>
<td align="center">&euro; 19,95</td>
<td align="center">&euro; 250</td>
<td align="center">2-5 Werktage</td>
</tr>
<tr>
<td>Europa</td>
<td align="center">&euro;&nbsp;&nbsp; 9,95</td>
<td align="center">&euro; 200</td>
<td align="center">2-5 Werktage</td>
</tr>
<tr>
<td>Weltweit</td>
<td align="center">&euro; 29,95</td>
<td align="center">&euro; 500</td>
<td align="center">5-10 Werktage</td>
</tr>
<tr>
<td><strong>DHL Express</strong></td>
<td align="center"><strong>Versandkosten</strong></td>
<td align="center"><strong>VK-Frei ab</strong></td>
<td align="center"><strong>Lieferzeit</strong></td>
</tr>
<tr>
<td>Schweiz &amp; Norwegen</td>
<td align="center">&euro; 29,95</td>
<td align="center">&euro; 400</td>
<td align="center">1-2 Werktage</td>
</tr>
<tr>
<td>Europa</td>
<td align="center">&euro;&nbsp; 29,95</td>
<td align="center">&euro; 400</td>
<td align="center">1-2 Werktage</td>
</tr>
<tr>
<td>Weltweit</td>
<td align="center">&euro; 49,95</td>
<td align="center">&nbsp;</td>
<td align="center">1-2 Werktage</td>
</tr>
</tbody>
</table></div>
	    </div>
	    		    	    <div id="product_modal_athlete_custom_tab2" class="white-popup mfp-hide">
			<h2 id="product_acc_athlete_custom_tab2" class="tab-heading"><a href="#">Zahlung</a></h2>
			<div style="overflow-x:auto;"><p><strong>Folgende Zahlungsmethoden bieten wir an:</strong></p>
<div class="clearfix">
<h3><strong>1.Kreditkarte</strong></h3>
<p>Unser System zur Kreditkartenbezahlung ist absolut sicher.</p>
<p>Wir arbeiten mit einem der f&uuml;hrenden Anbieter im Bereich Online-Kreditkartenzahlungen zusammen.</p>
<p>Nach dem Abschluss des Bestellprozesses auf unserer Seite wirst Du automatisch je nach Kreditkartenanbieter</p>
<p>zu einer 3D Secure (Master Card) beziehungsweise Verified by Visa Seite weitergeleitet.</p>
<p>Dieser zus&auml;tzliche Sicherheitscheck wird hier erkl&auml;rt: <a href="http://www.visa.de/de/sicher_mit_visa/sicher_online_mit_verified_by/so_funktioniert_es.aspx" target="_blank">Verified by Visa</a>&nbsp; <a href="http://www.mastercard.com/de/privatkunden/products/products_securecode.html" target="_blank">3D Secure Master Card</a></p>
<h3><strong>2.PayPal</strong></h3>
<p>Wenn Du per PayPal bezahlen m&ouml;chtest, so wirst du w&auml;hrend des Bestellvorgangs auf die PayPal Seite weitergeleitet, Dort kannst Du dich dann entscheiden, welche PayPal Bezahlart Du verwenden m&ouml;chtest.<strong></strong></p>
<h3><strong>3.Kauf per Rechnung, Lastschrift (Billpay)<br /></strong></h3>
<p>Mit unserem Partner Billpay kannst Du in unserem Online Shop bequem per Rechnung oder per Lastschrift bezahlen.</p>
<p>Das Zahlungsziel bei&nbsp;Kauf auf Rechnung betr&auml;gt 20 Tage ab Bestelldatum.</p>
<p>Deine Vorteile:</p>
<p>&nbsp;&nbsp;&nbsp; &bull;&nbsp;&nbsp;&nbsp; Erst die Ware, dann das Geld<br />&nbsp;&nbsp;&nbsp; &bull;&nbsp;&nbsp;&nbsp; Sofortiger Versand<br />&nbsp;&nbsp;&nbsp; &bull;&nbsp;&nbsp;&nbsp; Bequeme und sichere Abwicklung der Zahlung durch Billpay<br /><br />Bei der Zahlungsart Lastschrift gibst Du im Checkout deine Kontodaten an</p>
<p>und der Gesamtbetrag wird einfach und bequem von deinem&nbsp;Konto abgebucht.</p>
<p>Bei der Zahlungsart Kauf auf Rechnung und Lastschrift ist eine Lieferung an eine von der Rechnungsadresse</p>
<p>abweichende Lieferadresse nicht m&ouml;glich. Wir bitten hierf&uuml;r um Verst&auml;ndnis.</p>
<p>Weitere Infos zum Kauf auf Rechnung und Lastschrift mit BillPay findest Du in unseren AGB's unter dem Punkt Zahlungen.</p>
<p><strong>(Bis 100&euro; Warenkorbwert 3&euro; Geb&uuml;hr; bis 200&euro; Warenkorbwert 5&euro; Geb&uuml;hr; dar&uuml;ber 8&euro; Geb&uuml;hr)</strong></p>
<h3><strong>4.sofort&uuml;berweisung</strong></h3>
<p>Du wirst nach dem Abschluss der Bestellung direkt auf die Seite von sofort&uuml;berweisung.de geleitet. Von dort aus kannst Du dann mit deinen Online-Banking Daten die Bezahlung der Bestellung direkt ausf&uuml;hren.</p>
<p>Deine Vorteile:</p>
<p>&nbsp;&nbsp;&nbsp; &bull;&nbsp;&nbsp;&nbsp; Zahlungssystem mit T&Uuml;V-gepr&uuml;ftem Datenschutz<br />&nbsp;&nbsp;&nbsp; &bull;&nbsp;&nbsp;&nbsp; Keine Registrierung notwendig<br />&nbsp;&nbsp;&nbsp; &bull;&nbsp;&nbsp;&nbsp; Ware/Dienstleistung wird bei Verf&uuml;gbarkeit SOFORT versendet<br />&nbsp;&nbsp;&nbsp; &bull;&nbsp;&nbsp;&nbsp; Bitte halten Sie Ihre Online-Banking-Daten (PIN/TAN) bereit<br />&nbsp;&nbsp;</p>
<p>Dieser Service ist in folgenden L&auml;ndern verf&uuml;gbar:</p>
<p>Vereinigtes K&ouml;nigreich, Belgien, Deutschland, Italien, Niederlande, &Ouml;sterreich, Polen, Schweiz.</p>
<p><strong>Falls Du von ausserhalb der Europ&auml;ischen Union Deine Bestellung aufgibst (z.b. aus der Schweiz),<br />so wird 19% Mehrwertsteuer automatisch w&auml;hrend des Bestellvorgangs abgezogen.</strong></p>
</div></div>
	    </div>
	    	<ul class="article-options-links">
	        	    	        	            <li class="li_tab_tabbed"><a class="popup-link" href="#product_modal_tab_tabbed" ><span class="link_i"></span>Größentabelle</a></li>
	        	    	        	            <li class="li_athlete_custom_tab1"><a class="popup-link" href="#product_modal_athlete_custom_tab1" ><span class="link_i"></span>Versandkosten</a></li>
	        	    	        	            <li class="li_athlete_custom_tab2"><a class="popup-link" href="#product_modal_athlete_custom_tab2" ><span class="link_i"></span>Zahlung</a></li>
	        	    </ul>

<script type="text/javascript">
jQuery.noConflict();
(function( $ ) {
    $('.popup-link').magnificPopup({ 
    type:'inline',
  midClick: true
	});
	console.log('test');
})(jQuery);
</script>

<!--

<script type="text/javascript">
//<![CDATA[
Varien.Tabs = Class.create();
Varien.Tabs.prototype = {
  initialize: function(selector) {
    var self=this;
    $$(selector+' a').each(this.initTab.bind(this));
	this.showContent($$(selector+' a')[0]);
  },

  initTab: function(el) {
      el.href = 'javascript:void(0)';
      el.observe('click', this.showContent.bind(this, el));
  },

  showContent: function(a) {
    var li = $(a.parentNode), ul = $(li.parentNode);
    ul.select('li', 'ol').each(function(el){
      var contents = $(el.id+'_contents');
      if (el==li) {
        el.addClassName('active');
        contents.show();
      } else {
        el.removeClassName('active');
        contents.hide();
      }
    });
  }
}
new Varien.Tabs('.product-tabs');
//]]>
</script>

-->				<div class="clear"></div>
												
				
				

				
			</div>
			<div class="clear"></div>
			</div>
			

			

			<div class="clearer"></div>

		

		</form>



		
<script type="text/javascript">
			//<![CDATA[
			var productAddToCartForm = new VarienForm('product_addtocart_form');
			productAddToCartForm.submit = function(button, url) {
				if (this.validator.validate()) {
					var form = this.form;
					var oldUrl = form.action;
					if (url) {
						form.action = url;
					}
					var e = null;
					if ( $(button).id.indexOf('ec_shortcut') != -1  ) {
						try {
							this.form.submit();
							return;
						} catch (e) {
						}
					}
					if (!url) {
						url = jQuery('#product_addtocart_form').attr('action');
					}
					url = url.replace("checkout/cart","oxajax/cart");
					url = url.replace("wishlist/index/cart", "oxajax/cart/add");
					var data = jQuery('#product_addtocart_form').serialize();
					data += '&isAjax=1';
					if ('https:' == document.location.protocol) {
						url = url.replace('http:', 'https:');
					}
					jQuery.fancybox.showActivity();
					jQuery.ajax({
						url:url,
						dataType:'jsonp',
						type:'post',
						data:data,
						success:function (data) {
							Olegnax.Ajaxcart.helpers.showMessage(data.message);
							Olegnax.Ajaxcart.helpers.cartSuccessFunc(data);
						}
					});
					this.form.action = oldUrl;
					if (e) {
						throw e;
					}
				}
			}.bind(productAddToCartForm);

			productAddToCartForm.submitLight = function(button, url){
				if(this.validator) {
					var nv = Validation.methods;
					delete Validation.methods['required-entry'];
					delete Validation.methods['validate-one-required'];
					delete Validation.methods['validate-one-required-by-name'];
					if (this.validator.validate()) {
						if (url) {
							this.form.action = url;
						}
						this.form.submit();
					}
					Object.extend(Validation.methods, nv);
				}
			}.bind(productAddToCartForm);

			productAddToCartForm.submitWishlist = function (button, url) {
				if(this.validator) {
					var nv = Validation.methods;
					delete Validation.methods['required-entry'];
					delete Validation.methods['validate-one-required'];
					delete Validation.methods['validate-one-required-by-name'];
					if (this.validator.validate()) {
						var form = this.form;
						var oldUrl = form.action;
						if (url) {
							form.action = url;
						}
						var e = null;
						if (!url) {
							url = jQuery('#product_addtocart_form').attr('action');
						}
						url = url.replace("wishlist/index", "oxajax/wishlist");
						if ('https:' == document.location.protocol) {
							url = url.replace('http:', 'https:');
						}
						var data = jQuery('#product_addtocart_form').serialize();
						data += '&isAjax=1';
						if ('https:' == document.location.protocol) {
							url = url.replace('http:', 'https:');
						}
						jQuery.fancybox.showActivity();
						jQuery.ajax({
							url:url,
							dataType:'jsonp',
							type:'post',
							data:data,
							success:function (data) {
								Olegnax.Ajaxcart.helpers.showMessage(data.message);
								Olegnax.Ajaxcart.helpers.wishlistSuccessFunc(data);
							}
						});
						this.form.action = oldUrl;

						if (e) {
							throw e;
						}
					}
					Object.extend(Validation.methods, nv);
				}
			}.bind(productAddToCartForm);

			//]]>
		</script>

		
		                <div class="product-slider-container category-products product-columns-5">
            <div class="clearfix title-container">
				<h3>Andere Produkte</h3><div class="slider-nav" id="slider_athlete_589183915a3bc_nav" ><ul><li><a class="prev icon-white mm-icon-black" href="#"></a></li><li><a class="next icon-white mm-icon-black" href="#"></a></li></ul></div>
			</div>
            <div class="slider-container">
                    <ul class="carousel-slider products-grid clearfix" id="slider_athlete_589183915a3bc"
                data-items="[ [0, 2], [426, 2], [756, 3], [960, 5], [1200, 6], [1300, 6], [1380, 7], [1520, 8] ]"
                data-autoscroll="0"
				data-scroll="item"
		        data-rewind="0"

                >
                                                                <li class="item">
                        
                        <div class="item-wrap">
                <a href="https://www.allikestore.com/german/stone-island-fleece-pants-dove-grey-661566660-v0092.html" title="" class="product-image">
	                	                	                <img class="regular_img" src="https://www.allikestore.com/media/catalog/product/cache/2/small_image/217x217/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-fleece-pants-dove-grey-3.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/small_image/434x434/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-fleece-pants-dove-grey-3.jpg" width="217" height="217" alt="" />                </a>
                <div class="product-hover">
	                		                <h2 class="product-name"><a href="https://www.allikestore.com/german/stone-island-fleece-pants-dove-grey-661566660-v0092.html" title="Stone Island Fleece Bermuda Shorts (Grau)">Stone Island Fleece Bermuda Shorts (Grau)</a></h2><br/>
	                		                

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-56966_slider">
                                            <span class="price">179,90 €</span>                                    </span>
                        
        </div>

                </div>
                <div class="name">
	                                </div>
	            </div>
                        </li>
                                                                <li class="item">
                        
                        <div class="item-wrap">
                <a href="https://www.allikestore.com/german/stone-island-polo-shirt-white-661521919-v0001.html" title="" class="product-image">
	                	                	                <img class="regular_img" src="https://www.allikestore.com/media/catalog/product/cache/2/small_image/217x217/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-polo-shirt-white-1.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/small_image/434x434/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-polo-shirt-white-1.jpg" width="217" height="217" alt="" />                </a>
                <div class="product-hover">
	                		                <h2 class="product-name"><a href="https://www.allikestore.com/german/stone-island-polo-shirt-white-661521919-v0001.html" title="Stone Island Polo Shirt (Weiß)">Stone Island Polo Shirt (Weiß)</a></h2><br/>
	                		                

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-54507_slider">
                                            <span class="price">189,90 €</span>                                    </span>
                        
        </div>

                </div>
                <div class="name">
	                                </div>
	            </div>
                        </li>
                                                                <li class="item">
                        
                        <div class="item-wrap">
                <a href="https://www.allikestore.com/german/stone-island-knitwear-t-shirt-avio-blue-6615534sa-v0024.html" title="" class="product-image">
	                	                	                <img class="regular_img" src="https://www.allikestore.com/media/catalog/product/cache/2/small_image/217x217/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-knitwear-tshirt-avio-blue-1_1.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/small_image/434x434/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-knitwear-tshirt-avio-blue-1_1.jpg" width="217" height="217" alt="" />                </a>
                <div class="product-hover">
	                		                <h2 class="product-name"><a href="https://www.allikestore.com/german/stone-island-knitwear-t-shirt-avio-blue-6615534sa-v0024.html" title="Stone Island Knitwear T-Shirt (Avio Blau)">Stone Island Knitwear T-Shirt (Avio Blau)</a></h2><br/>
	                		                

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-54497_slider">
                                            <span class="price">239,90 €</span>                                    </span>
                        
        </div>

                </div>
                <div class="name">
	                                </div>
	            </div>
                        </li>
                                                                <li class="item">
                        
                        <div class="item-wrap">
                <a href="https://www.allikestore.com/german/stone-island-t-shirt-white-661524141-v0001.html" title="" class="product-image">
	                	                	                <img class="regular_img" src="https://www.allikestore.com/media/catalog/product/cache/2/small_image/217x217/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-t-shirt-_white_-1.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/small_image/434x434/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-t-shirt-_white_-1.jpg" width="217" height="217" alt="" />                </a>
                <div class="product-hover">
	                		                <h2 class="product-name"><a href="https://www.allikestore.com/german/stone-island-t-shirt-white-661524141-v0001.html" title="Stone Island T-Shirt (Weiß)">Stone Island T-Shirt (Weiß)</a></h2><br/>
	                		                

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-54512_slider">
                                            <span class="price">79,90 €</span>                                    </span>
                        
        </div>

                </div>
                <div class="name">
	                                </div>
	            </div>
                        </li>
                                                                <li class="item">
                        
                        <div class="item-wrap">
                <a href="https://www.allikestore.com/german/stone-island-knitwear-crewneck-navy-blue-6615528b0-v0020.html" title="" class="product-image">
	                	                	                <img class="regular_img" src="https://www.allikestore.com/media/catalog/product/cache/2/small_image/217x217/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-knitwear-crewneck-navy-blue-1.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/small_image/434x434/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-knitwear-crewneck-navy-blue-1.jpg" width="217" height="217" alt="" />                </a>
                <div class="product-hover">
	                		                <h2 class="product-name"><a href="https://www.allikestore.com/german/stone-island-knitwear-crewneck-navy-blue-6615528b0-v0020.html" title="Stone Island Knitwear Crewneck (Navy Blau)">Stone Island Knitwear Crewneck (Navy Blau)</a></h2><br/>
	                		                

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-55375_slider">
                                            <span class="price">274,90 €</span>                                    </span>
                        
        </div>

                </div>
                <div class="name">
	                                </div>
	            </div>
                        </li>
                                                                <li class="item">
                        
                        <div class="item-wrap">
                <a href="https://www.allikestore.com/german/stone-island-light-soft-shell-jacket-black-661541826-v0029.html" title="" class="product-image">
	                	                	                <img class="regular_img" src="https://www.allikestore.com/media/catalog/product/cache/2/small_image/217x217/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-jacket-black-3.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/small_image/434x434/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-jacket-black-3.jpg" width="217" height="217" alt="" />                </a>
                <div class="product-hover">
	                		                <h2 class="product-name"><a href="https://www.allikestore.com/german/stone-island-light-soft-shell-jacket-black-661541826-v0029.html" title="Stone Island Light Soft Shell Jacket (Schwarz)">Stone Island Light Soft Shell Jacket (Schwarz)</a></h2><br/>
	                		                

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-55670_slider">
                                            <span class="price">449,90 €</span>                                    </span>
                        
        </div>

                </div>
                <div class="name">
	                                </div>
	            </div>
                        </li>
                                                                <li class="item">
                        
                        <div class="item-wrap">
                <a href="https://www.allikestore.com/german/stone-island-sweat-shirt-marine-blue-661563258-v0028.html" title="" class="product-image">
	                	                	                <img class="regular_img" src="https://www.allikestore.com/media/catalog/product/cache/2/small_image/217x217/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-sweat-shirt-marine-blue-1_1.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/small_image/434x434/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-sweat-shirt-marine-blue-1_1.jpg" width="217" height="217" alt="" />                </a>
                <div class="product-hover">
	                		                <h2 class="product-name"><a href="https://www.allikestore.com/german/stone-island-sweat-shirt-marine-blue-661563258-v0028.html" title="Stone Island Sweat Shirt (Marine Blau)">Stone Island Sweat Shirt (Marine Blau)</a></h2><br/>
	                		                

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-54493_slider">
                                            <span class="price">254,90 €</span>                                    </span>
                        
        </div>

                </div>
                <div class="name">
	                                </div>
	            </div>
                        </li>
                                                                <li class="item">
                        
                        <div class="item-wrap">
                <a href="https://www.allikestore.com/german/stone-island-t-shirt-military-green-661524141-v0054.html" title="" class="product-image">
	                	                	                <img class="regular_img" src="https://www.allikestore.com/media/catalog/product/cache/2/small_image/217x217/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-tshirt-military-green-2.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/small_image/434x434/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-tshirt-military-green-2.jpg" width="217" height="217" alt="" />                </a>
                <div class="product-hover">
	                		                <h2 class="product-name"><a href="https://www.allikestore.com/german/stone-island-t-shirt-military-green-661524141-v0054.html" title="Stone Island T-Shirt (Military Green)">Stone Island T-Shirt (Military Green)</a></h2><br/>
	                		                

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-55390_slider">
                                            <span class="price">79,90 €</span>                                    </span>
                        
        </div>

                </div>
                <div class="name">
	                                </div>
	            </div>
                        </li>
                                                                <li class="item">
                        
                        <div class="item-wrap">
                <a href="https://www.allikestore.com/german/stone-island-long-sleeve-black-661522819-v29.html" title="" class="product-image">
	                	                	                <img class="regular_img" src="https://www.allikestore.com/media/catalog/product/cache/2/small_image/217x217/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-longsleeve-crewneck-black-1.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/small_image/434x434/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-longsleeve-crewneck-black-1.jpg" width="217" height="217" alt="" />                </a>
                <div class="product-hover">
	                		                <h2 class="product-name"><a href="https://www.allikestore.com/german/stone-island-long-sleeve-black-661522819-v29.html" title="Stone Island Long Sleeve (Schwarz)">Stone Island Long Sleeve (Schwarz)</a></h2><br/>
	                		                

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-55381_slider">
                                            <span class="price">129,90 €</span>                                    </span>
                        
        </div>

                </div>
                <div class="name">
	                                </div>
	            </div>
                        </li>
                                                                <li class="item">
                        
                        <div class="item-wrap">
                <a href="https://www.allikestore.com/german/stone-island-knitwear-long-sleeve-shirt-ice-6615528b0-v0003.html" title="" class="product-image">
	                	                	                <img class="regular_img" src="https://www.allikestore.com/media/catalog/product/cache/2/small_image/217x217/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-knitwear-long-sleeve-shirt-ice-1.jpg" data-srcx2="https://www.allikestore.com/media/catalog/product/cache/2/small_image/434x434/9df78eab33525d08d6e5fb8d27136e95/s/t/stone-island-knitwear-long-sleeve-shirt-ice-1.jpg" width="217" height="217" alt="" />                </a>
                <div class="product-hover">
	                		                <h2 class="product-name"><a href="https://www.allikestore.com/german/stone-island-knitwear-long-sleeve-shirt-ice-6615528b0-v0003.html" title="Stone Island Knitwear Long Sleeve Shirt (Ice)">Stone Island Knitwear Long Sleeve Shirt (Ice)</a></h2><br/>
	                		                

                        
    <div class="price-box">
                                                                <span class="regular-price" id="product-price-54523_slider">
                                            <span class="price">199,90 €</span>                                    </span>
                        
        </div>

                </div>
                <div class="name">
	                                </div>
	            </div>
                        </li>
                                    </ul>
            </div>
        </div>
    
<script type="text/javascript">
        //<![CDATA[
        document.observe('dom:loaded', function () {
            var aw_arp_popup=false;
            $$('#map-popup').each(
            function(item){
                if(aw_arp_popup){ item.remove(); }
                aw_arp_popup=true;
            });
        });
        //]]>
    </script>

	
	</div>
<meta itemprop="description" content="Stone Island Sweat Shirt (Military Green)">
<meta itemprop="color" content="Oliv">
<meta itemprop="manufacturer" content="Stone Island">
</div>
<!-- Kupona Produktdetailseite -->
 
<script language="JavaScript" type="text/javascript">
    var kp_product_brand = "Stone Island";
    var kp_product_id = "661565640.V0054";
    var kp_product_category_id = "65";
    var kp_recommended_product_ids = "";
</script>


<script type="text/javascript" src="https://d31bfnnwekbny6.cloudfront.net/customers/11132.min.js"></script>


<!-- belboon tag -->

<script type="text/javascript">
var belboonTag = {
	productBrand: "Stone Island",
	categoryId: "Neuheiten",
	products: [
		{id: "661565640.V0054", price: "149.90"}
	]
};
</script>


<script type="text/javascript" src="https://containertags.belboon.de/belboonTag.js.php?pId=19049&page=product&type=dynamic"></script>


<script type="text/javascript">
    var lifetime = 3600;
    var expireAt = Mage.Cookies.expires;
    if (lifetime > 0) {
        expireAt = new Date();
        expireAt.setTime(expireAt.getTime() + lifetime * 1000);
    }
    Mage.Cookies.set('external_no_cache', 1, expireAt);
</script>
	                </div>
	            </div>
            </div>
	        <div class="snippets-footer">
	<div itemscope itemtype="http://schema.org/Product">	
		<ul class="snippets-fields">
							<li><span itemprop="name">Stone Island Sweat Shirt (Military Green)</span></li>
						<li>
				<span itemprop="offers" itemscope itemtype="http://schema.org/Offer">
											Preis: <span itemprop="price" content="149.90">149,90 €</span>						
										
					<meta itemprop="priceCurrency" content="EUR" />
											<link itemprop="availability" href="http://schema.org/InStock"> - Auf Lager																<link itemprop="itemCondition" href="http://schema.org/NewCondition">
											
				</span>
			</li>
												<li>Brand: <span itemprop="brand" itemscope itemtype="http://schema.org/Brand"><span itemprop="name">Stone Island</span></span></li>
												
		</ul>			
	</div>
</div><!-- ESI START DUMMY COMMENT [] -->
<!-- ESI URL DUMMY COMMENT -->

 <div class="footer-container">
	<div class="footer row clearfix">
		<div class="grid_18">
			<div class="footer-info clearfix">
				<!-- 4 column -->
<div class="newsletter-container">
<div class="newsletter"><div class="footer-subscribe">
	<div class="title">Newsletter</div>
	<form action="https://www.allikestore.com/german/newsletter/subscriber/new/" method="post" id="footer-newsletter-validate-detail">
		<p>Abonniere unseren Newsletter für Updates, Gutschein Codes u.v.m.</p>
		<button type="submit" title="Abonnieren" class="button">
			<span><span>Abonnieren</span></span></button>
		<div class="input-box">
		<input type="email" name="email" id="footer-newsletter"
		       title="Melden Sie sich für unseren Newsletter an"
		       class="input-text required-entry validate-email"/>
		</div>
		<div class="clear"></div>
	</form>
	<script type="text/javascript">
		//<![CDATA[
		var newsletterSubscriberFormDetail = new VarienForm('footer-newsletter-validate-detail');
		//]]>
	</script>
</div></div>
</div>
<div class="info info-content clearfix">
<div class="one_fourth">
<h4>Service:</h4>
<ul>
<li><strong>Tel: 040 / 67 38 17 80</strong></li>
<li>Mo.-Fr. 11:00 - 19:00 Uhr</li>
<li>Sa. 11:00 - 17:00 Uhr</li>
<li><a href="mailto:info@allikestore.com"><strong>info@allikestore.com</strong></a></li>
</ul>
</div>
<div class="one_fourth">
<h4>Hilfe / FAQ:</h4>
<ul>
<li><a href="https://www.allikestore.com/german/faq/">Wie kann ich bezahlen</a></li>
<li><a href="https://www.allikestore.com/german/shipping/">Versand &amp; R&uuml;cksendungen</a></li>
<li><a href="https://www.allikestore.com/german/agb/">AGB / Widerrufsbelehrung</a></li>
<li><a href="https://www.allikestore.com/german/privacy/">Privatsph&auml;re / Datenschutz</a></li>
<li><a href="http://www.allikestore.com/german/odr">ODR</a></li>
</ul>
</div>
<div class="one_fourth">
<h4>Alles &uuml;ber uns:</h4>
<ul>
<li><a href="https://www.allikestore.com/german/about-us/">&Uuml;ber Allike</a></li>
<li><a href="http://www.allikestore.com/german/jobs">Jobs @ Allike</a></li>
<li><a href="https://www.allikestore.com/german/contacts/">Kontakt / Impressum</a></li>
<li><a href="https://www.allikestore.com/german/affiliate/">Partnerprogramm</a></li>
<li><a href="https://www.allikestore.com/german/blog/">Allike Blog</a></li>
<li><a href="http://www.allikestore.com/german/glossar">Glossar</a></li>
</ul>
</div>
<div class="athlete_footer_connect"><p><a title="Facebook" class="social-icon icon-facebook " href="https://www.facebook.com/AllikeStore" target="_social"></a> <a title="Twitter" class="social-icon icon-twitter " href="https://twitter.com/allike" target="_social"></a> <a title="Youtube" class="social-icon icon-youtube " href="http://www.youtube.com/AllikeStore" target="_social"></a> <a title="Rss" class="social-icon icon-rss " href="http://www.allikestore.com/german/rss/catalog/new/store_id/1/" target="_social"></a>&nbsp;<a title="Instagram" class="social-icon icon-instagram " href="https://www.instagram.com/allikestore/?hl=de" ></a></p></div>
</div>			</div>
			<div class="facebook-block">
				<div class="athlete_footer_customer_service">
									</div>
			</div>
		</div>
	</div>
</div>
<div class="copyright-container">
	<div class="row clearfix">
		<div class="grid_18">
			<div class="cc-icons"><p><img src="https://www.allikestore.com/media/payment_2.png" alt="credit card icons" width="200" height="31" /></p></div>
			<address>&copy; 2017 Allike Store. Alle Rechte vorbehalten.</address>
		</div>
	</div>
</div>
 
<!-- ESI END DUMMY COMMENT [] -->
        </div>
	    	    <!-- ESI START DUMMY COMMENT [] -->
<!-- ESI URL DUMMY COMMENT -->

 

 
<!-- ESI END DUMMY COMMENT [] -->
<!-- BEGIN: Google Certified Shops -->
<script type="text/javascript">

    var gts = gts || [];

    gts.push( ['id', '654711'] );

    gts.push( ['badge_position', 'BOTTOM_LEFT'] );

    
    gts.push( ['locale', 'de_DE'] );

            gts.push( ['google_base_offer_id', '661565640.V0054'] );
    
            gts.push( ['google_base_subaccount_id', '6403931'] );
    
            gts.push( ['google_base_country', 'DE'] );
    
            gts.push( ['google_base_language', 'de'] );
    
    (function() {
        var gts = document.createElement('script');
        gts.type = 'text/javascript';
        gts.async = true;
        gts.src = 'https://www.googlecommerce.com/trustedstores/api/js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(gts, s);
    })();

</script>
<!-- END: Google Certified Shops -->
<script type="text/javascript">
	//<![CDATA[
	if (typeof Olegnax == "undefined") var Olegnax = {};
	if (typeof Olegnax.Ajaxcart == "undefined") {
		Olegnax.Ajaxcart = {
			translation : {},
			options : {},
			helpers : {}
		};
	}
	Olegnax.Ajaxcart.baseUrl = 'https://www.allikestore.com/german/';
	Olegnax.Ajaxcart.options.status = 1;
	Olegnax.Ajaxcart.options.quick_view = 1;
	Olegnax.Ajaxcart.options.wishlist = 1;
	Olegnax.Ajaxcart.options.compare = 0;

	Olegnax.Ajaxcart.translation.quick_view = 'Quick View';
	//]]>
</script>    
<!-- Anaraky GDRT v.1.0.9 script begin -->
<script type="text/javascript">
/* <![CDATA[ */
var google_tag_params = {
	ecomm_prodid: "56949",
	ecomm_pagetype: "product",
	ecomm_totalvalue: 125.97
};
var google_conversion_id = 1044228447;
var google_custom_params = google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/1044228447/?value=0&amp;guid=ON&amp;script=0&amp;data=ecomm_prodid%3D56949%3Becomm_pagetype%3Dproduct%3Becomm_totalvalue%3D125.97"/>
</div>
</noscript>
<!-- Anaraky GDRT script end -->
    </div>
</div>
</body>
</html>`,
};

exports.dataForSize = {
  simpleHtml: `
<div>
    <p>
    <a href="sdfsdfds">sdfsdfsdf</a>
        <span>
            Hahaha
        </span>
    </p>
</div>
`,
  lowHtmlToWordsRatio: `
<div class="random random random">
<div class="random random random">
<div class="random random random">
<div class="random random random">
<div class="random random random">
<div class="random random random">
    <p class="random random random">
    <p class="random random random">
    <p class="random random random">
    <p class="random random random">
    <p class="random random random">
    <p class="random random random">
    <a href="sdfsdfds">sdfsdfsdf</a>
    <a href="sdfsdfds">sdfsdfsdf</a>
    <a href="sdfsdfds">sdfsdfsdf</a>
    <a href="sdfsdfds">sdfsdfsdf</a>
    <a href="sdfsdfds">sdfsdfsdf</a>
    <a href="sdfsdfds">sdfsdfsdf</a>
        <span class="random random random">
        <span class="random random random">
        <span class="random random random">
        <span class="random random random">
        <span class="random random random">
            Hahaha
        </span>
        </span>
        </span>
        </span>
        </span>
        </span>
    </p>
    </p>
    </p>
    </p>
    </p>
</div>
</div>
</div>
</div>
</div>
`
};