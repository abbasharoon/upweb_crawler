exports.robotsTxt = {
  complete: `User-agent: *
Disallow: /search
Disallow: /ebooks?*q=*
Disallow: /ebooks?*output=*
Disallow: /ebooks?*pg=*
Disallow: /ebooks?*jscmd=*
Disallow: /ebooks?*buy=*
Disallow: /ebooks?*zoom=*
Disallow: /intl/*/about/views/
User-agent: Twitterbot
Allow: /imgres
User-agent: facebookexternalhit
Allow: /imgres

Sitemap: http://www.sampledomain.com/sitemap.xml
Sitemap: http://www.sampledomain.com/sitemap.xml
Sitemap: http://www.gstatic.com/profiles-sitemap.xml
Sitemap: https://www.google.com/sitemap.xml`,
  spaceErros: `
   User-agent: *
Disallow: /search
Disallow: /ebooks?*q=*
Disallow: /ebooks?*output=*
Disallow: /ebooks?*pg=*
Disallow: /ebooks?*jscmd=*
Disallow: /ebooks?*buy=*
Disallow: /ebooks?*zoom=*
Disallow: /intl/*/about/views/
User-agent: Twitterbot
Allow: /imgres
User-agent: facebookexternalhit
Allow: /imgres

Sitemap: http://www.sampledomain.com/sitemap.xml
Sitemap: http://www.sampledomain.com/sitemap.xml
Sitemap: http://www.gstatic.com/profiles-sitemap.xml
Sitemap: https://www.google.com/sitemap.xml`,
  noSitemap: `User-agent: *
Disallow: /search
Disallow: /ebooks?*q=*
Disallow: /ebooks?*output=*
Disallow: /ebooks?*pg=*
Disallow: /ebooks?*jscmd=*
Disallow: /ebooks?*buy=*
Disallow: /ebooks?*zoom=*
Disallow: /intl/*/about/views/
User-agent: Twitterbot
Allow: /imgres
User-agent: facebookexternalhit
Allow: /imgres`,
};
