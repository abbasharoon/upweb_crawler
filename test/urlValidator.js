/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable prefer-arrow-callback*/
/* eslint-disable no-underscore-dangle*/
/* eslint-disable no-unused-expressions*/
/* eslint-disable func-names*/
const chai = require('chai');
const urlValidator = require('../controllers/components/urlValidator');
const data = require('./data/urlData').urls;

const expect = chai.expect;

describe('Url Validator', function () {
  describe('check Underscores', function () {
    it('should return true when underscores are found', function () {
      expect(urlValidator.underscoresCheck(data.underscores)).to.be.true;
    });
  });
  describe('Check Max Parameters',function () {
    it('Should return true if maximum parameters are found',function () {
      expect(urlValidator.checkMaxParameters(data.parameters)).to.be.true;
    });
  });
});
