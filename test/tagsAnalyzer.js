  /* eslint-disable import/no-extraneous-dependencies */
  /* eslint-disable prefer-arrow-callback*/
  /* eslint-disable no-underscore-dangle*/
  /* eslint-disable func-names*/

require('../controllers/modules/util');
const chai = require('chai');
const html = require('./data/dummyHTML').data;
const chaiAsPromised = require('chai-as-promised');
const rewire = require('rewire');
const $ = require('cheerio');
// const expect = chai.expect;

const assert = chai.assert;
chai.use(chaiAsPromised);
let tagsAnalyzer;
describe('Tags Analyzer', function () {
  beforeEach(function () {
    tagsAnalyzer = rewire('../controllers/components/tagsAnalyzer');
  });
  describe('Check Required Tags', function () {
    it('Should detect a tag in html and return true if exists', function () {
      return assert.eventually.deepEqual(
        tagsAnalyzer.checkRequiredTags($.load(html.requiredTags)), []);
    });
    it('Should return an array of missing tags', function () {
      return assert.eventually.deepEqual(
        tagsAnalyzer.checkRequiredTags($.load(html.missingRequiredTags)),
        [
          'description',
          'keywords',
        ], 'Description and keywords must be missing');
    });
    it('Should return an error if the passed param is not cheerio dom', function () {
      return assert.isRejected(tagsAnalyzer.checkRequiredTags('No Cheerio Object'),
        /Parameter provided is not a cheerio dom object/, 'Error messages should Match');
    });
  });

  describe('Check Disallowed Tags', function () {
    it('Should Return an array of disallowed tags', function () {
      return assert.eventually.deepEqual(tagsAnalyzer.checkDisallowedTags(
        $.load(html.disallowedTags)),
        [
          {
            count: 1,
            name: 'img',
          },
          {
            count: 1,
            name: 'img',
          },
          {
            count: 3,
            name: 'nofollow',
          },
          {
            count: 1,
            name: 'iframe',
          },
          {
            count: 1,
            name: 'object',
          },
        ]);
    });
    it('Should Return an EMPTY array with no tags', function () {
      return assert.eventually.deepEqual(tagsAnalyzer.checkDisallowedTags(
        $.load(html.missingDisallowedTags)),
        []);
    });
    it('Should throw an error', function () {
      return assert.isRejected(tagsAnalyzer.checkDisallowedTags(
        'No Cheerio Dom'),
        /Parameter provided is not a cheerio dom object/,
        'HTML should be converted to cheerio dom first');
    });
  });


  describe('check Multiple Tags', function () {
    it('Should return an array with multiple tags with', function () {
      return assert.eventually.isArray(tagsAnalyzer.checkMultipleTags(
        $.load(html.multipleTags)), 'name', 'h1');
    });
    it('Should return an empty array', function () {
      return assert.eventually.deepEqual(tagsAnalyzer.checkMultipleTags(
        $.load(html.noMultipleTags)), []);
    });
    it('Should return throw an error', function () {
      return assert.isRejected(tagsAnalyzer.checkMultipleTags('No Cheerio Object'),
        /Parameter provided is not a cheerio dom object/);
    });
  });

  describe('Start', function () {
    it('Should emit three events', function () {

    });
  });
});
