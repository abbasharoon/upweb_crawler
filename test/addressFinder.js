/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable prefer-arrow-callback*/
/* eslint-disable no-underscore-dangle*/
/* eslint-disable func-names*/
/*
 @todo add further tests and refactor
 */
const rewire = require('rewire');
require('../controllers/modules/util');
const dummyData = require('./data/addressText');
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');

let addressFinder;
const assert = chai.assert;
const expect = chai.expect;
chai.use(chaiAsPromised);

describe.skip('addressFinder', function mainSuite() {
  this.timeout(8000);
  beforeEach(function () {
    addressFinder = rewire('../controllers/components/addressFinder');
  });
  describe('find', function () {
    it('Should return an object with the address', function () {
      return assert.eventually.isOk(addressFinder.find(dummyData.withAddress, dummyData.host, true),
        'With an address, It should return an Object with address');
    });
    it('Should return an empty object', function () {
      return assert.eventually.deepEqual(addressFinder.find('dummy text without address', dummyData.host, true), {},
        'Without an address, Find should return empty Object');
    });
  });
  describe('checkWithGoogle', function () {
    beforeEach(function () {
      addressFinder = rewire('../controllers/components/addressFinder');
      addressFinder.__set__('host', dummyData.host);
    });
    it('Should return an Object validated with google', function () {
      return expect(addressFinder.checkWithGoogle(dummyData.selectedAddress)).to.eventually.have.deep.property('[0].formatted_address');
    });
  });
  describe('filterAddresses', function () {
    beforeEach(function () {
      addressFinder = rewire('../controllers/components/addressFinder');
      addressFinder.__set__('possibleAddresses', dummyData.possibleAddresses);
      addressFinder.__set__('that', {
        checkWithGoogle(data) {
          return new Promise((resolve, reject) => {
            if (data) {
              resolve(data);
            } else reject(data);
          });
        },
      });
    });
    it('Should addresses with highest confidence but short length string', function (done) {
      addressFinder.__set__('promiseReject', done);
      addressFinder.__set__('promiseResolve', done);
      return assert.deepEqual(addressFinder.addressFilter(), dummyData.selectedAddress);
    });
  });
});
