/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable prefer-arrow-callback*/
/* eslint-disable no-underscore-dangle*/
/* eslint-disable func-names*/
/**
 * @todo Add tests to the guessed data for undefined and nulled
 */
const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const dataExtractor = require('../controllers/components/dataExtractor');
const data = require('./data/dummyHTML').dataForExtraction;

chai.use(chaiAsPromised);
const expect = chai.expect;
describe('Data Extractor', function () {
  describe('Get Structured Data', function () {
    it('Should return an object with data', function () {
      return expect(dataExtractor.getStructuredData(data.structuredData))
        .to.contain.all.keys(['title', 'heading',
          'description',
          'metaDescription',
          'image',
          'name']);
    });
    it('Should check that each value must be string', function () {
      return expect(dataExtractor.getStructuredData(data.microdata))
        .to.have.property('description', `0.7 cubic feet countertop microwave.
  Has six preset cooking categories and convenience features like
  Add-A-Minute and Child Lock.`);
    });
    it('Should return a complete object for JSONLD', function () {
      return expect(dataExtractor.getStructuredData(data.jsonldData))
        .to.contain.all.keys(['title', 'heading',
          'description',
          'metaDescription',
          'image',
          'name']);
    });

    it('Should return a complete object with partial data', function () {
      return expect(dataExtractor.getStructuredData(data.withoutTitleDescription))
        .to.contain.all.keys(['title', 'heading',
          'description',
          'metaDescription',
          'image',
          'name']);
    });
    it('Should return an empty object when no data supplied', function () {
      return expect(dataExtractor.getStructuredData(''))
        .to.eql({});
    });
  });

  describe('Get Guessed Data', function () {
    it('Should return an object with data', function () {
      return expect(dataExtractor.getGuessedData(data.htmlForGuess)).to.contain.all.keys(
        ['title', 'heading', 'description',
          'metaDescription', 'name', 'image']);
    });
    // it('Should return an object with data', function () {
    //   return expect(dataExtractor.getGuessedData(data.partialhtmlForGuess)).to.contain.all.keys(
    //     ['title', 'heading', 'description',
    //       'metaDescription', 'name', 'image'])
    //     ;
    // });
  });
});
