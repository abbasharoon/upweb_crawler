/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable prefer-arrow-callback*/
/* eslint-disable no-underscore-dangle*/
/* eslint-disable func-names*/

const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const duplicateFinder = require('../controllers/components/duplicateFinder');
const data = require('./data/duplicateData').data;


chai.use(chaiAsPromised);
const assert = chai.assert;
const expect = chai.expect;
describe('Duplicate Finder', function () {
  describe.skip('Check Identical', function () {
    it('Should return the first identical string', function () {
      return assert.eventually.deepEqual(duplicateFinder.checkIdentical(
        'This is sample string', data.arrayForIdentical), {
        string: 'This is sample string', uriId: 0,
      });
    });
  });
  describe('Check Similarity', function () {
    it('Should return the string with highest Similarity level', function () {
      return expect(duplicateFinder.checkSimilar(
        data.hayForSimilar, data.needleForSimilar)).to.eql(
        {
          name: {
            uriId: 0, score: 1,
          },
          h1vsTitle: {
            score: 0,
            uriId: '',
          },
          title: {
            uriId: 0, score: 1,
          },
          description: {
            uriId: 0, score: 1,
          },
          heading: {
            uriId: 0, score: 1,
          },
          metaDescription: {
            uriId: '', score: 0,
          },
        });
    });
    it('Should return an empty object if no match is found', function () {
      return expect(duplicateFinder.checkSimilar(
        data.hayForSimilar, data.emptyNeedleForSimilar)).to.eql(
        {
          title: {
            score: 0, uriId: '',
          },
          metaDescription: {
            score: 0, uriId: '',
          },
          description: {
            score: 0, uriId: '',
          },
          name: {
            score: 0, uriId: '',
          },
          heading: {
            score: 0, uriId: '',
          },
          h1vsTitle: {
            score: 0,
            uriId: '',
          },
        });
    });
    it('Should return object with max similarity values', function () {
      return expect(duplicateFinder.checkSimilar(
        data.hayForSimilar, data.maxNeedleForSimilar)).to.eql(
        {
          title: {
            score: 1, uriId: 0,
          },
          metaDescription: {
            score: 0, uriId: '',
          },
          description: {
            score: 1, uriId: 2,
          },
          name: {
            score: 1, uriId: 1,
          },
          heading: {
            score: 0.6666666666666666, uriId: 0,
          },
          h1vsTitle: {
            score: 1,
            uriId: '',
          },
        });
    });
  });
  describe('Start', function () {
    it('Should not have any value with score less than 0.75', function () {
      return expect(duplicateFinder.start(
        data.hayForSimilar, data.maxNeedleForSimilar)).to.eventually.eql(
        {
          title: {
            score: 1, uriId: 0,
          },
          description: {
            score: 1, uriId: 2,
          },
          name: {
            score: 1, uriId: 1,
          },
          h1vsTitle: {
            score: 1,
            uriId: '',
          },
          uriId: 4,
        });
    });
    it('Should return an integer of uriId if no match is found', function () {
      return expect(duplicateFinder.start(
        data.hayForSimilar, data.emptyNeedleForSimilar)).to.eventually.equal(4);
    });
  });
});
