/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable prefer-arrow-callback*/
/* eslint-disable no-underscore-dangle*/
/* eslint-disable func-names*/

const chai = require('chai');
const data = require('./data/dummyHTML').dataForSize;
const dataSize = require('../controllers/components/dataSize');
const chaiAsPromised = require('chai-as-promised');

let tooLargeHtml = data.simpleHtml;
let lowHtmltoText = data.lowHtmlToWordsRatio;
let normalPage = data.simpleHtml;

chai.use(chaiAsPromised);
const expect = chai.expect;
describe('Data Size', function () {
  beforeEach(function () {
    for (let i = 0; i < 500; i += 1) {
      tooLargeHtml += data.simpleHtml;
    }
    for (let i = 0; i < 15; i += 1) {
      lowHtmltoText += data.lowHtmlToWordsRatio;
      normalPage += data.simpleHtml;
      normalPage += data.simpleHtml;
      normalPage += data.simpleHtml;
      normalPage += data.simpleHtml;
    }
  });
  describe('Check', function () {
    it('Should return an tooLargeHtml as true', function () {
      return expect(dataSize.check(tooLargeHtml, true)).to.eventually.eql({tooLargeHtml: true});
    });
    it('Should return too low words count', function () {
      return expect(dataSize.check(data.simpleHtml, true)).to.eventually.eql({lowWords: true});
    });
    it('Should return low html to text ratio', function () {
      return expect(dataSize.check(normalPage, true)).to.eventually.eql({});
    });
  });
});
