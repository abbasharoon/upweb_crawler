/* eslint-disable import/no-extraneous-dependencies */
/* eslint-disable prefer-arrow-callback*/
/* eslint-disable no-underscore-dangle*/
/* eslint-disable func-names*/
const chai = require('chai');
const robotics = require('../controllers/components/robotics');
const chaiAsPromised = require('chai-as-promised');
const nock = require('nock');
const robotsTxt = require('./data/robotsData').robotsTxt;

chai.use(chaiAsPromised);
const expect = chai.expect;

describe('Robotics', function () {
  this.timeout(50000);
  describe('Test Robots', function () {
    before(function () {
      nock('http://completerobots.com').get('/robots.txt').reply(200, robotsTxt.complete);
      nock('http://withparseerror.com').get('/robots.txt').reply(200, robotsTxt.spaceErros);
      nock('http://nositemap.com').get('/robots.txt').reply(200, robotsTxt.noSitemap);
    });
    it('Should return a complete object with sitemaps and disallowed urls', function () {
      return expect(robotics.start('http://completerobots.com'))
        .to.eventually.have.all.keys(['userAgents', 'sitemapinRobots']);
    });
    it('Should return an error with no sitemap', function () {
      return expect(robotics.start('http://nositemap.com'))
        .to.eventually.have.all.keys(['userAgents']);
    });
    it('Should return an object parse Errors', function () {
      return expect(robotics.start('http://withparseerror.com'))
        .to.eventually.be.contain.keys(['parseError']);
    });
    it.skip('Should return an error with no sitemap', function () {
      return expect(robotics.start('http://abbasharoon.com'))
        .to.eventually.be.rejectedWith(/EAI_AGAIN/);
    });
  });
});
