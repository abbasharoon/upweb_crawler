/**
 * @todo Make sure all items from ucDBDATA is added to db at the end
 */
const URI = require('urijs');
URI.preventInvalidHostname = false;

const Crawler = require('simplecrawler');
// const htmlTidy = require('htmltidy2');
const dataExtractor = require('./components/dataExtractor');
const dbUtil = require('./components/dbUtilities');
const uriFilter = require('./modules/urlFilter');
const pageFilter = require('./modules/pageFilter');
const cheerio = require('cheerio');
const duplicateFinder = require('./components/duplicateFinder');
const _ = require('lodash');
const tagAnalyzer = require('./components/tagsAnalyzer');
const dataSize = require('./components/dataSize');
const uriValidator = require('./components/urlValidator');
const db = require('../models');
const linkFinder = require('./components/linkFinder');
const Urijs = require('urijs');
const htmlToText = require('html-to-text');
const browserChecks = require('./modules/phantomjs');
const Pl = require('./progressLogger');
const emailNotifier = require('./components/emailNotifier');

const scan = function scan(initialData) {
  Urijs.preventInvalidHostname = true;
  const pl = new Pl(initialData.token);
  // Varibale to hold data from finding table
  let findings;
  // Data from initialchecks including url etc along with data for database
  const siteData = initialData;

  // Array for accumulating data from the pages,
  // Used for checking duplicates
  const extractedData = [];

  // A variable to keep track of running process
  let runningProcesses = 0;
  let crawlStopped = false;

  // Object to hold those uris that are to be added to database
  const dueUris = {};
  // An object containing all the urids for stored uris wrt to uri
  siteData.uriIds = {};
  siteData.fetchedUris;
  // object to hold uncompleted data for database,
  // usually it misses a urid which is fetched from
  // DB and then on completion the object is pushed to cDBData
  const ucDbData = {
    referrers: {},
    duplicateContents: {},
    htmlChecks: {},
    socialChecks: {},
    aboveFoldChecks: {},
  };
  // Completed Data for data base to be added on completion of the scan
  const cDbData = {
    referrers: [],
    duplicateContents: [],
    htmlChecks: [],
    socialChecks: [],
    aboveFoldChecks: [],
    business: [siteData.sequelizeData.business],
    sitemapsRobots: [siteData.sequelizeData.sitemapsRobots],
    scanChecks: [siteData.sequelizeData.scanChecks],
  };
  console.log(cDbData);
  /**
   * Object to hold the incomplete duplicate data for databased
   * As the duplicates requires two columns to be filled from uri table
   * We first queue it in this object for the first column
   * And then on getting that column, we push the object to ucDBData
   * For completion of the other column
   * */
  const duplicateHolder = {};
  /**
   * An Object that holds the scan ids of imp pages like cart and checkout etc
   * And Social Links for FB upto Youtube
   */
  siteData.impPages = {};
  // dbUtil prevents repeation for URI table, sitedata is used to set the id for
  // Host Id
  dbUtil.initilizar(siteData);

  // Initialize the crawler
  const crawler = new Crawler(siteData.url);
  // myDomain is used to check for the referrer Queue Item in the fetch Condtion
  // If the referrer is not equal to this then its 2nd level external domain and we don't want that
  crawler.myDomain = new Urijs(siteData.url).domain();
  crawler.maxDepth = 4;
  // Milliseconds
  crawler.interval = 100;
  // False = Allow external domains
  crawler.filterByDomain = false;
  // Decode response from buffer
  crawler.decodeResponses = true;
  crawler.maxConcurrency = 5;
  crawler.parseHTMLComments = false;
  crawler.parseScriptTags = false;
  crawler.downloadUnsupported = false;
  crawler.timeout = 30000;
  crawler.userAgent = 'UpWebSenaBot';
  // Colnames stores names of the columsn for tables viz are keys
  // Its used when completing the objects in ubDBData
  // The table name is passed and the column name is fetched from it
  const colNames = {
    referrers: 'fromUriId',
    htmlChecks: 'uriId',
    duplicateContents: 'matchedUriId',
    socialChecks: 'uriId',
    aboveFoldChecks: 'uriId',
  };
  // Running Db temporarily stores the uri path which is being
  // Fetched or inserted and is removed on competion of the
  // Request
  const runningDb = {};
  let scanFinished = false;
  const processCheck = function processCheck() {
    if (scanFinished) {
      return;
    }
    if (crawlStopped && runningProcesses === 0) {

      // On completion of scan we need to add all the objects to the database
      // We iterate over all the completed objects
      // For referrers, we change the name to the referred. To be fixed
      let dbProgress = 0;
      scanFinished = true;
      pl.logStatus('stopped');
      cDbData.scanHistory = [
        pl.getCounts(siteData.scanId, siteData.hostId, Object.keys(siteData.uriIds).length),
      ];
      dbUtil.clearOldData(siteData.scanId).then(() => {
        _.forEach(cDbData, (tableData, tableName) => {
          dbProgress += 1;
          console.log(tableName);
          console.log(tableData.length);
          db.sequelize
            .transaction(t =>
              // chain all your queries here. make sure you return them.
              db[tableName].bulkCreate(tableData, {
                transaction: t,
              }))
            .then(() => {
              dbProgress -= 1;
              console.log(`${tableName} Success`);

              if (dbProgress === 0) {
                dbUtil.updateScan(siteData.scanId).then((data) => {
                  if (data) {
                    pl.logStatus('completed');
                  }
                  emailNotifier.run(siteData.hostId, siteData.scanId);
                }).catch((err) => {
                  console.log(err);
                  pl.logStatus('error');
                });
              }
              // Transaction has been committed
              // result is whatever the result of the promise chain
              // returned to the transaction callback
            })
            .catch((err) => {
              dbProgress -= 1;
              if (dbProgress === 0) {
                pl.logStatus('error');
              }
              console.log(err);
              console.log(tableName);
              // Transaction has been rolled back
              // err is whatever rejected the promise chain returned to the transaction callback
            });
        });
      }).catch(() => {
        pl.logStatus('completed');
      });
    }
  };
  /**
   * Builds the object for duplicate tables and pushes it to the ubDBData
   * @function duplicates
   * @param dataObject {Object} - Dataobject returned from the duplicate utility
   * @param queueItem {Object} - The main Queue item which was compared
   */
  const duplicates = function duplicates(dataObject, queueItem) {
    // DataObject contains multiple items ranging from meta title to contents
    _.forEach(dataObject, (v) => {
      // Create a unique id for ucDBData
      const id = v.uriId + v.field;
      ucDbData.duplicateContents[id] = {
        // Duplicates function is called when the urid for the main uri is already
        // Fetched so we assign it right now,
        uriId: siteData.uriIds[queueItem.path],
        // For matchedUriId, we assign it temporarily
        matchedUriId: siteData.uriIds[v.uriId],
        findingsId: findings[v.field],
        // The value returned is on scale 0-1 so converting it percentage
        value: Math.round(v.score * 100),
        scanId: siteData.scanId,
      };
      pl.logData({
        duplicateContent: findings[v.field],
      }, queueItem.path);

      // need the complete queueItem for the dueURI
      // SO using the queue builin function to fetch the Object by
      // UriId
      crawler.queue.filterItems({
        path: v.uriId,
      }, (error, item) => {
        // The returned itme is an array so we pick the first item
        if (!error && item) {
          dueUri(item[0], 'duplicateContents', id);
        }
      });
    });
  };
  /**
   * @function This function completes the ucDbData & Duplicates by either fetching the
   *  the object and assigning or by checking siteData.uriIds
   * @param queueItem {Object} - The main Queue Item to be inserted or fetched
   * @param type {String} - The table name used as key in ucDBData, holding the Incomplete Object
   * @param id {String} - The key at which object was inseted into the type
   * @returns {*} - Returns Null or the id
   */
  const dueUrisHistory = {
    called: 0,
    db: 0,
  };
  const dueUri = function dueUri(queueItem, type, id) {
    dueUrisHistory.called += 1;
    // If the item is uri row is already created of fetched from db
    // Then it will be contained in the siteData.uriIds with key of its path
    if (siteData.uriIds[queueItem.path]) { //TODO path not found
      // If it exists and the type is set then complete the object and
      // And insert it into the database
      if (type) {
        ucDbData[type][id][colNames[type]] = siteData.uriIds[queueItem.path];
        cDbData[type].push(ucDbData[type][id]);
        // Logging the check typo that faied for front end live stats
        if (type === 'htmlChecks') {
          pl.logData({
            checkTypeFailed: ucDbData[type][id].findingsId,
          }, queueItem.path);
        }
      }
      // And then return the id
      return siteData.uriIds[queueItem.path];
    }
    // If the above check fails then we need to fetch it from db
    // But first check if there are already other objects that are due for this uri
    // If so, we will have an array for its path in the dueUris object
    // Else create one
    if (!dueUris[queueItem.path]) {
      dueUris[queueItem.path] = [];
    }
    // Now if type is set then push an object to the array of this uri
    // This array is later used to complete these objects from ucDbData
    // By the referecnce of its type and id
    if (type) {
      dueUris[queueItem.path].push({
        type,
        id,
      });
    }
    // This check prevents multiple inserts and selects by checking if a process is already in place
    if (!runningDb[queueItem.path]) {
      dueUrisHistory.db += 1;
      // Running process is incremneted to count running processes
      runningProcesses += 1;
      // console.log('Running Process Sat for uri' + runningProcesses);
      // Set the running DB object to true to mark a running process for this path
      runningDb[queueItem.path] = true;
      dbUtil.findSaveUri(queueItem, siteData.hostId).then((data) => {
        // console.log(dueUrisHistory);
        // On Completion of the request to db, assign the id to the uriIds object
        // So that it can be used in the above check
        siteData.uriIds[queueItem.path] = data.get('id'); //canot set uriid of underined
        // Then delete the rurunning db check, as its used in duplicates too
        delete runningDb[queueItem.path];
        // Now for all due objects, we itereate to complete them
        _.forEach(dueUris[queueItem.path], (v) => {
          // Firs assign the id to the col name that is due
          ucDbData[v.type][v.id][colNames[v.type]] = data.get('id');
          // Then push it into the ucDBData
          cDbData[v.type].push(ucDbData[v.type][v.id]);
          if (v.type === 'htmlChecks') {
            pl.logData({
              checkTypeFailed: ucDbData[v.type][v.id].findingsId,
            }, queueItem.path);
          }

          // And detlete the orignal one
          delete ucDbData[v.type][v.id];
        });
        // Now check if there are any duplicate items in the duplicate key
        if (duplicateHolder[queueItem.path]) {
          // It is (usually one for duplicates only) then run it through duplicate
          // Function and delete the item
          duplicates(duplicateHolder[queueItem.path], queueItem);
          delete duplicateHolder[queueItem.path];
        }
        // Deceremetnt the process and enjoy
        runningProcesses -= 1;
        processCheck();
      }).catch((error) => {
        console.log(error);
        // Else just decremtent the process
        runningProcesses -= 1;
        processCheck();
      });
    }
    return null;
  };
  /**
   * @function Referer builds objects for the referer table
   * @param queueItem {Object}
   */
  const referrer = function referrer(queueItem) {
    // Mark a running Process, usually broken urls or
    // Imporantant pages
    queueItem.stateData.contentLength = '';

    let referrerQueue;
    let toUriId = queueItem.path;
    // For broken links we will have no findings, so we will just
    // Identify them and assign them a status code
    if (!queueItem.findings) {
      queueItem.findings = queueItem.stateData.code;
    }
    // Now when we have got our main broken uri id, its time for the referrer uri id
    crawler.queue.filterItems({
      // QueueItem.referrer returns the full url so we search the queue by uri
      url: queueItem.referrer,
    }, (error, item) => {
      if (!error) {
        // Assign the first item in the returned array to the variable as referre
        referrerQueue = item[0];
        if (!item[0] || !referrerQueue.path) {
          referrerQueue = queueItem;
        }
        // Now check if the findings is array
        // Because link finder can return an array for important pages
        // As a link can pass many tests e.g faq/returns is true for faq and returns
        if (_.isArray(queueItem.findings)) {
          // If so, then itereate the array and and create and object type for each type of
          // Findings
          for (let i = 0; i < queueItem.findings.length; i += 1) {
            ucDbData.referrers[`${queueItem.path}-${queueItem.findings[i]}`] = {
              // A placeholder value for fromURiId
              fromUriId: siteData.uriIds[referrerQueue.path],
              toUriId: queueItem.url.substring(0, 500),
              // We just use a simple findings id in case of referrer table
              findingsId: queueItem.findings[i],
              scanId: siteData.scanId,
            };
            // And now feed it to the dueUri for further completion
            dueUri(referrerQueue, 'referrers', `${queueItem.path}-${queueItem.findings[i]}`);
          }
        } else {
          // If its not array then create a single object feed it to dueURi
          ucDbData.referrers[queueItem.path + 0] = {
            fromUriId: siteData.uriIds[referrerQueue.path],
            toUriId: queueItem.url.substring(0, 500),
            findingsId: queueItem.findings,
            scanId: siteData.scanId,
          };
          dueUri(referrerQueue, 'referrers', queueItem.path + 0);
        }
      }
    });
  };
  /**
   * @function Checks for broken links with wrt http code
   * @param queueItem
   */
  const handleBrokenUrl = function handleBrokenUrl(queueItem) {
    // These codes are not due to error and are allowed to be used
    if (queueItem.stateData.code !== 404 && queueItem.host !== crawler.host) {
      return;
    }
    const allowedCodes = [
      200, 300, 302, 400, 401, 402, 403, 405,
    ];
    // If its not the allowed code then add the item to the referrer
    if (allowedCodes.indexOf(queueItem.stateData.code) === -1 &&
      uriFilter[siteData.cms](queueItem.path)) {
      pl.logData({
        brokenUrl: queueItem.stateData.code,
      });
      if (queueItem.host !== crawler.host) {
        queueItem.path = queueItem.url;
      }
      referrer(queueItem);
    }
  };
  /**
   * @function Builds the Object for Html Checks table
   * @param data {Array} - An Array containing objects with the data for corresponding columns
   * @param queueItem
   */
  const htmlChecks = function htmlChecks(data, queueItem) {
    // Pretty much self explainatory
    _.forEach(data, (value) => {
      ucDbData.htmlChecks[queueItem.id + value.findings + value.value] = {
        findingsId: findings[value.findings],
        uriId: siteData.uriIds[queueItem.path],
        value: value.value,
        scanId: siteData.scanId,
      };

      dueUri(queueItem, 'htmlChecks', queueItem.id + value.findings + value.value);
    });
  };

  const browserurls = {};
  //
  //
  //
  //
  //
  //
  crawler
    .on('fetchcomplete', (queueItem, responseBuffer) => {
      // :pg a downloaded Url
      // We either download the file if its html or if it has no content length set
      // So we run the following checks for the html only files
      queueItem.stateData.contentLength = queueItem.stateData.actualDataSize;
      if (queueItem.stateData.contentType.indexOf('html') > -1 && uriFilter[siteData.cms](queueItem.path)) {
        // Crawl Limit is decremented here because this event is fired in parallel
        // Becaus eof which we can't decrement it after it's check. That can result in the if
        // Statements gettting passed for multipple events. Hence multiple data
        siteData.limit -= 1;
        runningProcesses += 1;
        // runningProcesses += 1;
        if (siteData.limit === 0) {
          setTimeout(() => {
            if (runningProcesses > 0) {
              console.log('Set Time Out Fired');
              runningProcesses = 0;
              processCheck();
            }
          }, 50000);
          console.log('Stoppppppppppppppppped');
          console.log("fsdsfdsf" + runningProcesses + "sdfsdf");
          crawler.stop();
          crawlStopped = true;
          processCheck();
        }
        if (siteData.limit < 0) {
          runningProcesses -= 1;
          return;
        }
        dueUri(queueItem);
        pl.logData({
          downloadedUrl: queueItem.path,
          requestLatency: queueItem.stateData.requestLatency,
        });
        // First we create a cheerio dom Object from the html to check and manipulate the html
        const $ = cheerio.load(responseBuffer);
        // Check for cart and checkout links and assign the value
        queueItem.cartLinks = linkFinder.cartLinks($);
        if (siteData.uriIds[queueItem.path]) {
          dbUtil.updateUri(siteData.uriIds[queueItem.path], queueItem);
        }

        // Data size checks returns two checks
        const dataSizeChecks = dataSize.check(responseBuffer);
        // IF they are true, we send them straight to the html checks
        if (dataSizeChecks.miniWords.isTrue) {
          htmlChecks([{
            findings: 'miniWords',
            value: dataSizeChecks.miniWords.value,
          }], queueItem);
        }
        if (dataSizeChecks.textToHtml.isTrue) {
          htmlChecks([{
            findings: 'textToHtml',
            value: dataSizeChecks.textToHtml.value,
          }], queueItem);
        }

        // Here we extract the data using node micro format and node unfluff
        const dataObject = dataExtractor.start(responseBuffer);
        // Now if the dataObject is not empty and
        if (!_.isEmpty(dataObject)) {
          // If its extracted from the microformat then we set the it in the queue Object
          // Microformat is only used on the products pages, so it means we have a product page
          // Which we can send to phantom js for further processing
          const bcData = {
            dataObject,
            scanId: siteData.scanId,
            url: queueItem.url,
          };
          if (siteData.browserChecks) {
            console.log('running Browser Checks');
            runningProcesses += 1;
            browserurls[queueItem.url] = 'Called';
            browserChecks(bcData).then((data) => {
              ucDbData.aboveFoldChecks[queueItem.id] = {
                addToCart: data.add_to_cart,
                image: data.image,
                title: data.title,
                description: data.description,
                price: data.price,
                searchBar: data.stickySearch,
                uriId: queueItem.path,
                scanId: siteData.scanId,
              };
              dueUri(queueItem, 'aboveFoldChecks', queueItem.id);
              runningProcesses -= 1;
              processCheck();
              pl.logData({
                aboveFoldCheck: data,
              }, queueItem.path);
              delete browserurls[queueItem.url];
            }).catch(() => {
              runningProcesses -= 1;
              processCheck();
            });
          }
          // We set a dataObject Uri for the duplicate to return it if it
          // Matches anything
          dataObject.uriId = queueItem.path;
          const duplicateData = duplicateFinder.start(extractedData, dataObject);
          // add this data to the extracted data array so that other pages can be compared with it
          extractedData.push(dataObject);
          // If duplicates is found then an object is returned
          if (_.isObject(duplicateData)) {
            // First delete the uriID as we will be itereating over the object
            delete duplicateData.uriId;
            // Now if this current queue is already created or fetched
            if (siteData.uriIds[queueItem.path]) {
              // Then pass it to duplicates function for further processing
              duplicates(duplicateData, queueItem);
            } else {
              // Else add it to the duplicateHolder object
              duplicateHolder[queueItem.path] = duplicateData;
              // And ping the dueUri object for adding/fetching the currenct queue Item
            }
          }

          // First we check for tags, forbidden, multiple and required ones
          const tagsCheck = tagAnalyzer.start($, crawler.myDomain);
          // Then for each of the three types, we iterate over the main Object returned
          _.forEach(tagsCheck, (v) => {
            // If its not empty then we send it to the html checks for further processing
            if (!_.isEmpty(v)) {
              htmlChecks(v, queueItem);
            }
          });
          // runningProcesses -= 1;
          // processCheck();
        }
        // Noe check if we have a product page
        // The page filter contains cms specific filters
        // Also if there is microformat then it means that the page is a product page
        if (pageFilter[siteData.cms]($) || queueItem.microformat) {
          const text = htmlToText.fromString(responseBuffer, {
            ignoreHref: true,
            ignoreImage: true,
            preserveNewlines: true,
          });
          // cDbData.scanChecks[0].productReviews.count += 1;
          if (text.toLowerCase().indexOf('review') > -1 || text.toLowerCase().indexOf('rating') > -1
            && cDbData.scanChecks[0].productReviews == 0) {
            cDbData.scanChecks[0].productReviews = 1;
            pl.logData({
              productReviews: 1,
            });
          }
        }
        runningProcesses -= 1;
        processCheck();
      }
    })
    //   .on('fetchclienterror', (error, URLData) => {
    //   // console.log(error);
    //   // console.log(URLData);
    // }).on('fetchtimeout', (error, URLData) => {
    //   console.log(error);
    //   console.log(URLData);
    // }).on('fetcherror', (error, URLData) => {
    //   console.log(error);
    //   console.log(URLData);
    // }).on('queueerror', (error, URLData) => {
    //   console.log(error);
    //   console.log(URLData);
    // }).on('fetchprevented', (error) => {
    //   console.log(error);
    // }).on('robotstxterror', (error) => {
    //   console.log(error);
    // })
    .on('complete', function(){
      let continueConstant = null;
      if (runningProcesses !== 0) {
        continueConstant = this.wait();
      }
      setTimeout(() => {
        if (runningProcesses > 0) {
          console.log('Set Time Out Fired');
          runningProcesses = 0;
          processCheck();
          if (continueConstant) {
            continueConstant();
          }
        }
      }, 50000);
      console.log('COmpleted--------------------------------------------------------------');
      console.log('COmpleted--------------------------------------------------------------');
      console.log('COmpleted--------------------------------------------------------------');
      console.log('COmpleted--------------------------------------------------------------');
      console.log('COmpleted--------------------------------------------------------------');
      console.log('COmpleted--------------------------------------------------------------');
      console.log('COmpleted--------------------------------------------------------------');
      crawlStopped = true;
      processCheck();
    })
    .on('fetchheaders', (queueItem) => {
      pl.logData({
        queuedUrl: queueItem.path,
      });
      // Check for the important pages like shipping page, cart page etc
      const linkChecks = linkFinder.impPages(queueItem.path);
      // It will return an array so if its not empty
      if (linkChecks.length !== 0) {
        // Then create an empty array to contain found ids
        const linkfindings = [];
        // Now itereate over all the types that are returned
        for (let i = 0; i < linkChecks.length; i += 1) {
          // If this type is not already detected Then do two things
          if (!siteData.impPages[linkChecks[i]]) {
            // First if its a shipment page, then mark the shipment column
            // In the business Object to 1, its 0 by default
            if (linkChecks[i] === 5) {
              cDbData.business.shipment = 1;
            }
            // Now push the specific id for the found page to linkfindings
            linkfindings.push(linkChecks[i]);
            pl.logData({
              impLinks: linkChecks[i],
            });
            // And also to the impPages array to prevent duplicate insertions
            siteData.impPages[linkChecks[i]] = true;
          }
        }
        // If the link findings array is not empty then handover the data
        // to the referrer function for further processing
        if (linkfindings.length !== 0) {
          // Add add the found ids to the queue item to be used there
          queueItem.findings = linkfindings;
          referrer(queueItem);
        }
      }
      // Check for lengthy, parametrized and underscored urls
      const uriState = uriValidator(queueItem.path);
      // If this object is not empty then handover
      // The data to htmlChecks
      if (!_.isEmpty(uriState)) {
        htmlChecks(uriState, queueItem);
      }
      // Handle Broken Links
      handleBrokenUrl(queueItem);
    });

  //TODO Remove the debug code
  // let originalEmit = crawler.emit;
  // crawler.emit = function (evtName, queueItem) {
  //   crawler.queue.countItems({fetched: true}, function (err, completeCount) {
  //     if (err) {
  //       throw err;
  //     }
  //
  //     crawler.queue.getLength(function (err, length) {
  //       if (err) {
  //         throw err;
  //       }
  //
  //       console.log("fetched %d of %d — %d open requests, %d open listeners",
  //         completeCount,
  //         length,
  //         crawler._openRequests.length,
  //         crawler._openListeners);
  //     });
  //   });
  //
  //   console.log(evtName, queueItem ? queueItem.url ? queueItem.url : queueItem : null);
  //   originalEmit.apply(crawler, arguments);
  // };
  /**
   * @todo Make sure that user is informed
   * When error occurs here
   */
  // We need the findings table data to associate different
  // Findings with it  , We start the crawling once we get that dat
  dbUtil.findings().then((data) => {
      dbUtil.getUris(siteData.hostId).then((datas) => {
        _.forEach(datas, (r) => {
          crawler.queueURL(r.uri, {url: r.referrerLink});
          if (r.statusCode === 200) {
            siteData.uriIds[r.uri] = r.id;
          }
        });
        return true;
      }).then(() => {
        // crawler.queue.countItems({}, function (error, count) {
        // });
      }).catch((err) => {
        console.log(err);
      });

      // In case of succes, we assign it to the findings varibale,
      // Turning it to an array
      findings = data;
      crawler.start();
      const dataForLog = initialData;
      dataForLog.findings = data;
      pl.logData({
        initialChecks: dataForLog,
      });
    }
  ).catch(() => {
    // If we fail, then we retry again
    dbUtil.findings().then((data2) => {
      // In case of succes then well and good
      findings = data2;
      crawler.start();
    }).catch((err2) => {
      pl.logStatus('Scan Failed');
      //   Else bye bye
    });
  });
  crawler.discoverResources = function (buffer, queueItem) {
    // The crawler default link discovere finds all the ids,
    // We only need few types of ids
    const links = [];

    // const contentTypeHeader = queueItem.stateData.contentType || "";
    //
    // const embeddedEncoding = /<meta[^>]*charset\s*=\s*["']?([\w\-]*)/i.exec(buffer.toString(undefined, 0, 512)) ||
    // [];  let encoding = contentTypeHeader.split("charset=")[1] || embeddedEncoding[1] || contentTypeHeader;
    // encoding = iconv.encodingExists(encoding) ? encoding : "utf8";

    // console.log(buffer.toString('utf8').substring(0, 100));
    // First we check if its html we are dealing with
    if (queueItem.stateData.contentType.indexOf('html') > -1) {
      // Then create a cheerio item from the buffer by converitng it to html first
      const $ = cheerio.load(buffer.toString('utf8'));
      // Check for a and link tags
      // a for discovering links, link for processing css
      // ,link[rel=stylesheet i]
      $('a[href]').each((index, element) => {
        links.push($(element).attr('href'));
      });
      // get these 3s for broken script
      // ,script[src],audio[src]
      $('img[src]').each((index, element) => {
        links.push($(element).attr('src'));
      });
      // Return the links
      return links;
    }
  };
  const crawlLimit = initialData.limit;
  const dontDownload = crawler.addDownloadCondition((queueItem, response) => {
    // Assign the mimetype to avoid retyping
    const mimeType = queueItem.stateData.contentType;
    // If its html page, or if it doesn't have a content length
    // Is not one of the forbidden links
    // Then add it
    // console.log('Sitedata ' + Object.keys(siteData.uriIds).length);
    // console.log('CrawlLimit ' + siteData.limit);
    if ((mimeType.indexOf('html') > -1 &&
        queueItem.stateData.code === 200 && siteData.limit < 0) &&
      (Object.keys(siteData.uriIds).length > crawlLimit)) {
      console.log(siteData.uriIds);
      console.log('Download False');
      return false;
    }
    // (queueItem.stateData.contentLength === 0 &&
    //   (mimeType.indexOf('image') > -1 ||
    //     mimeType.indexOf('css') > -1 ||
    //     mimeType.indexOf('javascript' > -1)
    //   ))
    return (mimeType.indexOf('text/html') > -1
      && uriFilter[siteData.cms](queueItem.path) &&
      new Urijs(queueItem.url).domain() === crawler.myDomain);
  });
  const conditionID = crawler.addFetchCondition((queueItem, referrerQueueItem) => {
    // This fetch condintion only allow urls on the same domain
    // And first page of the external domain
    if (siteData.filterum) {
      for (let i = 0; i < siteData.filterum.length; i += 1) {
        if (queueItem.path.indexOf(siteData.filterum[i]) > -1) {
          return false;
        }
      }
    }


    if (new Urijs(referrerQueueItem.url).domain() === crawler.myDomain) {
      // Also check for social links as our bot is blocked by robots.txt
      const socialChecks = linkFinder.socialChecks(queueItem.url);
      // If there is a social link detected and its not already added
      if (socialChecks && !siteData.impPages[socialChecks]) {
        // Then add the important pages to the link
        siteData.impPages[socialChecks] = true;
        // And add the object to the socialChecks  object
        // ucDbData.socialChecks[queueItem.url] = {
        //   link: queueItem.url,
        //   typeId: socialChecks,
        //   uriId: referrerQueueItem.path,
        //   scanId: siteData.scanId,
        // };
        // @todo Re activate it
        cDbData.socialChecks.push({
          link: queueItem.url,
          typeId: socialChecks,
          uriId: referrerQueueItem.path,
          scanId: siteData.scanId,
        });
        pl.logData({
          socialLinks: socialChecks,
        });

        // And ping the dueUri function
        // dueUri(referrerQueueItem, 'socialChecks', queueItem.url);
      }
      return true;
    }
    return false;
  });
};
module.exports = scan;
