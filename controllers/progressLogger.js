/* eslint-disable brace-style */
const Configs = require('./../configs/configurations.js');
const _ = require('lodash');

const configs = Configs();
const socket = require('socket.io-client')(configs.socketHost);
const jameel = {
  critical: {},
  seo: {},
};

socket.on('connect', () => {
  // socket.on('progressData', (data) => {
  //   console.log('Data Received in the connection');
  //   console.log(data);
  // });

  // console.log('Crawler Connected');
  // socket.on('connect_failed', () => {
  //   console.log('Connection Failed');
  // });
  // socket.on('error', () => {
  //   console.log('Message Error');
  // });
  // socket.on('messageSuccess', () => {
  //   console.log('Message Success');
  // });
  // socket.on('disconnect', () => {
  //   console.log('Crawler  disconnected');
  // });
});
// @todo it's not the same as report page
// @todo scanCheck is scanChecks while keys are camelCased intead of snake case
const issuesCategories = {
  critical: {
    percentage: 100,
    totalIssues: 26,
    category: 'a',
    includedChecks: {
      importantLinks: [8],
      scanChecks: ['wwwResolve'],
      sitemapsRobots: ['sitemap', 'robots', 'sitemapInRobots', 'sitemapWrong'],
    },
    countableChecks: {
      brokenLinks: ['brokenImage', 'brokenInternalLink', 'brokenExternalLink', 'serverErrorLink'],
      htmlCheck: [4, 5, 9, 10, 11, 12, 13, 14, 15, 16, 21, 22],
      duplicateContent: [29],
    }
  },
  seo: {
    count: 0,
    percentage: 100,
    totalIssues: 40,
    category: 'b',
    includedChecks: {
      importantLinks: [],
      scanChecks: ['wwwResolve', 'ssl'],
      sitemapsRobots: ['sitemap', 'robots', 'sitemapInRobots', 'sitemapWrong'],
      business: ['address', 'badge'],
    },
    countableChecks: {
      brokenLinks: ['brokenImage', 'brokenInternalLink', 'brokenExternalLink', 'serverErrorLink', 'temporaryRedirect'],
      htmlCheck: [1, 2, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 21, 22, 23, 24, 31],
      duplicateContent: [25, 26, 29, 30],
    }
  },
  recommendations: {
    count: 0,
    percentage: 100,
    totalIssues: 16,
    category: 'c',
    includedChecks: {
      importantLinks: [1, 3, 4, 5, 6, 7, 8, 9, 11],
      socialLinks: [12, 13, 14],
      scanChecks: ['productReviews'],
      business: ['address', 'phoneNumber', 'tradingHours'],
    },

  },
  cro: {
    count: 0,
    percentage: 0,
    totalIssues: 9,
    category: 'd',
    includedChecks: {
      importantLinks: [1, 10],
      scanChecks: ['liveChat',
        'productReviews',
        // @todo Complete these checks
        // 'related_products',
        // 'cart_button_size',
        // 'cart_button_color'
      ],
      business: ['phoneNumber', 'badge'],
    },
  },
};
const checksCount = {};
const issueTypesCount = {};

class processLogger {
  constructor(token) {
    this.token = token;
    this.socket = socket;
    checksCount[token] = {
      critical: {
        issuesCount: 0,
        typeCount: 0,
        percentage: 100,
      },
      seo: {
        issuesCount: 0,
        typeCount: 0,
        percentage: 100,
      },
      recommendations: {
        issuesCount: 0,
        typeCount: 0,
        percentage: 100,
      },
      cro: {
        issuesCount: 0,
        typeCount: 0,
        percentage: 100,
      },
      pagesCrawled: 0,
      urisWithIssues: 0,
      uniqueIssues: 0,
      totalIssuesCount: 0,
    };
    issueTypesCount[token] = {
      seo: {
        issueByTypeCount: [],
      },
      critical: {
        issueByTypeCount: [],
      },
      cro: {
        issueByTypeCount: [],
      },
      recommendations: {
        issueByTypeCount: [],
      },
      downloadedUrls: [],
      queuedUrls: [],
      urisWithIssues: [],
      uniqueIssues: [],
    };
  }


  setToken(newToken) {
    this.token = newToken;
  }

  issuesCountCalculator(data, token) {
    if (data.checkTypeFailed) {
      this.checkCountableIssue('htmlCheck', data.checkTypeFailed, token);
    }
    else if (data.downloadedUrl) {
      if (data.requestLatency > 10000) {
        checksCount[token].totalIssuesCount += 1;
        checksCount[token].seo.issuesCount += 1;
        checksCount[token].critical.issuesCount += 1;
        if (issueTypesCount[token].seo.issueByTypeCount.indexOf('request_latency') === -1) {
          issueTypesCount[token].seo.issueByTypeCount.push('request_latency');
          checksCount[token].seo.typeCount += 1;
          issueTypesCount[token].critical.issueByTypeCount.push('request_latency');
          checksCount[token].critical.typeCount += 1;
          //TODO REMOVE JAMEEL
          jameel.seo['request_latency'] += 0;
        }
        //TODO REMOVE JAMEEL
        jameel.seo['request_latency'] += 1;

      }
    }
    else if (data.aboveFoldCheck) {
      const abc = data.aboveFoldCheck;
      delete abc.aboveFoldCheck.stickySearch;
      delete abc.aboveFoldCheck.uriId;
      delete abc.aboveFoldCheck.scanId;
      let withIssue = false;
      _.forEach(abc, (v) => {
        if (v > 1) {
          withIssue = true;
        }
      });
      if (withIssue) {
        checksCount[token].seo.issuesCount += 1;
        checksCount[token].cro.issuesCount += 1;
        checksCount[token].totalIssuesCount += 1;
        if (checksCount[token].seo.issueByTypeCount.indexOf('aboveFoldCheck') === -1) {
          issueTypesCount[token].seo.issueByTypeCount.push('aboveFoldCheck');
          issueTypesCount[token].cro.issueByTypeCount.push('aboveFoldCheck');
          checksCount[token].seo.typeCount += 1;
          checksCount[token].critical.typeCount += 1;
        }
      }
    }
    else if (data.impLinks) {
      _.forEach(issuesCategories, (v, k) => {
        if (v.includedChecks.importantLinks.indexOf(data.impLinks) > -1) {
          if (issueTypesCount[token][k].issueByTypeCount.indexOf(`importantLinks${data.impLinks}`) > -1) {
            checksCount[token].totalIssuesCount -= 1;
            issueTypesCount[token][k].issueByTypeCount
              .splice(issueTypesCount[token][k].issueByTypeCount.indexOf(`importantLinks${data.impLinks}`), 1);
            checksCount[token][k].issuesCount -= 1;
            checksCount[token][k].typeCount -= 1;
            console.log(k);
            console.log(`importantLinks${data.impLinks}`);
            console.log(issueTypesCount[token][k].issueByTypeCount);
          }
        }
      });
    } else if (data.socialLinks) {
      if (issueTypesCount[token].recommendations.issueByTypeCount.indexOf(`socialLinks${data.socialLinks}`) > -1) {
        checksCount[token].totalIssuesCount -= 1;
        issueTypesCount[token].recommendations.issueByTypeCount
          .splice(issueTypesCount[token].recommendations.issueByTypeCount.indexOf(`socialLinks${data.socialLinks}`), 1);
        checksCount[token].recommendations.issuesCount -= 1;
        checksCount[token].recommendations.typeCount -= 1;
      }
    }
    else if (data.duplicateContent) {
      this.checkCountableIssue('duplicateContent', data.duplicateContent, token);
    } else if (data.brokenUrl) {
      checksCount[token].totalIssuesCount += 1;
      if (issueTypesCount[token].seo.issueByTypeCount.indexOf(`statusCode${data.brokenUrl}`) === -1) {
        issueTypesCount[token].seo.issueByTypeCount.push(`statusCode${data.brokenUrl}`);
        checksCount[token].seo.typeCount += 1;
        //TODO REMOVE JAMEEL
        jameel.seo['statusCode' + data.brokenUrl] = 0;
      }
      //TODO REMOVE JAMEEL
      jameel.seo['statusCode' + data.brokenUrl] += 1;
      checksCount[token].seo.issuesCount += 1;

      if (data.brokenUrl > 400 && data.brokenUrl < 600) {
        if (issueTypesCount[token].critical.issueByTypeCount.indexOf(`statusCode${data.brokenUrl}`) === -1) {
          issueTypesCount[token].critical.issueByTypeCount.push(`statusCode${data.brokenUrl}`);
          checksCount[token].critical.typeCount += 1;
        }
        checksCount[token].critical.issuesCount += 1;
      }
      // checksCount[token].seo.issuesCount += 1;
    } else if (data.initialChecks) {// console.log(data.initialChecks);
      const sequelizeData = data.initialChecks.sequelizeData;
      if (sequelizeData.scanChecks.cms !== 'upWebDefault') {
        const socketData = {
          siteCMS: sequelizeData.scanChecks.cms,
        };
        socketData.token = this.token;
        this.socket.emit('progressData', socketData);
      }
      for (let c = 0; c < [
        'importantLinks',
        'socialChecks',
        'sitemapsRobots',
        'business',
        'socialLinks',
        'scanChecks',
      ].length; c += 1) {
        const i = [
          'importantLinks',
          'socialChecks',
          'sitemapsRobots',
          'business',
          'socialLinks',
          'scanChecks',
        ][c];
        for (const cat in issuesCategories) {
          // console.log(cat);
          if (issuesCategories[cat].includedChecks[i]) {
            // console.log(i);
            for (let ict = 0; ict < issuesCategories[cat].includedChecks[i].length; ict += 1) {
              if (issuesCategories[cat].includedChecks[i][ict] === 'sitemap'
                && sequelizeData[i][issuesCategories[cat].includedChecks[i][ict]] === 2) {
                checksCount[token][cat].issuesCount += 1;
                issueTypesCount[token][cat].issueByTypeCount
                  .push(i + issuesCategories[cat].includedChecks[i][ict] + i);
                checksCount[token][cat].typeCount += 1;
                checksCount[token].totalIssuesCount += 1;
              }
              if (!sequelizeData || !sequelizeData[i] ||
                sequelizeData[i][issuesCategories[cat].includedChecks[i][ict]] === 0 ||
                !sequelizeData[i][issuesCategories[cat].includedChecks[i][ict]]) {

                checksCount[token][cat].issuesCount += 1;
                issueTypesCount[token][cat].issueByTypeCount
                  .push(i + issuesCategories[cat].includedChecks[i][ict]);
                checksCount[token][cat].typeCount += 1;
                checksCount[token].totalIssuesCount += 1;
              }
            }
          }
          // console.log('---------------------');
          // console.log('+');
          // console.log('+');
          // console.log('+');
          // console.log(cat);
          // console.log(issueTypesCount[token][cat]);
          // console.log(issueTypesCount[token][cat].issueByTypeCount);
          // console.log('+');
          // console.log('+');
          // console.log('+');
          // console.log('---------------------');
        }
      }
      // checksCount[token].totalIssuesCount = this.getUniqueIssuesCount(token).length;
    }
    else if (data.productReviews) {
      const ncIndex = issueTypesCount[token].recommendations.issueByTypeCount.indexOf('scanChecksproductReviews');
      if (ncIndex > -1) {
        checksCount[token].recommendations.issuesCount -= 1;
        checksCount[token].cro.issuesCount -= 1;
        issueTypesCount[token].recommendations.issueByTypeCount.splice(ncIndex, 1);
        issueTypesCount[token].cro.issueByTypeCount
          .splice(issueTypesCount[token].cro.issueByTypeCount.indexOf('scanChecksproductReviews'), 1);
        checksCount[token].recommendations.typeCount -= 1;
        checksCount[token].cro.typeCount -= 1;
      }
    }
    checksCount[token].uniqueIssues = this.getUniqueIssuesCount(token).length;
  }

  checkCountableIssue(table, value, token) {
    const criticalIssuesArray = issuesCategories.critical.countableChecks[table];
    const seoIssuesArray = issuesCategories.seo.countableChecks[table];
    checksCount[token].totalIssuesCount += 1;

    if (criticalIssuesArray.indexOf(value) > -1) {
      checksCount[token].critical.issuesCount += 1;
      if (issueTypesCount[token].critical.issueByTypeCount.indexOf(table + value) === -1) {
        issueTypesCount[token].critical.issueByTypeCount.push(table + value);
        // TODO REMOVE JAMEEL
        jameel.critical[table + value] = 0;
        checksCount[token].critical.typeCount += 1;
      }
      // TODO REMOVE JAMEEL
      jameel.critical[table + value] += 1;
    }
    if (seoIssuesArray.indexOf(value) > -1) {
      checksCount[token].seo.issuesCount += 1;
      // console.log('===============================');
      // console.log('SEO');
      // console.log(issueTypesCount[token].seo.issueByTypeCount);
      // console.log(checksCount[token].seo.issuesCount);
      // console.log('===============================');
      if (issueTypesCount[token].seo.issueByTypeCount.indexOf(table + value) === -1) {
        issueTypesCount[token].seo.issueByTypeCount.push(table + value);
        // TODO REMOVE JAMEEL
        jameel.seo[table + value] = 0;
        checksCount[token].seo.typeCount += 1;
      }
      // TODO REMOVE JAMEEL
      jameel.seo[table + value] += 1;
    }
    // TODO REMOVE JAMEEL
    // console.log(jameel);
    // console.log(issueTypesCount[token].seo.issueByTypeCount);
    // console.log(issueTypesCount[token].critical.issueByTypeCount);
  }


  getUniqueIssuesCount(token) {
    return (issueTypesCount[token].critical.issueByTypeCount
      .concat(issueTypesCount[token].seo.issueByTypeCount)
      .concat(issueTypesCount[token].recommendations.issueByTypeCount)
      .concat(issueTypesCount[token].cro.issueByTypeCount)).filter((value, index, self) => {
      return self.indexOf(value) === index;
    });
    // if (issueTypesCount[token].uniqueIssues.indexOf(id) === -1) {
    //   issueTypesCount[token].uniqueIssues.push(id);
    // }
  }


// Start the request
  logData(req, uriWIthIssue = null) {
    if (!issueTypesCount[this.token]) {
      return;
    }
    let data = {};
    if (uriWIthIssue) {
      if (issueTypesCount[this.token].urisWithIssues.indexOf(uriWIthIssue) === -1) {
        issueTypesCount[this.token].urisWithIssues.push(uriWIthIssue);
        checksCount[this.token].urisWithIssues += 1;
      }
    }
    if (req.queuedUrl || req.status || req.downloadedUrl) {
      this.issuesCountCalculator(req, this.token);
      data = req;
      data.token = this.token;
      this.socket.emit('progressData', data);
    } else {
      this.issuesCountCalculator(req, this.token);
      data.checksCount = checksCount[this.token];
      data.token = this.token;
      this.socket.emit('progressData', data);
    }
  }

  getCounts(scanId, hostId, pagesCrawled) {
    if (!checksCount[this.token]) {
      return;
    }
    return {
      criticalCount: checksCount[this.token].critical.issuesCount,
      criticalTypes: checksCount[this.token].critical.typeCount,
      seoCount: checksCount[this.token].seo.issuesCount,
      seoTypes: checksCount[this.token].seo.typeCount,
      recommendationsCount: checksCount[this.token].recommendations.issuesCount,
      recommendationsTypes: checksCount[this.token].recommendations.typeCount,
      croCount: checksCount[this.token].cro.issuesCount,
      croTypes: checksCount[this.token].cro.typeCount,
      issuesPages: checksCount[this.token].urisWithIssues,
      totalIssuesCount: checksCount[this.token].totalIssuesCount,
      uniqueIssues: this.getUniqueIssuesCount(this.token).length,
      pagesCrawled,
      scanId,
      hostId,
    };
  }

  logStatus(status) {
    const data = {
      status,
      token: this.token,
    };
    this.socket.emit('progressData', data);
    if (status === 'completed') {
      delete checksCount[this.token];
      delete issueTypesCount[this.token];
    }
  }
}

module
  .exports = processLogger;
