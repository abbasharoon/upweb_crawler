const pageKeywords = [
  {
    keyword: 'contact',
    id: 1,
  },
  {
    keyword: 'support',
    id: 1,
  },
  {
    keyword: 'gift-card',
    id: 2,
  },
  {
    keyword: 'coupon',
    id: 2,
  },
  {
    keyword: 'discount',
    id: 2,
  },
  {
    keyword: 'redeem',
    id: 2,
  },
  {
    keyword: 'voucher',
    id: 2,
  },
  {
    keyword: 'tos',
    id: 3,
  },
  {
    keyword: 'terms',
    id: 3,
  },
  {
    keyword: 'about',
    id: 4,
  },
  {
    keyword: 'company-info',
    id: 4,
  },
  {
    keyword: 'our-story',
    id: 4,
  },
  {
    keyword: 'who-are-we',
    id: 4,
  },
  {
    keyword: 'delivery',
    id: 5,
  },
  {
    keyword: 'shipment',
    id: 5,
  },
  {
    keyword: 'faq',
    id: 6,
  },
  {
    keyword: 'question',
    id: 6,
  },
  {
    keyword: 'help',
    id: 6,
  },
  {
    keyword: 'guarantee',
    id: 7,
  },
  {
    keyword: 'warranty',
    id: 7,
  },
  {
    keyword: 'privacy',
    id: 8,
  },
  {
    keyword: 'login',
    id: 9,
  },
  {
    keyword: 'account',
    id: 9,
  },
  {
    keyword: 'user',
    id: 9,
  },
  {
    keyword: 'signin',
    id: 9,
  }, {
    keyword: 'sign-in',
    id: 9,
  },
  {
    keyword: 'reward',

    id: 10,
  }, {
    keyword: 'affiliate',
    id: 10,
  }, {
    keyword: 'refund',
    id: 11,
  }, {
    keyword: 'return',
    id: 11,
  }];
const socialLinks = [
  {
    keyword: '://www.facebook.com',
    id: 12,
  },
  {
    keyword: '://facebook.com',
    id: 12,
  },
  {
    keyword: '://facebook.net',
    id: 12,
  },
  {
    keyword: '://www.facebook.net',
    id: 12,
  },
  {
    keyword: '://fb.me',
    id: 12,
  },
  {
    keyword: '://www.fb.me',
    id: 12,
  },
  {
    keyword: '://twitter.com',
    id: 13,
  },
  {
    keyword: '://www.twitter.com',
    id: 13,
  },
  {
    keyword: '://plus.google.com',
    id: 14,
  },
  {
    keyword: '://www.plus.google.com',
    id: 14,
  },
  {
    keyword: '://instagram.com',
    id: 15,
  },
  {
    keyword: '://www.instagram.com',
    id: 15,
  },
  {
    keyword: '://youtube.com',
    id: 16,
  },
  {
    keyword: '://www.youtube.com',
    id: 16,
  },
];
const linkFinder = {
  cartLinks($) {
    let result = 0;
    const cart = $('a[href*=cart i]').length > 0;
    const checkout = $('a[href*=checkout i]').length > 0;
    if (cart && checkout) {
      result = 1;
    } else if (cart) {
      result = 2;
    } else if (cart) {
      result = 3;
    }
    return result;
  },
  impPages(uri) {
    const links = [];
    if (uri) {
      for (let i = 0; i < pageKeywords.length; i += 1) {
        if (uri.toLowerCase().indexOf(pageKeywords[i].keyword) > -1) {
          links.push(pageKeywords[i].id);
        }
      }
    }
    return links;
  },
  socialChecks(url) {
    if (url) {
      for (let i = 0; i < socialLinks.length; i += 1) {
        if (url.toLowerCase().indexOf(socialLinks[i].keyword) > -1) {
          return socialLinks[i].id;
        }
      }
    }
    return false;
  },
};
module.exports = linkFinder;
