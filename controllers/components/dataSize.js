/**
 * This module Finds the text to html ratio, html size and words count
 * @module components/dataSize
 * @requires htmlToText - html-to-text to convert the given html to text
 * @todo Add different word count for product pages if required
 */
const htmlToText = require('html-to-text');
/**
 * @function {object} dataSize
 */
const dataSize = {
  /**
   * Count the words based on the spaces by removing all
   * the duplicate spaces and tabs first
   * @param data
   * @returns {Number}
   */
  countWords(data) {
    return data.replace(/\s\s+/g, ' ')
      .split(' ').length;
  },
  /**
   * Checks all the three parameters and returns either empty or full object depending on the module
   * @param html
   * @param uriId
   * @returns {Promise} Object with the three states as true
   */
  check(html) {
    if (!html) {
      throw new Error('Parameters are missing');
    }
    const text = htmlToText.fromString(html, {
      wordwrap: null,
      ignoreHref: true,
      ignoreImage: true,
    });
    const wordCount = this.countWords(text);
    const htmlSize = Buffer.byteLength(html);
    const textSize = Buffer.byteLength(text);
    const data = {
      miniWords: {
        value: wordCount,
      },
      textToHtml: {
        value: textSize / htmlSize,
      },
    };
    if (wordCount < 200) {
      data.miniWords.isTrue = true;
    }
    if ((textSize / htmlSize) <= 0.1) {
      data.textToHtml.isTrue = true;
    }
    return data;
  },
};

module.exports = dataSize;
