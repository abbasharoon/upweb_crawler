/**
 * @todo Add a fix for the parse errors
 * @todo Lines should be trimmed in robotto parsing
 */
const robotto = require('robotto');
const _ = require('lodash');
const robotsParser = require('robots-parser');

const robotics = {
  getSitemap(url, content) {
    const sitemaps = robotsParser(url, content);
    return sitemaps.getSitemaps();
  },
  getDisallowed(userAgents) {
    const disallowed = [];
    _.forEach(userAgents, (value) => {
      if (!_.isEmpty(value.disallow)) {
        disallowed.push(value.disallow);
      }
    });
    return disallowed;
  },
  start(url) {
    const robotsUrl = robotto.getRobotsUrl(url);
    const results = {
      sitemap: null,
      sitemapInRobots: 0,
      robots: 0,
    };
    console.log(robotsUrl);
    return new Promise((resolve, reject) => {
      robotto.fetch(robotsUrl, (err, content) => {
        if (err) {
          console.log(err);
          console.log(content);
          reject(results);
          return;
        }
        results.robots = 1;
        const sitemaps = this.getSitemap(robotsUrl, content);
        if (!_.isEmpty(sitemaps)) {
          results.sitemapInRobots = 1;
          results.sitemap = sitemaps[0];
        }
        try {
          const userAgents = robotto.parse(content).userAgents;
          if (_.isEmpty(userAgents)) {
            results.robots = 2;
          }
        } catch (error) {
          results.robots = 3;
        }
        resolve(results);
      });
    });
  },
};
module.exports = robotics;
