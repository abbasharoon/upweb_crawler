/**
 * This module detects the similarity Between strings
 * And reports them if its more than 0.75 on a scale of 0-1
 * @module components/duplicateFinder
 * @namespace duplicateFinder
 * @requires stringSimilarity - String-similarity module to calculate the string differences
 * @requires _ - Lodash library to calculate array
 */
const stringSimilarity = require('string-similarity');
const _ = require('lodash');

const duplicateFinder = {
  /**
   *
   * @todo Add the identical Function if required in Future
   */
  // checkIdentical(hay, needle) {
  // return new Promise((resolve, reject) => {
  //   for (let i = 0; i < Object.keys(stack).length; i += 1) {
  //     const str1 = hay.replace(/\s\s+/g, ' ');
  //     const str2 = stack[i].string.replace(/\s\s+/g, ' ');
  //     if (str1 == str2) {
  //       resolve(stack[i]);
  //       return;
  //     }
  //   }
  //   reject(new Error('No Match Found'));
  // });
  // },

  /**
   * @param hay {Array} Array of objects holding Individual data
   * @param needleObject {Object} Object to be compared with the rest of data
   * @returns {{title: {score: number, uriId: nu}, metaDescription: {score: number, uriId: string}, description:
   *   {score: number, uriId: string}, name: {score: number, uriId: string}, heading: {score: number, uriId: string}}}
   */
  checkSimilar(hay, needleObject) {
    const similar = {
      title: {
        score: 0, uriId: '', field: 'dupMTitle',
      },
      metaDescription: {
        score: 0, uriId: '', field: 'dupMDesc',
      },
      description: {
        score: 0, uriId: '', field: 'dupContents',
      },
      name: {
        score: 0, uriId: '', field: 'dupName',
      },
      heading: {
        score: 0, uriId: '', field: 'dupHeading',
      },
      h1vsTitle: {
        score: 0, uriId: needleObject.uriId, field: 'dupH1Title',
      },
    };

    if (needleObject.title && needleObject.heading) {
      const similarityScore = stringSimilarity.compareTwoStrings(
        needleObject.title, needleObject.heading);
      if (similarityScore > 0.9) {
        similar.h1vsTitle.score = similarityScore;
      }
    }
    _.forEach(hay, (obj) => {
      if (obj.title && needleObject.title) {
        const similarityScore = stringSimilarity.compareTwoStrings(obj.title, needleObject.title);
        if (similarityScore > similar.title.score) {
          similar.title.score = similarityScore;
          similar.title.uriId = obj.uriId;
        }
      }
      if (obj.metaDescription && needleObject.metaDescription) {
        const similarityScore = stringSimilarity.compareTwoStrings(
          obj.metaDescription, needleObject.metaDescription);
        if (similarityScore > similar.metaDescription.score) {
          similar.metaDescription.score = similarityScore;
          similar.metaDescription.uriId = obj.uriId;
        }
      }
      if (obj.description && needleObject.description) {
        const similarityScore = stringSimilarity.compareTwoStrings(
          obj.description, needleObject.description);
        if (similarityScore > similar.description.score) {
          similar.description.score = similarityScore;
          similar.description.uriId = obj.uriId;
        }
      }
      if (obj.description && needleObject.name) {
        const similarityScore = stringSimilarity.compareTwoStrings(obj.name, needleObject.name);
        if (similarityScore > similar.name.score) {
          similar.name.score = similarityScore;
          similar.name.uriId = obj.uriId;
        }
      }
      if (obj.heading && needleObject.heading) {
        const similarityScore = stringSimilarity.compareTwoStrings(
          obj.heading, needleObject.heading);
        if (similarityScore > similar.heading.score) {
          similar.heading.score = similarityScore;
          similar.heading.uriId = obj.uriId;
        }
      }
    });
    return similar;
  },
  /**
   *
   * @param hay
   * @param needle
   * @returns {Promise} Object with only matched keys else empty object is returned
   */
  start(hay, needle) {
    if (!_.isArray(hay) || !_.isObject(needle)) {
      throw new Error('Invalid or Missing Arguments detected');
    }
    const similarity = this.checkSimilar(hay, needle);
    _.forEach(similarity, (value, key) => {
      if (value.score < 0.75) {
        delete similarity[key];
      }
    });
    if (!_.isEmpty(similarity)) {
      similarity.uriId = needle.uriId;
      return similarity;
    }
    return null;
  },
};

module.exports = duplicateFinder;
