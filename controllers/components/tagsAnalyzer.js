const urijs = require('urijs');
/**
 * This module checks for required, disallowed and duplicate tags
 * @module components/tagAnalyzer
 * @exports tagAnalyzer
 * @event start#missingRequiredTags
 * @event start#foundDisallowedTags
 * @event start#foundMultipleTags
 */

const tagsAnalyzer = {
  /**
   * Checks compulsory tags [title,meta : description, keywords, viewport, charset;, doctype]
   * @function checkRequiredTags
   * @param {Object} $ - Cheerio Dom Object
   * @returns {Promise} - With Missing Tags
   */
  checkRequiredTags($) {
    // Array of the tags to check for
    const tags = [
      {
        name: 'title',
        selector: 'title',
        field: 'mMtitle',
      },
      {
        name: 'description',
        selector: 'meta[name=description i]',
        field: 'mMDesc',
      },
      {
        name: 'keywords',
        selector: 'meta[name=keywords i]',
        field: 'mMKeyword',
      },
      {
        name: 'viewport',
        selector: 'meta[name=viewport i]',
        field: 'mViewport',
      },
      {
        name: 'charset',
        selector: 'meta[charset i]',
        field: 'mEncoding',
      },
      {
        name: 'language',
        selector: 'html[lang i]',
        field: 'mLang',
      },
      {
        name: 'h1',
        selector: 'h1',
        field: 'mH1',
      },
    ];
    const missingTags = [];

    // Check if the passed parameter is cheerio object
    if (typeof $.html !== 'function') {
      // If not then throw an error
      throw new Error('Parameter provided is not a cheerio dom object');
    }
    for (let i = 0; i < tags.length; i += 1) {
      // If the tag is not found then add it to the missing tags array
      if ($(tags[i].selector).length === 0) {
        missingTags.push({
          findings: tags[i].field,
          value: null,
        });
      }
    }

    // There is no selector for doctype in cheerio so
    // We are using JS method to find it in the dom string
    if (!$.html().toLowerCase().includes('<!doctype')) {
      missingTags.push({
        findings: 'mDoctype',
        value: null,
      });
    }
    // Return the missing Tags Array
    return missingTags;
  },
  /**
   * Checks tags which are not allowed [flash object, iframe, images without alt tag]
   * @function checkDisallowedTags
   * @param {Object} $  - Cheerio Dom Object
   * @param {String} domain  - Domain of the website being crawled
   * @returns {Promise} - With Found Tags
   */
  checkDisallowedTags($, domain) {
    // Array of the tags to check for
    const tags = [
      {
        name: 'img',
        selector: 'img[alt="" i]',
        field: 'mAlt',
        valueAttribute: 'src',
      },
      {
        name: 'img',
        selector: 'img:not([alt i])',
        field: 'mAlt',
        valueAttribute: 'src',
      },
      {
        name: 'nofollow',
        selector: 'a[rel~=nofollow i]',
        field: 'Nofollow',
        valueAttribute: 'href',
      },
      {
        name: 'iframe',
        selector: 'iframe',
        field: 'iframe',
        valueAttribute: 'src',
      },
    ];
    const foundTags = [];
    // Check if the passed parameter is cheerio object
    if (typeof $.html !== 'function') {
      throw new Error('Parameter provided is not a cheerio dom object');
    }


    for (let i = 0; i < tags.length; i += 1) {
      const detectedTags = $(tags[i].selector);
      if (detectedTags.length > 0) {
        $(tags[i].selector).each((index, element) => {
          if ($(element).attr(tags[i].valueAttribute) &&
            urijs($(element).attr(tags[i].valueAttribute)).hostname()) {
            let fieldValue = tags[i].field;
            if (tags[i].valueAttribute === 'href') {
              console.log($(element).attr('rel'));
              if (urijs($(element).attr(tags[i].valueAttribute)).domain() !== domain) {
                fieldValue = `ext${tags[i].field}`;
              } else {
                fieldValue = `int${tags[i].field}`;
              }
              console.log(fieldValue);
            }
            foundTags.push({
              findings: fieldValue,
              value: $(element).attr(tags[i].valueAttribute),
            });
          }
        });
      }
    }

    // Flash is embedded using <object> tag
    // We first detect the object tag and then check
    // Whether is used for flash or something else
    const objectTags = $('object');
    let embeddedFlash = 0;
    if (objectTags.length > 0) {
      objectTags.each((index, element) => {
        const data = $(element).attr('data');
        if (data && data.toLowerCase().includes('.swf')) {
          embeddedFlash += 1;
        }
      });
      if (embeddedFlash > 0) {
        foundTags.push({
          findings: 'flash',
          value: embeddedFlash,
        });
      }
    }
    return foundTags;
  },
  /**
   * Checks for tags max occurrence [a: to be set, h1: 1]
   * @function checkMultipleTags
   * @param {object} $ - Cheerio Dom Object
   * @returns {Promise} - With Multiple Occurrence
   */
  checkMultipleTags($) {
    // Array of the tags to check for
    const tags = [
      {
        name: 'a',
        selector: 'a',
        max: '200',
        field: 'manyLinks',
      },
      {
        name: 'h1',
        selector: 'h1',
        max: '1',
        field: 'manyH1',
      },
    ];
    const excessTags = [];

    // Check if the passed parameter is cheerio object
    if (typeof $.html !== 'function') {
      throw new Error('Parameter provided is not a cheerio dom object');
    }

    // Rest is self explanatory
    for (let i = 0; i < tags.length; i += 1) {
      const detectedTags = $(tags[i].selector);
      if (detectedTags.length > tags[i].max) {
        excessTags.push({
          findings: tags[i].field,
          value: detectedTags.length,
        });
      }
    }

    return excessTags;
  },

  /**
   *    * Starts the analysis by calling all the three methods on the passed HTML
   * @function start
   * @param {String} $ - HTML to run tests on,
   * @event start#missingRequiredTags
   * @event start#foundDisallowedTags
   * @event start#foundMultipleTags
   * @param domain - Domain of the site
   */
  start($, domain) {
    const data = {};

    // Fires the events when the method returns some data.
    data.requiredTags = this.checkRequiredTags($);
    data.disallowedTags = this.checkDisallowedTags($, domain);
    data.multipleTags = this.checkMultipleTags($);
    return data;
  },
};


module.exports = tagsAnalyzer;
