const db = require('../../models');
const _ = require('lodash');

const dbUtilities = {
  findSaveUri(queueItem, hostId) {
    return new Promise((resolve, reject) => {
      if (queueItem.stateData.requestLatency > 32767) {
        queueItem.stateData.requestLatency = 32767;
      }
      if (!queueItem.stateData.contentType) {
        queueItem.stateData.contentType = 'unknown';
      }
      db.uris.findOrCreate({
        where: {
          uri: queueItem.path,
          hostId,
        },
        defaults: {
          uri: queueItem.path,
          type: queueItem.stateData.contentType,
          statusCode: queueItem.stateData.code,
          responseTime: queueItem.stateData.requestLatency,
          contentLength: queueItem.stateData.contentLength,
          cartLinks: queueItem.cartLinks,
          referrerLink: queueItem.referrer,
          hostId,
        },
      }).spread((uriInstance) => {
        resolve(uriInstance);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  findings() {
    const findings = {};
    return new Promise((resolve, reject) => {
      db.findings.findAll({
        attributes: ['id', 'textId'],
      }).then((rows) => {
        _.forEach(rows, (r) => {
          findings[r.textId] = r.id;
        });
        resolve(findings);
      }).catch((err) => {
        reject(err);
      });
    });
  },
  clearOldData(scanId) {
    // @todo remove uris table
    return new Promise((resolve, reject) => {
      let res = 0;
      db.business.destroy({
        where: {
          scanId,
        },
      }).then(() => {
        res += 1;
        if (res === 2) {
          resolve(true);
        }
      }).catch((err) => {
        reject(err);
      });
      db.scanChecks.destroy({
        where: {
          scanId,
        },
      }).then(() => {
        res += 1;
        if (res === 2) {
          resolve(true);
        }
      }).catch((err) => {
        reject(err);
      });
    });
  },
  updateUri(id, queueItem) {
    return new Promise((resolve, reject) => {
      if (queueItem.stateData.requestLatency > 32767) {
        queueItem.stateData.requestLatency = 32767;
      }
      db.uris.update(
        {
          statusCode: queueItem.stateData.code,
          responseTime: queueItem.stateData.requestLatency,
          contentLength: queueItem.stateData.contentLength,
          cartLinks: queueItem.cartLinks,
        },
        {
          where: {
            id,
          },
        })
        .then(() => {
          resolve(true);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  updateScan(scanId) {
    return new Promise((resolve, reject) => {
      db.scans.update(
        {
          status: 2,
        },
        {
          where: {
            id: scanId,
          },
        })
        .then(() => {
          resolve(true);
        })
        .catch((err) => {
          reject(err);
        });
    });
  },
  getUris(hostId) {
    return db.uris.findAll({
      where: {
        hostId,
      },
    });
  },
  initilizar(siteData) {
    // this.data.hostId = siteData.hostId;
  },
};

module.exports = dbUtilities;
