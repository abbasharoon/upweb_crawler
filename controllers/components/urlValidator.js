/**
 * @todo Ask dave for the maximum number of queries
 */
const urijs = require('urijs');

module.exports = function urlValidator(url) {
  const data = [];
  const uriQueries = urijs.parse(url).query;
  const queriesObjects = urijs.parseQuery(uriQueries);

  if (url.indexOf('_') > -1) {
    data.push({
      findings: '_links',
      value: url,
    });
  } else if (Object.keys(queriesObjects).length > 4) {
    data.push({
      findings: 'manyParam',
      value: url,
    });
  } else if (url > 110) {
    data.push({
      findings: 'longUrls',
      value: url,
    });
  }
};

