/**
 * @todo add jdoc and tests
 */

const request = require('request');
const db = require('../../models');
const Configs = require('./../../configs/configurations');

const configs = Configs();

const emailNotifier = {
  run(hostId, scanId) {
    db.scanQueue.findOne({
      where: {
        hostId,
        status: 2,
      },
      order: 'scan_time DESC',
    }).then((data) => {
      if (data && data.get('mailReport')) {
        const opts = {
          url: configs.emailNotificationUrl,
          method: 'POST',
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          form: {
            authKey: configs.sitemapValidatorSecret,
            scanId,
          },
        };
        request(opts, () => {
        });
      }
      if (data && data.get('id')) {
        db.scanQueue.update(
          {
            status: 3,
          },
          {
            where: {
              id: data.get('id'),
            }
          })
          .then(() => {
          });
      }
    });
  },
};


module.exports = emailNotifier;

