const async = require('async');

module.exports = function tradingHours(text) {
  const matches = [
    /(?:mon[\s]{0,3}(?:\/|-|–|to|through)[\s]{0,3}(?:fri|sat|thu|sun))/gi,
    /(?:monday[\s]{0,3}(?:\/|–|-|to|through)[\s]{0,3}(?:friday|saturday|thursday|sunday))/gi,
  ];
  const matchFirst = text.match(matches[0]);
  const match2nd = text.match(matches[1]);
  return !!(matchFirst || match2nd);
};
