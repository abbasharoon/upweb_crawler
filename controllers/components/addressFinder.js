/**
 * Module to Find/Extract Address
 * @module components/addressFinder
 * @event addressFound
 * @requires async
 * @requires models/geography
 * @requires lodash
 * @requires fs
 * @requires path
 * @requires cheerio
 * @requires events
 * @requires @google/maps
 * @todo Add support for search of address tag
 * @todo Use microformats to detect formatted address
 * @todo Extend the search to address related tags
 * @todo Add Events
 */


const async = require('async');
const geography = require('../../models/geography');
const _ = require('lodash');
const fs = require('fs');
const path = require('path');
const cheerio = require('cheerio');
const Events = require('events').EventEmitter;
const googleMapsClient = require('@google/maps').createClient({
  key: 'AIzaSyA51oRBGeRSpkxZOtt3141o55682JpbI6Y',
});

const events = new Events();
let runningProcesses = true;
const runningZipChecks = {};
let promiseResolve;
let promiseReject;
let html;
let host;
let $;
let that;
let text;
/*
 Our process can either end in the main callback or in the zip checker.
 The reason is too many async iterations
 Resulting in finishing the main process while the zip checker is still querying the database
 So before we start analysing the possible addresses,
 we need to know whether there are any calls left to the database
 Patterns to match the Zip Code, or state or county of a country
 */
const initialPatterns = {
  0: {
    country: 'US',
    pattern: /(?:<[A-Za-z]+)[^<>]+>[^<]*(?:Alabama|AL|Montana|MT|Alaska|AK|Nebraska|NE|Arizona|AZ|Nevada|NV|Arkansas|AR|New Hampshire|NH|California|CA|New Jersey|NJ|Colorado|CO|New Mexico|NM|Connecticut|CT|New York|NY|Delaware|DE|North Carolina|NC|Florida|FL|North Dakota|ND|Georgia|GA|Ohio|OH|Hawaii|HI|Oklahoma|OK|Idaho|ID|Oregon|OR|Illinois|IL|Pennsylvania|PA|Indiana|IN|Rhode Island|RI|Iowa|IA|South Carolina|SC|Kansas|KS|South Dakota|SD|Kentucky|KY|Tennessee|TN|Louisiana|LA|Texas|TX|Maine|ME|Utah|UT|Maryland|MD|Vermont|VT|Massachusetts|MA|Virginia|VA|Michigan|MI|Washington|WA|Minnesota|MN|West Virginia|WV|Mississippi|MS|Wisconsin|WI|Missouri|MO|Wyoming|WY)[^>]*<[/][^>]+>/gm,
    patternTagless: /(?:Alabama|AL|Montana|MT|Alaska|AK|Nebraska|NE|Arizona|AZ|Nevada|NV|Arkansas|AR|New Hampshire|NH|California|CA|New Jersey|NJ|Colorado|CO|New Mexico|NM|Connecticut|CT|New York|NY|Delaware|DE|North Carolina|NC|Florida|FL|North Dakota|ND|Georgia|GA|Ohio|OH|Hawaii|HI|Oklahoma|OK|Idaho|ID|Oregon|OR|Illinois|IL|Pennsylvania|PA|Indiana|IN|Rhode Island|RI|Iowa|IA|South Carolina|SC|Kansas|KS|South Dakota|SD|Kentucky|KY|Tennessee|TN|Louisiana|LA|Texas|TX|Maine|ME|Utah|UT|Maryland|MD|Vermont|VT|Massachusetts|MA|Virginia|VA|Michigan|MI|Washington|WA|Minnesota|MN|West Virginia|WV|Mississippi|MS|Wisconsin|WI|Missouri|MO|Wyoming|WY)/gm,
    patternForText: /(?:.{0,30}|[\n]){0,3}(?:Alabama|AL|Montana|MT|Alaska|AK|Nebraska|NE|Arizona|AZ|Nevada|NV|Arkansas|AR|New Hampshire|NH|California|CA|New Jersey|NJ|Colorado|CO|New Mexico|NM|Connecticut|CT|New York|NY|Delaware|DE|North Carolina|NC|Florida|FL|North Dakota|ND|Georgia|GA|Ohio|OH|Hawaii|HI|Oklahoma|OK|Idaho|ID|Oregon|OR|Illinois|IL|Pennsylvania|PA|Indiana|IN|Rhode Island|RI|Iowa|IA|South Carolina|SC|Kansas|KS|South Dakota|SD|Kentucky|KY|Tennessee|TN|Louisiana|LA|Texas|TX|Maine|ME|Utah|UT|Maryland|MD|Vermont|VT|Massachusetts|MA|Virginia|VA|Michigan|MI|Washington|WA|Minnesota|MN|West Virginia|WV|Mississippi|MS|Wisconsin|WI|Missouri|MO|Wyoming|WY)(?:.{0,30}|[\n]){0,3}/gm,
  },
  1: {
    country: 'CA',
    pattern: /(?:<[A-Za-z]+)[^<>]+>[^<]*(?:AB|Alberta|BC|British Columbia|MB|Manitoba|NB|New Brunswick|NL|Newfoundland|Labrador|NS|Nova Scotia|NU|Nunavut|NT|Northwest Territories|ON|Ontario|PE|Prince Edward Island|QC|Quebec|SK|Saskatchewan|YT|Yukon)[^>]*<[/]?[^>]+>/gm,
    patternTagless: /(?:AB|Alberta|BC|British Columbia|MB|Manitoba|NB|New Brunswick|NL|Newfoundland|Labrador|NS|Nova Scotia|NU|Nunavut|NT|Northwest Territories|ON|Ontario|PE|Prince Edward Island|QC|Quebec|SK|Saskatchewan|YT|Yukon)/gm,
    patternForText: /(?:.{0,30}|[\n]){0,3}(?:AB|Alberta|BC|British Columbia|MB|Manitoba|NB|New Brunswick|NL|Newfoundland|Labrador|NS|Nova Scotia|NU|Nunavut|NT|Northwest Territories|ON|Ontario|PE|Prince Edward Island|QC|Quebec|SK|Saskatchewan|YT|Yukon)(?:.{0,30}|[\n]){0,3}/gm,
  },
  2: {
    country: 'GB',
    pattern: /(?:<[A-Za-z]+)[^<>]+>[^<]*(?:GIR[ ]?0AA|(?:(?:AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|BX|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(?:\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\d{1,4})[^>]*<[/][^>]+>/gm,
    patternTagless: /GIR[ ]?0AA|(?:(?:AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|BX|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\d{1,4}/gm,
    patternForText: /(?:.{0,30}|[\n]){0,3}(?:GIR[ ]?0AA|(?:(?:AB|AL|B|BA|BB|BD|BH|BL|BN|BR|BS|BT|BX|CA|CB|CF|CH|CM|CO|CR|CT|CV|CW|DA|DD|DE|DG|DH|DL|DN|DT|DY|E|EC|EH|EN|EX|FK|FY|G|GL|GY|GU|HA|HD|HG|HP|HR|HS|HU|HX|IG|IM|IP|IV|JE|KA|KT|KW|KY|L|LA|LD|LE|LL|LN|LS|LU|M|ME|MK|ML|N|NE|NG|NN|NP|NR|NW|OL|OX|PA|PE|PH|PL|PO|PR|RG|RH|RM|S|SA|SE|SG|SK|SL|SM|SN|SO|SP|SR|SS|ST|SW|SY|TA|TD|TF|TN|TQ|TR|TS|TW|UB|W|WA|WC|WD|WF|WN|WR|WS|WV|YO|ZE)(\d[\dA-Z]?[ ]?\d[ABD-HJLN-UW-Z]{2}))|BFPO[ ]?\d{1,4})(?:.{0,30}|[\n]){0,3}/gm,
  },
  3: {
    country: 'AU',
    pattern: /(?:<[A-Za-z]+)[^<>]+>[^<]*(?:ACT|Australian Capital Territory|NSW|New South Wales|NT|Northern Territory|QLD|Queensland|SA|South Australia|TAS|Tasmania|VIC|Victoria|WA|Western Australia)[^>]*<[/][^>]+>/gm,
    patternTagless: /(?:ACT|Australian Capital Territory|NSW|New South Wales|NT|Northern Territory|QLD|Queensland|SA|South Australia|TAS|Tasmania|VIC|Victoria|WA|Western Australia)/gm,
    patternForText: /(?:.{0,30}|[\n]){0,3}(?:ACT|Australian Capital Territory|NSW|New South Wales|NT|Northern Territory|QLD|Queensland|SA|South Australia|TAS|Tasmania|VIC|Victoria|WA|Western Australia)(?:.{0,30}|[\n]){0,3}/gm,
  },
};
//    Common words used in addresses
const commonWords = /[^a-z](?:ALLEE|ALLEY|ALLY|ALY|ANEX|ANNEX|ANNX|ANX|ARC|ARCADE|AV|AVE|AVEN|AVENU|AVENUE|AVN|AVNUE|BAYOO|BAYOU|BCH|BEACH|BEND|BND|BLF|BLUF|BLUFF|BLUFFS|BOT|BTM|BOTTM|BOTTOM|BLVD|BOUL|BOULEVARD|BOULV|BR|BRNCH|BRANCH|BRDGE|BRG|BRIDGE|BRK|BROOK|BROOKS|BURG|BURGS|BYP|BYPA|BYPAS|BYPASS|BYPS|CAMP|CP|CMP|CANYN|CANYON|CNYN|CAPE|CPE|CAUSEWAY|CAUSWA|CSWY|CEN|CENT|CENTER|CENTR|CENTRE|CNTER|CNTR|CTR|CENTERS|CIR|CIRC|CIRCL|CIRCLE|CRCL|CRCLE|CIRCLES|CLF|CLIFF|CLFS|CLIFFS|CLB|CLUB|COMMON|COMMONS|COR|CORNER|CORNERS|CORS|COURSE|CRSE|COURT|CT|COURTS|CTS|COVE|CV|COVES|CREEK|CRK|CRESCENT|CRES|CRSENT|CRSNT|CREST|CROSSING|CRSSNG|XING|CROSSROAD|CROSSROADS|CURVE|DALE|DL|DAM|DM|DIV|DIVIDE|DV|DVD|DR|DRIV|DRIVE|DRV|DRIVES|EST|ESTATE|ESTATES|ESTS|EXP|EXPR|EXPRESS|EXPRESSWAY|EXPW|EXPY|EXT|EXTENSION|EXTN|EXTNSN|EXTS|FALL|FALLS|FLS|FERRY|FRRY|FRY|FIELD|FLD|FIELDS|FLDS|FLAT|FLT|FLATS|FLTS|FORD|FRD|FORDS|FOREST|FORESTS|FRST|FORG|FORGE|FRG|FORGES|FORK|FRK|FORKS|FRKS|FORT|FRT|FT|FREEWAY|FREEWY|FRWAY|FRWY|FWY|GARDEN|GARDN|GRDEN|GRDN|GARDENS|GDNS|GRDNS|GATEWAY|GATEWY|GATWAY|GTWAY|GTWY|GLEN|GLN|GLENS|GREEN|GRN|GREENS|GROV|GROVE|GRV|GROVES|HARB|HARBOR|HARBR|HBR|HRBOR|HARBORS|HAVEN|HVN|HT|HTS|HIGHWAY|HIGHWY|HIWAY|HIWY|HWAY|HWY|HILL|HL|HILLS|HLS|HLLW|HOLLOW|HOLLOWS|HOLW|HOLWS|INLT|IS|ISLAND|ISLND|ISLANDS|ISLNDS|ISS|ISLE|ISLES|JCT|JCTION|JCTN|JUNCTION|JUNCTN|JUNCTON|JCTNS|JCTS|JUNCTIONS|KEY|KY|KEYS|KYS|KNL|KNOL|KNOLL|KNLS|KNOLLS|LK|LAKE|LKS|LAKES|LAND|LANDING|LNDG|LNDNG|LANE|LN|LGT|LIGHT|LIGHTS|LF|LOAF|LCK|LOCK|LCKS|LOCKS|LDG|LDGE|LODG|LODGE|LOOP|LOOPS|MALL|MNR|MANOR|MANORS|MNRS|MEADOW|MDW|MDWS|MEADOWS|MEDOWS|MEWS|MILL|MILLS|MISSN|MSSN|MOTORWAY|MNT|MT|MOUNT|MNTAIN|MNTN|MOUNTAIN|MOUNTIN|MTIN|MTN|MNTNS|MOUNTAINS|NCK|NECK|ORCH|ORCHARD|ORCHRD|OVAL|OVL|OVERPASS|PARK|PRK|PARKS|PARKWAY|PARKWY|PKWAY|PKWY|PKY|PARKWAYS|PKWYS|PASS|PASSAGE|PATH|PATHS|PIKE|PIKES|PINE|PINES|PNES|PL|PLAIN|PLN|PLAINS|PLNS|PLAZA|PLZ|PLZA|POINT|PT|POINTS|PTS|PORT|PRT|PORTS|PRTS|PR|PRAIRIE|PRR|RAD|RADIAL|RADIEL|RADL|RAMP|RANCH|RANCHES|RNCH|RNCHS|RAPID|RPD|RAPIDS|RPDS|REST|RST|RDG|RDGE|RIDGE|RDGS|RIDGES|RIV|RIVER|RVR|RIVR|RD|ROAD|ROADS|RDS|ROUTE|ROW|RUE|RUN|SHL|SHOAL|SHLS|SHOALS|SHOAR|SHORE|SHR|SHOARS|SHORES|SHRS|SKYWAY|SPG|SPNG|SPRING|SPRNG|SPGS|SPNGS|SPRINGS|SPRNGS|SPUR|SPURS|SQ|SQR|SQRE|SQU|SQUARE|SQRS|SQUARES|STA|STATION|STATN|STN|STRA|STRAV|STRAVEN|STRAVENUE|STRAVN|STRVN|STRVNUE|STREAM|STREME|STRM|STREET|STRT|ST|STR|STREETS|STE|SMT|SUITE|SUMIT|SUMITT|SUMMIT|TER|TERR|TERRACE|THROUGHWAY|TRACE|TRACES|TRCE|TRACK|TRACKS|TRAK|TRK|TRKS|TRAFFICWAY|TRAIL|TRAILS|TRL|TRLS|TRAILER|TRLR|TRLRS|TUNEL|TUNL|TUNLS|TUNNEL|TUNNELS|TUNNL|TRNPK|TURNPIKE|TURNPK|UNDERPASS|UN|UNION|UNIONS|VALLEY|VALLY|VLLY|VLY|VALLEYS|VLYS|VDCT|VIA|VIADCT|VIADUCT|VIEW|VW|VIEWS|VWS|VILL|VILLAG|VILLAGE|VILLG|VILLIAGE|VLG|VILLAGES|VLGS|VILLE|VL|VIS|VIST|VISTA|VST|VSTA|WALK|WALKS|WALL|WY|WAY|WAYS|WELL|WELLS|WLS)[^a-z]/igm;
// Object to hold the detected (possible) address and
// their assessments to decide a winner at the end
const possibleAddresses = {};
// Object that we will use later to store the addresses
// that we have filtered based on various metrices
const filteredAddresses = {};
// For is a placeholder for country symbol. We will use it to build the Object of possible addresses

// This is the function for final evaluation of the selected address
// Using api from google maps

/**
 * Finds the address inside the provided text or dom
 * @type {class}
 */
const addressFinder = {
  /**
   * Checks and Validates the address against the google map api
   * @function checkWithGoogle
   * @param {Object} selectedAddress - to be checked against google api
   * @returns {Promise} Returns JSON with data or rejects the promise
   */
  checkWithGoogle(selectedAddress) {
    const address = selectedAddress;
    return new Promise((fulfillCheck, rejectCheck) => {
      if (!address) {
        rejectCheck(new Error('Address String is empty'));
      }
      // First geocode the address, if it is successful then it means that the address exists
      if (!address.string) {
        address.string = host;
      }
      googleMapsClient.geocode({
        address: address.string,
      }, (err, response) => {
        if (!err && response.json.status !== 'ZERO_RESULTS') {
          const result = response.json.results[0];
          result.country = address.country;
          fulfillCheck(result);
        } else {
          // But most of the time, google geocode needs the exact string to evaluate
          // So we can use the places to find the place
          googleMapsClient.places({
            query: address.string, location: [address.latitude, address.longitude],
          }, (err2, response2) => {
            if (!err2 && response2.json.status !== 'ZERO_RESULTS') {
              const result = response2.json.results[0];
              result.country = address.country;
              fulfillCheck(result);
            } else {
              // If that fails too then use then use the host name to find the address in the
              // suspected area
              googleMapsClient.places({
                query: host, location: [address.latitude, address.longitude],
              }, (err3, response3) => {
                if (!err3 && response3.json.status !== 'ZERO_RESULTS') {
                  const result = response3.json.results[0];
                  result.country = address.country;
                  fulfillCheck(result);
                } else if (err) {
                  rejectCheck(err);
                } else {
                  // else there is no address to find here
                  fulfillCheck(address);
                }
              });
            }
          });
        }
      });
    });
  },

// This function is called when all the possible addresses are detected
// Possible addresses are analysed and the best one is selected
  /**
   * Filters the possible address and selects the best ones
   * It returns the promise generated inside
   * @function addressFilter
   */
  addressFilter() {
    // Our Object of possible counties is nested at three levels the first level is based on country
    async.each(possibleAddresses, (country, firstCallback) => {
      // Now we are inside the country and we will have Object
      // containing blocks of possible addresses
      async.each(country, (blocks, secondCallback) => {
        // Now we are at the block level, each block contains a string,
        // common words, zip, division and lat long
        async.each(blocks, (block, thirdCallback) => {
          // Now itereating  over the single block to get individual strings
          // Check if the string is empty or is of more then 250 character length
          // or is missing the zip
          // or is too small to be an address
          if (block.string && block.string.length < 150 && block.zip &&
            block.string.length > 25 && block.commonWords) {
            // If the string pass all the tests then add it a simple object for final process
            // We are using the string length as key so that
            // we can get the shortest possible string to verify later
            filteredAddresses[block.string.length] = block;
            filteredAddresses[block.string.length].confidence = 100;
          } else if (block.string && block.string.length < 250 && block.zip &&
            block.string.length > 15 && block.commonWords) {
            filteredAddresses[block.string.length] = block;
            filteredAddresses[block.string.length].confidence = 75;
          } else if (block.string && block.string.length < 250 && block.zip &&
            block.string.length > 15) {
            filteredAddresses[block.string.length] = block;
            filteredAddresses[block.string.length].confidence = 50;
          } else if (block.string && block.string.length > 10 &&
            block.string.length < 350 && block.zip) {
            filteredAddresses[block.string.length] = block;
            filteredAddresses[block.string.length].confidence = 25;
          }
          // callback for block level
          thirdCallback(null);
        }, (err) => {
          // callback for blocks  level
          secondCallback(err);
          //    end of foreach for the blocks
        });
      }, (error) => {
        // Main callback
        firstCallback(error);
        //    end of foreach for country
      });
    }, (error) => {
      if (!error) {
        const address = _.last(_.sortBy(filteredAddresses, ['confidence']));
        // After getting the filtered arrays, we will now sort
        // the arrays by the lenght of the string
        // shorter strings are preffered
        that.checkWithGoogle(address).then((data) => {
          promiseResolve(data);
          events.emit('addressFound', data);
        }).catch((err) => {
          promiseReject(err);
        });
      } else {
        promiseReject(error);
      }
    });
  },

// THis function checks the zip in the database to verify it
  zipChecker(matchedZip, matchedState, g, i, p, matchedText) {
    // Where object for sequilize
    let where;
    // For Us and Au, the zips are simple so we need to cross check them using the states as well
    if (g === 'US' || g === 'AU') {
      where = {
        zip: matchedZip, state: matchedState,
      };
    } else if (g === 'CA') {
      matchedZip.forEach((zip, index, array) => {
        //  noinspection Eslint
        array[index] = zip.replace(/\s/g, '');
      });
      // For GB and Uk, we can use the zips only due to the uniqueness of the pattern
      where = {
        zip: matchedZip,
      };
    } else {
      where = {
        zip: matchedZip,
      };
    }
    return geography[g.toLowerCase()].find({
      where,
    }).then((result) => {
      // If result is not empty then it means that the zip is correct
      if (result) {
        // Assign the zip and the text to the object
        possibleAddresses[g][i][p].zip = result.zip;
        possibleAddresses[g][i][p].string = matchedText;
        possibleAddresses[g][i][p].latitude = result.latitude;
        possibleAddresses[g][i][p].longitude = result.longitude;
      }
      // Delete the entry for this query.
      // Running zip keeps the log of the calls sent to the database
      delete runningZipChecks[g + i + p];
      // If all the zips have been processed and the main callback
      // is called (which means all iterations have finished)
      if (Object.keys(runningZipChecks).length === 0 && runningProcesses === false) {
        if (possibleAddresses.length === 0) {
          promiseResolve({});
        }
        // Then call the address filter to process the addresses
        that.addressFilter();
      }
    }).catch((error) => {
      if (error) {
        throw new Error(error);
      }
    });
  },

  cheerioIterator(domElements, preMatchedText, preMatchedState, country, g, i, subCallback) {
    const matchedText = preMatchedText;
    const matchedState = preMatchedState;

    function buildPossibleAddress(index) {
      const elementText = $(this).text();

      // Check it its text is the same as the detected string above
      // So that we can eliminate the tags which don't have addresses
      if (elementText === matchedText[0]) {
        // if the text is same then choose the parent node of the element and extract its text
        // We will select up to 3 levels of the parent elements
        matchedText[1] = $(this).parent().text();
        matchedText[2] = $(this).parent().parent().text();
        matchedText[3] = $(this).parent().parent().parent()
          .text();
        // Now we will iterate over all the 4 string,
        // 0 is the original one and 1,2,3 for parent nodes
        for (let p = 0; p < 4; p += 1) {
          // Create the last nesting object in the possible addresses
          possibleAddresses[g][i][p] = {};
          // Add the String to the array so that we can
          // calculate the best later in the address filter
          // For the main string, we will be using 0 index,
          possibleAddresses[g][i][p] = {};
          possibleAddresses[g][i][p].string = matchedText[0];
          possibleAddresses[g][i][p].division = matchedState[0];
          // We use this subIndex to mark the end of the above async.each
          const subIndex = index - (2 - p);

          // Checking for common word in the string
          const detectedCommonWords = matchedText[p].match(commonWords);
          if (detectedCommonWords) {
            possibleAddresses[g][i][p].commonWords = detectedCommonWords;
          }

          if (matchedState[0].length > 2) {
            _.map(country, (o) => {
              if (o.name.toLowerCase() === matchedState[0].toLowerCase()) {
                matchedState[0] = o.key;
              }
            });
          }

          let matchedZip = null;
          if (g === 'GB') {
            matchedZip = matchedState[0];
            matchedState[0] = null;
            // Now check if the country object, that contians google's i8n data
            //    contains the state or county name
            // PAKISTAN ZINDABAD
          } else if (Object.prototype.hasOwnProperty.call(country, matchedState[0])) {
            // Create a new pattern from the JSON  key zip inside the country json object
            let zipPattern;
            if (g === 'CA') {
              zipPattern = new RegExp('[a-z]\\d[a-z][\\s]?\\d[a-z]\\d', 'gmi');
            } else {
              zipPattern = new RegExp(`[\\S]*(?:${country[matchedState[0]].zip})[\\S]*`, 'g');
            }

            // //Now run the test over the the original string as well as the parent string
            matchedZip = matchedText[p].match(zipPattern);
            //    End of if-country.h1asOwnProperty Check
          }


          if (matchedZip) {
            // Then send the request for the query as above
            runningZipChecks[g + i + p] = true;
            that.zipChecker(matchedZip, matchedState[0], g, i, p, matchedText[p]);
            //    End of the if-matched
          }

          if (subIndex === domElements.length) {
            subCallback();
          }
          //    End of the loop iteration over the parent node
        }
        // same as subindex but for the main loop
        //    End if elementText=matchedText
      } else if (index + 1 === domElements.length) {
        subCallback();
      }
      //    End of cheerio selector.each
    }

    domElements.each(buildPossibleAddress);
  },

  /**
   * This function finds the address in the plain text
   * @function checkTextPatterns
   * @returns {void}
   */
  checkTextPatterns() {
    // Check the checkPatterns function for details
    async.each(initialPatterns, (matchedBlock, callback) => {
      const matched = text.match(matchedBlock.patternForText);
      if (matched) {
        const g = matchedBlock.country;
        possibleAddresses[g] = {};
        fs.readFile(path.join(__dirname, `../modules/libaddress/${matchedBlock.country}.json`), 'utf8', (err, data) => {
          if (err) {
            callback(err);
          }
          // Get the country data like zip codes regex
          // and state names by parsing the current country file.
          const country = JSON.parse(data);
          // we use i for sub iterations, Its fo
          let i = 0;
          // Js match returns an array for regex even if it is one element, Iterate over the array
          // Using async function to iterate because there is a an async query to database and
          // that takes time, so to handle the overall integrity of the possibleAddresses array
          // and to Call the callbacks on right time, we are using another async array
          async.each(matched, (matchedString, subCallback) => {
            // Get the detected String only (we get the whole string above)
            const matchedState = matchedString.match(matchedBlock.patternTagless);
            // Create block for each string that we have detected
            possibleAddresses[g][i] = {
              0: {},
            };
            const matchedText = matchedString;
            //Add the country, we may need it in phonelibrary for address validation
            possibleAddresses[g][i][0].country = matchedBlock.country;
            possibleAddresses[g][i][0].division = matchedState[0];
            const detectedCommonWords = matchedText.match(commonWords);

            if (detectedCommonWords) {
              possibleAddresses[g][i][0].commonWords = detectedCommonWords;
            }

            /**
             * {If the state name is longer then assign the shorter version because
                             * we have only keys at the database}
             */
            if (g !== 'GB' && matchedState[0].length > 2) {
              _.map(country, (o) => {
                if (o.name.toLowerCase() === matchedState[0].toLowerCase()) {
                  matchedState[0] = o.key;
                }
              });
            }

            let matchedZip = null;
            if (g === 'GB') {
              matchedZip = matchedState[0];
              matchedState[0] = null;
              // Now check if the country object, that contians google's i8n data
              //    contains the state or county name
              // PAKISTAN ZINDABAD
            } else if (Object.prototype.hasOwnProperty.call(country, matchedState[0])) {
              // Create a new pattern from the JSON  key zip inside the country json object
              let zipPattern;
              if (g === 'CA') {
                zipPattern = new RegExp('[a-z]\\d[a-z][\\s]?\\d[a-z]\\d', 'gmi');
              } else {
                zipPattern = new RegExp(`[\\S]*(?:${country[matchedState[0]].zip})[\\S]*`, 'g');
              }

              // //Now run the test over the the orignal string as well as the parent string
              matchedZip = matchedText.match(zipPattern);
              //    End of if-country.h1asOwnProperty Check
            }
            if (matchedZip) {
              // Then send the request for the query as above
              runningZipChecks[g + i + 0] = true;
              that.zipChecker(matchedZip, matchedState[0], g, i, 0, matchedText);
              //    End of the if-matched
            }

            i += 1;

            subCallback();
            //    Emd of iterate function for the matchecd block
          }, (error) => {
            callback(error);
            //    End of the sub async.each function
          });
          //    End of the fs.readFile
        });
      } else {
        callback();
      }
    }, (err) => {
      if (err) {
        promiseReject(err);
      }
      runningProcesses = false;
      // Now check if the zips have been processed, if they are
      // (which is rare to happen at this step) then process the possible addresses
      if (Object.keys(runningZipChecks).length === 0 && runningProcesses === false) {
        if (Object.keys(possibleAddresses).length === 0) {
          promiseResolve({});
        }
        that.addressFilter();
      }
    });
  },

  checkPatterns() {
    // Now let's start with the main process, first iterate the object holding regex pattern
    // for countries
    async.each(initialPatterns, (matchBlock, callback) => {
      // For each individual block, we need to run the pattern against the provided html
      const matched = html.match(matchBlock.pattern);
      // Make sure that the matched element is not empty or anything
      if (matched) {
        // Assign the county code to g, will be used later to fetch the country specific files
        const g = matchBlock.country;
        // Create a sub object for the country
        possibleAddresses[g] = {};
        // Loading the relative file of address data downloaded from https://chromium-i18n.appspot.com/ssl-address
        fs.readFile(path.join(__dirname, `../modules/libaddress/${matchBlock.country}.json`), 'utf8', (err, data) => {
          if (err) {
            callback(err);
          }
          // Get the country data like zipcodes regex and state names by parsing
          // the current country file. noinspection JSUnresolvedVariable
          const country = JSON.parse(data);
          // we use i for sub itereations, Its fo
          let i = 0;
          // Js match returns an array for regex even if it is one element, Iterate
          // over the array Using async function to iterate because there is a an
          // async query to database and that takes time, so to handle the overall
          // integrity of the possibleAddresses array and to Call the callbacks on
          // right time, we are using another async array
          async.each(matched, (matchedString, subCallback) => {
            // Escape the special characters beacause they casue
            // problems for regex and json parser And cheerio as well
            matchedString.escapeSpecialChars();
            // Get the detected String only (we get the whole string above)
            const matchedState = matchedString.match(matchBlock.patternTagless);

            // Converting the matched string to DOM object via cheerio The string is first
            // loaded to cheerio and then the root of document
            // is selected As the first child
            // is our string so we select it and convert the matched string to Matched Dom
            const matchedDom = cheerio.load(matchedString).root().children();

            // If its not null or empty
            if (matchedState && matchedDom.html() !== null) {
              // Create block for each string that we have detected
              possibleAddresses[g][i] = {};
              // Get the tag name of the element so that we can search for it later
              // in the whole dom We search for it because our detected string can
              // be short and may contain only the zip or state name, so we will
              // need to fetch their parents if required
              try {
                matchedDom.prop('tagName');
              } catch (error) {
                subCallback();
                return;
              }
              const tagName = matchedDom.prop('tagName');
              // Get the class, if it exists
              const tagClass = matchedDom.attr('class');
              // Get the Id, if it exists
              const tagId = matchedDom.attr('class');
              // Extract the pure text string to run further tests on it from
              // Google's i18n library
              const matchedText = [matchedDom.text()];
              // Selecting all the tags that matches the tag of the detected string,
              // $ is cheerio object viz passed to it
              let cheerioSelector = $(tagName.toLowerCase());
              // then Check if any class exists and if does then use that or
              // the first class as filter We are using the split and trim to
              // select a single class
              if (tagClass) {
                cheerioSelector = $(tagName).filter(`.${tagClass.trim().split(' ')[0]}`);
                //    Else check if any id exists and if it does
                // then use that id to find element
              } else if (tagId) {
                cheerioSelector = $(tagName).filter(`#${tagId.trim().split(' ')[0]}`);
              }
              // Filter all the tags so that we can get the one we detected earlier
              that.cheerioIterator(cheerioSelector, matchedText,
                matchedState, country, g, i, subCallback);
              //    End of if-matchedState && matchedDom
            } else {
              subCallback();
            }
            i += 1;
            //    Emd of iterate function for the matchec bloc
          }, (error) => {
            callback(error);
            //    End of the sub async.each function
          });
          //    End of the fs.readFile
        });
        //    End of if-matched at the very top
      } else {
        callback();
      }
      // End of the main async.each on matched
    }, (err) => {
      // if any of the file processing produced an error, err would equal that error
      if (err) {
        // One of the iterations produced an error.
        // All processing will now stop.
        promiseReject(err);
      }
      // Its the main callback, if we reach here without error then run mark the
      // process indicator as false
      runningProcesses = false;
      // Now check if the zips have been processed, if they are (which is rare to
      // happen at this step) then process the possible addresses
      if (Object.keys(runningZipChecks).length === 0 && runningProcesses === false) {
        if (Object.keys(possibleAddresses).length === 0) {
          promiseResolve({});
        }
        that.addressFilter();
      }
    });
  },

  /**
   * Prepares the data and function to start the search
   * @function find
   * @param {String} data - HTML or text string
   * @param {String} hostString - the host address used in google check
   * @param {Boolean} isText - If true, then the text based patterns are run
   * @returns {Object} Promise - Either an empty object or one with data
   * @throws error when address string passed to google check is empty
   */
  find(data, hostString, isText) {
    // Passing the data to file level variables so that we don't have to keep passing them
    host = hostString;
    // Set this to that to avoid scope conflicts
    that = this;
    return new Promise((resolve, reject) => {
      // Same as above, we need to resolve the promise else where
      setTimeout(() => {
        reject(new Error('Address not found'));
      }, 600);
      promiseResolve = resolve;
      promiseReject = reject;
      // If text is passed then run checks for text else for html
      if (isText) {
        text = data;
        that.checkTextPatterns();
      } else {
        html = data;
        // Convert cheerio to dom data
        $ = cheerio.load(data);
        that.checkPatterns();
      }
    });
  },
};

module.exports = addressFinder;
// good

