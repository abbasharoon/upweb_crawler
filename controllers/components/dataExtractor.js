/**
 * This module extracts the main data from the text
 * @module components/dataExtractor
 * @todo Add support for rdfa
 * @todo Add a converter from rdfa to jsonld
 * @todo Refine the third layer
 * @requires wae {web-auto-extractor} for extracting microdata, jsonld
 * @requires cheerio {cheerio} For getting the title of the page
 * @requires _ {lodash} Requires Lodash For manipulating arrays and objects
 * @requires util {modules/util} For using lowerCaseKeys
 * @requires unfluff {unfluff} For extracting the main contents of the page
 * @returns dataExtractor#start {promise}
 */
const wae = require('web-auto-extractor').default;
const cheerio = require('cheerio');
const _ = require('lodash');
const util = require('../modules/util');
const unfluff = require('unfluff');

/**
 * @type {Object}
 */
const dataExtractor = {

  /**
   * @param html
   * @returns {Object} - Containing title, meta description, description, image and heading
   */
  getStructuredData(html) {
    const $ = cheerio.load(html);
    const data = {};
    const autoExtracted = wae().parse(html);
    let extractedData = {};

    // Check if microdata exists and then use it else use jsonld if it exits
    if (!_.isEmpty(autoExtracted.microdata)) {
      // If it exists then make all the keys lower case
      // Because the keys are picked from the user's website
      extractedData = util.lowerCaseKeys(autoExtracted.microdata);
    } else if (!_.isEmpty(autoExtracted.jsonld)) {
      extractedData = util.lowerCaseKeys(autoExtracted.jsonld);
    }
    // else if (!_.isEmpty(autoExtracted.rdfa)) {
    //   // extractedData = autoExtracted.rdfa.product;
    // }

    // If we have a product key in th eobject returned by the auto extractor
    if (Object.prototype.hasOwnProperty.call(extractedData, 'product')) {
      // Then assign the values to our empty object
      extractedData = extractedData.product[0];
      data.title = $('head > title').text();
      data.heading = $('h1').text();
      data.metaDescription = autoExtracted.metatags.description;
      data.description = extractedData.description;
      data.name = extractedData.name;
      data.image = extractedData.image;
      if (extractedData.offers) {
        data.price = extractedData.offers.price;
      }
      // In case of multiple entries like description images etc,
      // we need to select only the first one
      _.forIn(data, (value, key) => {
        if (_.isArray(value)) {
          data[key] = value[0];
        }
      });
    }
    return data;
  },
  /**
   *
   * @param html
   * @returns {Object} - Containing th esame object as abve
   */
  getGuessedData(html) {
    const $ = cheerio.load(html);
    const data = {};
    const minLength = 20;
    const selectors = ['.description',
      '.short_description',
      'div[class*=desc]',
      '#description',
      '.desc',
      '#short_description'];
    const unfluffData = unfluff.lazy(html);

    data.title = $('head > title').html();
    data.metaDescription = $('meta[name=description]').text();
    data.heading = $('h1').text();
    data.image = unfluffData.image();
    data.name = unfluffData.softTitle();
    // Find the above classes or ids as they are frequently used
    _.forEach(selectors, (value) => {
      const node = $(value);
      if (node.length > 0) {
        if (!data.description && node.text().length > minLength) {
          data.description = node.text();
        }
      }
    });
    // If those classes are not found then
    // use unfluff to extract the data
    if (!data.description) {
      data.description = unfluffData.text();
    }
    return data;
  },
  /**
   *
   * @param html {String} HTML string
   * @returns {Promise} Returns the Object
   */
  start(html) {
    if (!html) {
      throw new Error('Must Provide Html String');
    }
    return this.getStructuredData(html);
  },
};

module.exports = dataExtractor;
