const pageFilter = {
  woocommerce($) {
    const feedLinks = $('link[rel=alternate i]');
    let status = true;
    feedLinks.each((index, element) => {
      if ($(element).attr('title')) {
        const title = $(element).attr('title').toLowerCase();
        if (title && (title.indexOf('tag') > -1 ||
            title.indexOf('category') > -1)) {
          status = false;
        }
      }
    });
    return status;
  },
  upWebDefault($) {
    const feedLinks = $('link[rel=alternate i]');
    let status = true;
    feedLinks.each((index, element) => {
      if ($(element).attr('title')) {
        const title = $(element).attr('title').toLowerCase();
        if (title && (title.indexOf('tag') > -1 ||
            title.indexOf('category') > -1)) {
          status = false;
        }
      }
    });
    return status;
  }
};
module.exports = pageFilter;
