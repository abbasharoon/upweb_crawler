/**
 * @todo Add condition to finding the search item to receive the selectors if possible
 * @todo obfuscate the whole code in phantomjs
 */
// const phantom = require('phantom');
const phantomPool = require('phantom-pool').default;
const Configs = require('./../../../configs/configurations');

const configs = Configs();
// Returns a generic-pool instance
const pool = phantomPool({
  max: configs.phantom.max, // default
  min: configs.phantom.min, // default
  // how long a resource can stay idle in pool before being removed
  idleTimeoutMillis: 15000, // default.
  // maximum number of times an individual resource can be reused before being destroyed; set to 0 to disable
  maxUses: configs.phantom.maxUses, // default
  // maxWaitingClients: 1000,
  // function to validate an instance prior to use; see https://github.com/coopernurse/node-pool#createpool
  validator: () => Promise.resolve(true), // defaults to always resolving true
  // validate resource before borrowing; required for `maxUses and `validator`
  testOnBorrow: true, // default
  // For all opts, see opts at https://github.comopernurse/node-pool#createpool
  phantomArgs: [[`--disk-cache=${configs.phantom.diskCache}`], {
    logLevel: 'warn',
  }], // arguments passed to phantomjs-node directly, default is `[]`. For all opts, see
      // https://github.com/amir20/phantomjs-node#phantom-object-api
});
const searchSelector = {};

function browserChecks(queueItem) {
  if (!searchSelector[queueItem.scanId]) {
    searchSelector[queueItem.scanId] = '';
  }
  // Automatically acquires a phantom instance and releases it back to the
  // pool when the function resolves or throws
  let returnData = {};
  let processChecks = 2;

  return pool.use((instance) => {
    return new Promise((resolve, reject) => {
      instance.createPage().then((page) => {
        page.on('ResourceError', function (resourceError) {
          console.log('Unable to load resource (#' + resourceError.id + 'URL:' + resourceError.url + ')');
          console.log('Error code: ' + resourceError.errorCode + '. Description: ' + resourceError.errorString);
        });
        page.on('onCallback', function (data) {
          if (data.stickySearch) {
            returnData.stickySearch = data.stickySearch;
            processChecks -= 1;
          } else if (data.productChecks) {
            returnData = Object.assign({}, returnData.db, data.productChecks);
            processChecks -= 1;
          } else if (data.searchBoxSelector) {
            searchSelector[queueItem.scanId] = data.searchBoxSelector;
          }
          if (!queueItem.dataObject) {
            processChecks -= 1;
          }
          if (processChecks === 0) {
            resolve(returnData);
          }
          // pool.drain().then(() => pool.clear());


          // Prints 'CALLBACK: { "hello": "world" }'
        });
        page.on('Error', function (msg, trace) {
          console.log('Phantomjs Page Error Happened');
          console.log(msg);
          trace.forEach(function(item) {
            console.log('  ', item.file, ':', item.line);
          });
        });
        page.property('viewportSize', {
          width: 1366, height: 760,
        }).then(() => {
            page.setting("resourceTimeout", 20000);
            return page.open(queueItem.url);
          })
          .then((data) => {
            if (data !== 'success') {
              console.log('Cannot Open the page');
              throw new Error('Cannot open ' + queueItem.url);
            } else console.log('Page Opened');
            return page.evaluate(function () {
              return (typeof jQuery === 'function') ? jQuery.fn.jquery : undefined;
            });
          })
          .then((jQueryPresent) => {
            if (jQueryPresent) {
              return true;
            }
            return page.injectJs(__dirname + '/jquery-3.1.1.min.js');
          })
          .then(() => {
            // This data object is sent to the webpage
            const data = queueItem.dataObject;
            // We set the searchbox selector here. Its used to surpass checks
            // Once we have chosen a searchbox
            data.searchBoxSelector = searchSelector[queueItem.scanId];
            page.evaluate(function (pageData) {
              // We don't need any mobile search forms or anything
              // Following function will be used as a check for that on
              // Every element that we find
              const mobileElement = function checkMobile(jqElement) {
                const classes = jqElement.attr('class');
                const ids = jqElement.attr('id');
                if (classes && (classes.toLowerCase().indexOf('mobile') > -1
                    || classes.toLowerCase().indexOf('smartphone') > -1
                    || classes.toLowerCase().indexOf('phone') > -1)) {
                  return true;
                } else if (ids && (ids.toLowerCase().indexOf('mobile') > -1
                    || ids.toLowerCase().indexOf('smartphone') > -1
                    || ids.toLowerCase().indexOf('phone') > -1)) {
                  return true;
                }
                return false;
              };
              // If data from the microdata is passed to this page
              if (pageData.price) {
                // Then create an objects of selectors
                var imageSelector = jQuery('img[src="' + pageData.image + '"]');
                if (imageSelector.length === 0 && jQuery('a[href="' + pageData.image + '"] > img').length > 0) {
                  imageSelector = jQuery('a[href="' + pageData.image + '"] > img').first();
                }
                const partsSelectors = {
                  image: imageSelector,
                };
                // And array of their keys
                const selectorKeys = [
                  'image',
                ];
                // And an empty object for returning data
                const productData = {};
                const cartTags = [
                  'BUTTON',
                  'A',
                ];
                // Select all jquery elements
                jQuery('*').filter(function () {
                  // Convert their text to strings
                  const domElement = jQuery(this);
                  const string = domElement.text().toLowerCase();
                  // If the string is not empty
                  if (string) {
                    // Then check if we have a price and it has the price mentioned
                    if (!partsSelectors.price && pageData.price && domElement.attr('itemprop') === 'offers' && string.indexOf(pageData.price) > -1) {
                      // If yes then add the jquery selector to the partsSelector
                      partsSelectors.price = jQuery(this);
                      // And push its key to the selectorKeys
                      selectorKeys.push('price');
                      //   Repeat the same for all
                    } else if (!partsSelectors.description && pageData.description && domElement.attr('itemprop') === 'description') {
                      partsSelectors.description = jQuery(this);
                      selectorKeys.push('description');
                    } else if (!partsSelectors.name && pageData.name && domElement.attr('itemprop') === 'name') {
                      partsSelectors.name = jQuery(this);
                      selectorKeys.push('name');
                    } else if (!partsSelectors.add_to_cart && jQuery.inArray(jQuery(this).prop('tagName'), cartTags) > -1 && (string.indexOf('add to cart') > -1 ||
                        string.indexOf('add to shopping bag') > -1)) {
                      partsSelectors.add_to_cart = jQuery(this);
                      selectorKeys.push('add_to_cart');
                    }
                  }
                });
                // Now itereate over all the selector keys
                for (var i = 0; i < selectorKeys.length; i += 1) {
                  // If the part selector is not empty
                  if (partsSelectors[selectorKeys[i]].length > 0) {
                    // For each element get the top and bottom offsets
                    const topOffset = partsSelectors[selectorKeys[i]].offset().top;
                    const bottomOffset = topOffset + partsSelectors[selectorKeys[i]].height();
                    // Check if bottom is not covered
                    if (bottomOffset > 760) {
                      // If it is then its at marking 2 means that it is partially visible
                      productData[selectorKeys[i]] = 2;
                    } else if (topOffset > 760) {
                      // If product selectors is not visible then its 3
                      productData[selectorKeys[i]] = 3;
                    } else {
                      // Else its totally above the fold
                      productData[selectorKeys[i]] = 1;
                    }
                  } else {
                    productData[selectorKeys[i]] = 0;
                  }
                }
                window.callPhantom({productChecks: productData});
              }
              // Send all this data to the callback

              // Variables for assigning search element and data for checks
              var searchElementSelector = '';
              var searchBox = null;
              if (pageData.searchBoxSelector) {
                searchBox = jQuery(pageData.searchBoxSelector);
              } else {
                // First we check if input type = search exists and if it does then that's our search
                const searchInput = jQuery('input[type=search]');
                if (searchInput.length > 0 && !mobileElement(searchInput)) {
                  searchBox = searchInput;
                  searchElementSelector = 'input[type="' + searchInput.attr('type') + '"]';
                } else {
                  // Else we check for uputs with placeholders having search word, we use that if it exists
                  const searchPlaceholder = jQuery('input[placeholder]').filter(function () {
                    const selector = jQuery(this).attr('placeholder');
                    if (selector && selector.toLowerCase().indexOf('search') > -1) {
                      return true;
                    }
                  });
                  if (searchPlaceholder.length > 0 && !mobileElement(searchPlaceholder)) {
                    searchBox = searchPlaceholder;
                    searchElementSelector = 'input[placeholder="' + searchPlaceholder.attr('placeholder') + '"]';
                  } else {
                    // Then we search for all elements having search id or class
                    const possibleSearchElement = jQuery('*[class*="search"],*[class*="Search"],*[id*="Search"],*[id*="search"]');
                    // Then we iterate over it to find the one which is either itself an input
                    // or contains an input with type = text
                    possibleSearchElement.filter(function () {
                      // Assign the current element to the constant
                      const element = jQuery(this);
                      // Now if the element is itself and input tag and has type == text then we use it
                      if (element.prop('tagName') === 'INPUT' && element.attr('type').toLowerCase() === 'text' && !mobileElement(element)) {
                        searchBox = element;
                        searchElementSelector = element.attr('class') || element.attr('id');
                        //   Check if the element has any input childs
                      } else if (element.find('input[type="text"]').length > 0 && !mobileElement(element)) {
                        // We use this status to end the while loop
                        var status = 0;
                        // We start with the current child element and climb its tree in the
                        // While loop till me meet the orignal element "element"
                        var currentElement = element.find('input[type="text"]').first();
                        // Selector is used to make selectors string
                        var selector = '';
                        // While we are still looping towards the parent
                        while (status !== 'completed') {
                          // If its the main parent element then end the loop
                          // And assign the final selector with class or id
                          if (element.get(0) === currentElement.get(0)) {
                            status = 'completed';
                            var attributeSelector = '';
                            if (currentElement.attr('class')) {
                              attributeSelector = '.' + currentElement.attr('class').trim().split(/\s+/).join(' .');
                            } else if (currentElement.attr('id')) {
                              attributeSelector = '#' + currentElement.attr('id').trim().split(/\s+/).join(' #');
                            }
                            selector = currentElement.prop('tagName') + attributeSelector + selector;
                            //   Else keep adding the parent elements
                          } else {
                            selector = '>' + currentElement.prop('tagName') + selector;
                            currentElement = currentElement.parent();
                          }
                        }
                        // Check if selector srting is not empty
                        // And searchElementSelector is not set or is longer then
                        // The new one then use the new one.
                        if ((selector.length > 0
                            && searchElementSelector.length > selector.length)
                          || searchElementSelector.length === 0) {
                          searchElementSelector = selector;
                          searchBox = jQuery(selector);
                          return true;
                        }
                      }
                    });
                  }
                }
              }
              // Check if we have searchBox set
              if (searchBox) {
                if (!pageData.searchBoxSelector) {
                  window.callPhantom({searchBoxSelector: searchElementSelector});
                }
                // Then set an itereateElement, we will use it to climb
                // Set every parent to visible to make the form visible
                var iterateElement = searchBox;
                while (!searchBox.is(':visible')) {
                  iterateElement.css({
                    display: 'block', visibility: 'visible',
                  });
                  iterateElement = iterateElement.parent();
                }
                // We store the window and the document jquery selectors
                const windowQuery = jQuery(window);
                const documentQuery = jQuery(document);
                // If the document height is not more than our own window's height
                // Then send a signal that we are fininshed
                if (documentQuery.height() < 761) {
                  window.callPhantom({stickySearch: 2});
                } else {
                  // Else record an event for the scorlling
                  windowQuery.scroll(function () {
                    // If windows has scrolled till the end and the box is still inside the viewport
                    if (windowQuery.scrollTop() === (documentQuery.height() - windowQuery.height())
                      && searchBox.offset().top > windowQuery.scrollTop()) {
                      // Then send this signal else the form is not sticky
                      window.callPhantom({stickySearch: 3});
                    } else {
                      window.callPhantom({stickySearch: 4});
                    }
                  });
                  windowQuery.scrollTop(documentQuery.height());
                }
              } else {
                window.callPhantom({stickySearch: 0});
              }
            }, data).catch((err) => {
              reject(err);
            });
          })
          .catch((err) => {
            reject(err);
            console.error(err);
          });
      }).catch((content) => {
        reject(content);
      });
    });
  });
}

module.exports = browserChecks;
// browserChecks({
//   url: 'http://test2.seojuice.net/product/album-plastic-gift/',
//   dataObject: {},
// });
