const _ = require('lodash');

const utilities = {
  tagDetector($, selector) {
    const tags = $(selector);
    if (tags.length > 0) {
      return tags;
    }
    return false;
  },
  lowerCaseKeys(obj) {
    return _.transform(obj, (result, value, key) => {
      let k = key;
      if (_.isString(key)) {
        k = key.toLowerCase();
      }
      if (_.isObject(value) || _.isArray(value)) {
        result[k] = this.lowerCaseKeys(value);
      } else {
        result[k] = value;
      }
    }, []);
  },
};


module.exports = utilities;
