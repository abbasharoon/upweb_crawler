const urlFilter = {
  upWebDefault(uri, checkAll) {
    const bannedUris = [
      'pa_color',
      'pa_size',
      'xmlrpc',
      'wp-json',
      'wp-comments-post',
      'feed',
      // 'cart',
      'checkout',
      // 'account',
      // 'contact',
      '?p=',
      '?ver=',
      '?add-to-cart',
      'comment',
    ];
    const nonProductUris = [
      'tag',
      'category',
      '/page/',
    ];
    let uriArray = bannedUris;
    if (checkAll === true) {
      uriArray = bannedUris.concat(nonProductUris);
    }
    for (let i = 0; i < uriArray.length; i += 1) {
      if (uri.indexOf(uriArray[i]) > -1) {
        return false;
      }
    }
    return true;
  },
  woocommerce(uri, checkAll) {
    const bannedUris = [
      'pa_color',
      'pa_size',
      'xmlrpc',
      'wp-json',
      'wp-comments-post',
      'feed',
      // 'cart',
      'checkout',
      // 'account',
      // 'contact',
      '?p=',
      '?ver=',
      '?add-to-cart',
      'comment',
    ];
    const nonProductUris = [
      'tag',
      'category',
      '/page/',
    ];
    let uriArray = bannedUris;
    if (checkAll === true) {
      uriArray = bannedUris.concat(nonProductUris);
    }
    for (let i = 0; i < uriArray.length; i += 1) {
      if (uri.indexOf(uriArray[i]) > -1) {
        return false;
      }
    }
    return true;
  },
};

module.exports = urlFilter;
