/**
 * @todo add jdoc and tests
 */

const request = require('request');
const cheerio = require('cheerio');
const Configs = require('./../../configs/configurations');

const configs = Configs();
const sitemapFinder = {
  woocommerce(host) {
    return new Promise((resolve, reject) => {
      request(`${host}/product-sitemap.xml`, (err, res) => {
        if (!err && res.statusCode === 200) {
          resolve(`${host}/product-sitemap.xml`);
        } else {
          request(`${host}/sitemap.xml`, (error, response, body) => {
            if (!error && response.statusCode === 200) {
              const $ = cheerio.load(body.toString());
              const links = $('loc');
              links.each((index, element) => {
                const productSitemap = $(element).text();
                if (productSitemap.indexOf('pt-product') > -1) {
                  resolve($(element).text());
                  return true;
                }
              });
              resolve(`${host}/sitemap.xml`);
            } else reject(new Error('No Sitemap Found'));
          });
        }
      });
    });
  },
  upWebDefault(host) {
    return new Promise((resolve, reject) => {
      request(`${host}/sitemap.xml`, (err, res) => {
        if (!err && res.statusCode === 200) {
          resolve(`${host}/sitemap.xml`);
        } else {
          request(`${host}/Sitemap.xml`, (error, response) => {
            if (!error && response.statusCode === 200) {
              resolve(`${host}/Sitemap.xml`);
            } else {
              request(`${host}/sitemap_index.xml`, (error2, response2) => {
                if (!error2 && response2.statusCode === 200) {
                  resolve(`${host}/sitemap_index.xml`);
                } else {
                  reject(new Error('Sitemap Not Found'));
                }
              });
            }
          });
        }
      });
    });
  },
  validate(sitemapUrl) {
    return new Promise((resolve,reject) => {
      const opts = {
        url: configs.sitemapValidatorUrl,
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        form: {
          secret: configs.sitemapValidatorSecret,
          sitemapUrl,
        },
      };
      request(opts, (error, response, body) => {
        if (!error && response.statusCode === 200) {
          if (JSON.parse(body).status) {
            resolve(true);
          } else {
            resolve(false);
          }
        } else {
          reject(false);
        }
      });
    });
  },
};


module.exports = sitemapFinder;

