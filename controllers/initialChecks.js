/**
 * @todo add more urls finding address
 * @todo add a shipping details finder
 * @todo add a voucher/gift card finder
 */

const wappalyzer = require('./modules/wappalyzer');
const findAddress = require('./components/addressFinder');
const htmlToText = require('html-to-text');
const scan = require('./scan');
const cheerio = require('cheerio');
const URIJS = require('urijs');
const db = require('../models');
const _ = require('lodash');
const request = require('request');
const robotics = require('./components/robotics');
const sitemapFinder = require('./modules/siteMapFinder');
const phoneNumberFinder = require('./components/phoneNumberFinder');
const tradingHours = require('./components/tradingHours');
const Pl = require('./progressLogger');

function initialChecks(Crawler, serverSecret) {
  const pl = new Pl(serverSecret);
  // pl.setToken(serverSecret);
  pl.logStatus('Starting Initial Checks');
  const data = {};

  data.token = serverSecret;
  const sequelizeData = {
    scanChecks: {
      productReviews: 0,
    },
    sitemapsRobots: {},
    business: {},
  };
  const processes = {
    wwwResolve: true,
    cmsAndChat: true,
    httpsCheck: true,
    robotics: true,
    sitemap: true,
    address: true,
    phone: true,
  };

  let scanStatus = false;

  function processChecks(datas) {
    console.log('processes');
    console.log(processes);
    datas.sequelizeData = sequelizeData;
    if (Object.keys(processes).length === 0 && scanStatus === false) {
      scanStatus = true;
      pl.logStatus('Initial Checks Completed');
      scan(datas);
    }
  }

  function cmsAndChat(dataObject) {
    const options = {
      url: dataObject.url,
      hostname: dataObject.hostname,
    };
    wappalyzer.detectFromUrl(options, (err, apps, appInfo) => {

      if (!err) {
        sequelizeData.scanChecks.cms = null;
        sequelizeData.scanChecks.liveChat = null;
        _.forEach(appInfo, (v) => {
          if (v.cats && v.cats[0] === 6) {
            sequelizeData.scanChecks.cms = v.app.toLowerCase();
            data.cms = v.app.toLowerCase();
          } else if (v.cats && v.cats[0] === 52) {
            sequelizeData.scanChecks.liveChat = v.app.toLowerCase();
          }
        });
      }
      if (!sequelizeData.scanChecks.cms || ['woocommerce'].indexOf(sequelizeData.scanChecks.cms) === -1) {
        // sequelizeData.scanChecks.cms = ;
        data.cms = 'upWebDefault';
      }
      sequelizeData.sitemapsRobots.sitemapWrong = 1;
      sitemapFinder[data.cms](data.url).then((sitemap) => {
        data.sitemap = sitemap;
        sitemapFinder.validate(sitemap).then((status) => {
          delete processes.sitemap;
          if (status) {
            sequelizeData.sitemapsRobots.sitemap = 1;
          } else {
            console.log(status);
            sequelizeData.sitemapsRobots.sitemap = 2;
          }
          processChecks(data);
        }).catch(() => {
          delete processes.sitemap;
          processChecks(data);
        });
      }).catch(() => {
        if (data.sitemap) {
          sitemapFinder.validate(data.sitemap).then((status) => {
            delete processes.sitemap;
            if (status) {
              sequelizeData.sitemapsRobots.sitemap = 1;
            } else {
              console.log(status);
              sequelizeData.sitemapsRobots.sitemap = 2;
            }
            processChecks(data);
          }).catch(() => {
            delete processes.sitemap;
            processChecks(data);
          });
        } else if (!processes.robotics) {
          delete processes.sitemap;
          processChecks(data);
        }
      });

      delete processes.cmsAndChat;
      processChecks(data);
    });
  }

  function checkWWWResolve(uriObject) {
    let url;
    if (uriObject.subdomain().toLowerCase().indexOf('www') > -1) {
      url = uriObject.toString();
    } else {
      if (uriObject.subdomain()) {
        uriObject.subdomain(`www.${uriObject.subdomain()}`);
      } else {
        uriObject.subdomain('www');
      }
      url = uriObject.toString();
    }
    request({
      url,
      followRedirect: false,
    }, (err, res) => {
      delete processes.wwwResolve;
      processChecks(data);
      if (!err && res.statusCode === 301) {
        sequelizeData.scanChecks.wwwResolve = 1;
      } else {
        sequelizeData.scanChecks.wwwResolve = null;
      }
    });
  }

  function httpsCheck(uriObject) {
    if (uriObject.protocol().toLowerCase() === 'https') {
      sequelizeData.scanChecks.ssl = 1;
    } else {
      uriObject.protocol('https');
      const url = uriObject.toString();
      request({
        url,
        followRedirect: false,
      }, (err) => {
        if (err) {
          sequelizeData.scanChecks.ssl = null;
        } else {
          sequelizeData.scanChecks.ssl = 1;
        }
      });
    }
    delete processes.httpsCheck;
    processChecks(data);
  }


  db.scans.belongsTo(db.hosts);

  db.scans.findOne({
    where: {
      serverSecret,
    },
    include: [
      {
        model: db.hosts,
      },
    ],
  }).then((dbData) => {
    data.url = dbData.get('host').get('host');
    // data.url = 'https://www.houseofwhiskyscotland.com';
    data.hostId = dbData.get('host').get('id');
    data.limit = dbData.get('crawlLimit');
    data.scanId = dbData.get('id');
    data.browserChecks = dbData.get('browserChecks');
    if (dbData.get('host').get('filter')) {
      data.filterum = dbData.get('host').get('filter').split('\n');
    }
    sequelizeData.business.scanId = data.scanId;
    sequelizeData.scanChecks.scanId = data.scanId;

    const parsedUrl = new URIJS(data.url);
    data.hostname = parsedUrl.hostname();
    data.protocol = parsedUrl.protocol();

    checkWWWResolve(parsedUrl);
    httpsCheck(parsedUrl);
    cmsAndChat(data);
    robotics.start(data.url).then((result) => {

      sequelizeData.sitemapsRobots = result;
      sequelizeData.sitemapsRobots.scanId = data.scanId;
      if (!data.sitemap && result.sitemap) {
        data.sitemap = result.sitemap;
        sitemapFinder.validate(result.sitemap).then((status) => {
          delete processes.sitemap;
          if (status) {
            sequelizeData.sitemapsRobots.sitemap = 1;
          } else {
            console.log(status);
            sequelizeData.sitemapsRobots.sitemap = 2;
          }
          processChecks(data);
        }).catch((err) => {
          delete processes.sitemap;
          processChecks(data);
          console.log(err);
        });
      }
      delete processes.robotics;
      processChecks(data);
    }).catch((err) => {
      if (!processes.sitemap && !data.sitemap) {
        sequelizeData.sitemapsRobots = err;
        delete processes.sitemap;
      } else {
        sequelizeData.sitemapsRobots.sitemapInRobots = 0;
        sequelizeData.sitemapsRobots.sitemapInRobots.robots = 0;
      }
      sequelizeData.sitemapsRobots.scanId = data.scanId;
      delete processes.robotics;
      processChecks(data);
    });

    // Starting the crawler for the home page to get some basic data.
    const crawler = Crawler(data.url);
    crawler.maxDepth = 1;
    crawler.timeout = 10000;
    crawler.decodeResponses = true;
    crawler.userAgent = 'UpWebBot';


    crawler.on('fetchcomplete', (queueItem, responseBuffer) => {
      const $ = cheerio.load(responseBuffer);
      const text = htmlToText.fromString(responseBuffer, {
        ignoreHref: true,
        ignoreImage: true,
        preserveNewlines: true,
      });

      if ($('#GTS_CONTAINER').length > 0) {
        sequelizeData.business.badge = 1;
      } else {
        sequelizeData.business.badge = null;
      }
      if (tradingHours(text)) {
        sequelizeData.business.tradingHours = 1;
      } else {
        sequelizeData.business.tradingHours = null;
      }

      phoneNumberFinder(responseBuffer, (err, validnumber) => {
        if (err || !validnumber) {
          sequelizeData.business.phoneNumber = null;
        } else {
          sequelizeData.business.phoneNumber = validnumber;
        }
        delete processes.phone;
        processChecks(data);
      });

      findAddress.find(text, data.url, true).then((address) => {
        if (address.string) {
          sequelizeData.business.address = address.string;
        } else {
          sequelizeData.business.address = address.formatted_address;
        }
        delete processes.address;
        processChecks(data);
      }).catch(() => {
        sequelizeData.business.address = null;
        delete processes.address;
        processChecks(data);
      });
    })
      .on('fetchclienterror', (queueItem, error) => {
        console.log(data.url);
        pl.logStatus({error: error.reason});
        console.log('Client error');
        console.log(error);
      })
      .on('fetcherror', () => {
        console.log(data.url);
        console.log('fetch error');
      })
      .on('fetchdataerror', () => {
        console.log(data.url);
        console.log('fetch data error');
      })
      .on('fetchdisallowed', () => {
        console.log(data.url);
        console.log('fetchdisallowed')
      })
      .on('fetchtimeout', () => {
        console.log(data.url);
        console.log('fetchtimeout')
      })
      .on('fetchtimeout', () => {
        console.log(data.url);
        console.log('fetchtimeout')
      })
      .start();
  }).catch((err) => {
    pl.logStatus('Scan Failed');
    console.log('Failed at db fetching the scan by serversecret');
    console.log(err);
  });
}

module.exports = initialChecks;
